package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp;

public class Manutencao {
    private static long ID = 0;
    private final long id;
    private String tipo; //tipo manutenção
    private String data;
    private double preco;
    String descricao;

    public Manutencao(String tipo, String data, double preco, String descricao) {
        this.id = ID++;
        this.tipo = tipo;
        this.data = data;
        this.preco = preco;
        this.descricao = descricao;
    }

    public long getId() {return id;}

    public String getTipo(){
        return tipo;
    }

    public String getDescricao(){
        return descricao;
    }

    public String getData(){
        return data;
    }

    public double getPreco(){
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
