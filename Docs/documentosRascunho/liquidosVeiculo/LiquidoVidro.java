package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

public record LiquidoVidro(InfoLiquido info) {

    @Override
    public String toString() {
        return "Água do pote dos vidros:\n" + info.toString();
    }
}
