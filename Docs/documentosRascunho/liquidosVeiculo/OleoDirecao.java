package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

public record OleoDirecao(InfoLiquido info) {
    @Override
    public String toString() {
        return "Óleo da direção:\n" + info.toString();
    }
}
