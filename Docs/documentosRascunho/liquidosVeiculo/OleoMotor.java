package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

public record OleoMotor(InfoLiquido info) {
    @Override
    public String toString() {
        return "Óleo do motor:\n" + info.toString();
    }
}
