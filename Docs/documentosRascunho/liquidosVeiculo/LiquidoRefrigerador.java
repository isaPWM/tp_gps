package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

public record LiquidoRefrigerador(InfoLiquido info){
    @Override
    public String toString() {
        return "Liquido refrigerador:\n" + info.toString();
    }
}
