package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

public record OleoTravao(InfoLiquido info) {
    @Override
    public String toString() {
        return "Óleo dos travões:\n" + info.toString();
    }
}
