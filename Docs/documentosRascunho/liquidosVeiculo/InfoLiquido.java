package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.liquidosVeiculo;

import java.io.Serializable;

public record InfoLiquido(long ID, String marca, String tipo, int quantidade, double preco) implements Serializable {
    public long getID() {
        return ID;
    }
    public String getMarca() {
        return marca;
    }
    public String getTipo() {
        return tipo;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public double getPreco() {
        return preco;
    }

    @Override
    public String toString() {
        return "marca- " + marca + "; tipo- " + tipo + "; quantidade- " + quantidade + "; preco- " + preco;
    }
}
