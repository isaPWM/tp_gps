package pt.isec.tp_gps_digitalmechanics.Model.dataClasses;

import pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.Peca;

import java.io.Serializable;

public class BlocoPeca implements Serializable {
    private Peca peca;
    private static String FILE = "src/main/resources/appFiles/";
    private String imagemPeca;

    public BlocoPeca(String marca, String modelo, double preco, int amp, int tamanho, int bar, String descricao) {
        this.peca = new Peca(marca, modelo, preco, amp, tamanho, bar, descricao);
    }
}
