package pt.isec.tp_gps_digitalmechanics.UI.Buttons;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import pt.isec.tp_gps_digitalmechanics.AppMain;
import pt.isec.tp_gps_digitalmechanics.UI.BlocoInfoButton;

public class RegistoVeiculoButton extends Button {
    private Pane blocosInfoPane, listaRegistosInfoPane;
    private FlowPane listaBlocosInfoFlowPane, listaRegistosInfoScrollPane;

    public RegistoVeiculoButton(String registo, Pane listaRegistosInfoPane) {
        super(registo);
        this.listaRegistosInfoPane = listaRegistosInfoPane;
        setPrefSize(180, 180);
        setStyle("-fx-font-family: Arial Bold; -fx-font-size: 30px;-fx-background-radius: 20%; -fx-background-color: #009999; -fx-text-fill: WHITE;");
        setOnAction(event -> onRegistoVeiculoClick());
    }

    @FXML
    protected void onRegistoVeiculoClick() {
        System.out.println("registo " + getText() + " selecionado");
        String[] lista = AppMain.appManager.getNomesBlocosInfo();
        for (String registo : lista) {
            listaBlocosInfoFlowPane.getChildren().add(new BlocoInfoButton(registo, listaRegistosInfoPane, listaRegistosInfoScrollPane));
        }
        blocosInfoPane.setVisible(true);
    }
}
