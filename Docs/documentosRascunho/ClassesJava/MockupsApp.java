package pt.isec.tp_gps_digitalmechanics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MockupsApp extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        /*FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS1.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS2.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS3.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS6.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS7.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS8.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS9_4.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS10.fxml"));
        */FXMLLoader fxmlLoader = new FXMLLoader(MockupsApp.class.getResource("USs-mockups-view/mockups-viewUS1.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Digital & Mechanics");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}