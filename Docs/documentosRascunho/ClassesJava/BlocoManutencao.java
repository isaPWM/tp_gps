package pt.isec.tp_gps_digitalmechanics.Model.dataClasses;

import pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.Manutencao;

import java.io.Serializable;

public class BlocoManutencao implements Serializable {
    private Manutencao manutencao;
    private static String FILE = "src/main/resources/appFiles/";

    public BlocoManutencao(String tipo, String data, double preco, String descricao){
        this.manutencao = new Manutencao(tipo, data, preco, descricao);
    }

    public Manutencao getManutencao() {
        return manutencao;
    }

    public void setManutencao(Manutencao manutencao) {
        this.manutencao = manutencao;
    }
}
