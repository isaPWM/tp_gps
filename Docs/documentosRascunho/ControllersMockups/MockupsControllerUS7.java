package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class MockupsControllerUS7 {
    @FXML
    private Pane homePane, blocosInfoPane, lembretesPane;

    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        blocosInfoPane.setVisible(true);
    }
    @FXML
    protected void onBlocoInfoButtonClick() {
        blocosInfoPane.setVisible(false);
        lembretesPane.setVisible(true);
    }
    @FXML
    protected void onBack2Click() {
        blocosInfoPane.setVisible(true);
        lembretesPane.setVisible(false);
    }
    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        blocosInfoPane.setVisible(false);
    }
}
