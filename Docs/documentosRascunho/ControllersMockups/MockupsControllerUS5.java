package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.control.DialogPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import static javafx.scene.input.MouseButton.PRIMARY;
import static javafx.scene.input.MouseButton.SECONDARY;

public class MockupsControllerUS5 {
    @FXML
    private Pane homePane, editarVeiculoPane;
    @FXML
    private DialogPane inserirImagem;

    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        editarVeiculoPane.setVisible(true);
    }
    @FXML
    protected void inserirImagem() {
        inserirImagem.setVisible(true);
    }

    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        editarVeiculoPane.setVisible(false);
    }
}