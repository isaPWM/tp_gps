package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;

public class MockupsControllerUS6 {
    @FXML
    private Pane homePane, blocosInfoPane, historicoPane;
    @FXML
    private ChoiceBox<String> choice;

    private String[] opt = {"Revisão", "Troca de óleo", "Troca de Pneus","Reparação"};

    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        blocosInfoPane.setVisible(true);
    }

    @FXML
    protected void onBlocoInfoButtonClick() {
        blocosInfoPane.setVisible(false);
        historicoPane.setVisible(true);
        choice.getItems().addAll(opt);
    }

    @FXML
    protected void onBack2Click() {
        blocosInfoPane.setVisible(true);
        historicoPane.setVisible(false);
    }
    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        blocosInfoPane.setVisible(false);
    }
}
