package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class MockupsControllerUS10 {
    @FXML
    private Pane homePane, blocosInfoPane, eliminarRegistos;
    @FXML
    private Button eliminaButton;

    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        blocosInfoPane.setVisible(true);
    }
    @FXML
    protected void onBlocoInfoButtonClick() {
        blocosInfoPane.setVisible(false);
        eliminarRegistos.setVisible(true);
    }
    @FXML
    protected void onBack2Click() {
        blocosInfoPane.setVisible(true);
        eliminarRegistos.setVisible(false);
    }
    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        blocosInfoPane.setVisible(false);
    }
    @FXML
    protected void onCheck() {
         eliminaButton.setDisable(!eliminaButton.isDisable());
    }
}
