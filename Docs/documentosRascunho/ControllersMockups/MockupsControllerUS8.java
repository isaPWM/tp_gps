package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class MockupsControllerUS8 {
    @FXML
    private Pane homePane, blocosInfoPane, editarPane, procurarPane;
    @FXML
    private Label idRegisto;
    @FXML
    private TextField procurarIdTextField;


    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        blocosInfoPane.setVisible(true);
    }
    @FXML
    protected void onBlocoInfoButtonClick() {
        blocosInfoPane.setVisible(false);
        procurarPane.setVisible(true);
    }
    @FXML
    protected void onBack3Click() {
        editarPane.setVisible(false);
    }
    @FXML
    protected void onBack2Click() {
        blocosInfoPane.setVisible(true);
        procurarPane.setVisible(false);
    }
    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        blocosInfoPane.setVisible(false);
    }
    @FXML
    protected void OnProcurarRegisto() {
        idRegisto.setText("Editar registo: [id: " + procurarIdTextField.getText() + " ]");
        editarPane.setVisible(true);
    }
}