package pt.isec.tp_gps_digitalmechanics.UI.ControllersMockups;

import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class MockupsControllerUS2 {
    @FXML
    private Pane homePane, blocosInfoPane, registosManutencaoPane, novoRegistoPane;

    @FXML
    protected void onVeiculoButtonClick() {
        homePane.setVisible(false);
        blocosInfoPane.setVisible(true);
    }
    @FXML
    protected void onBlocoInfoButtonClick() {
        blocosInfoPane.setVisible(false);
        registosManutencaoPane.setVisible(true);
    }
    @FXML
    protected void onBack3Click() {
        novoRegistoPane.setVisible(false);
    }
    @FXML
    protected void onBack2Click() {
        blocosInfoPane.setVisible(true);
        registosManutencaoPane.setVisible(false);
    }
    @FXML
    protected void onBack1Click() {
        homePane.setVisible(true);
        blocosInfoPane.setVisible(false);
    }
    @FXML
    protected void onAddMudancaClick() {
        novoRegistoPane.setVisible(true);
    }
}