package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record CorreiaDistribuicao(InfoPeca infoPeca, String tipo) {
    public String getTipo() {
        return tipo;
    }
    @Override
    public String toString(){
        return "CorreiaDistribuicao:\n" + infoPeca.toString() + "; tipo- " + tipo;
    }
}
