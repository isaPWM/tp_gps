package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record FiltroAr(InfoPeca informacao) {
    @Override
    public String toString(){
        return "Filtro do ar:\n" + informacao.toString();
    }
}
