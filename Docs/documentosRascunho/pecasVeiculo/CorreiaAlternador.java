package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record CorreiaAlternador(InfoPeca informacao) {
    @Override
    public String toString(){
        return "Correia Alternador:\n" + informacao.toString();
    }
}
