package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Suspensao(InfoPeca infoPeca, String tipo, String forca, String tamanho) {
    public String getTipo() {
        return tipo;
    }

    public String getTamanho() {
        return tamanho;
    }

    public String getForca() {
        return forca;
    }

    @Override
    public String toString() {
        return "CorreiaDistribuicao:\n" + infoPeca.toString() + "; tipo- " + tipo + "; tamanho- " + tamanho + "; forca- " + forca;
    }
}
