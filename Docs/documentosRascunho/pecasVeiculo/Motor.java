package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Motor(int potenciaCv, int potenciaKw, int cilindrada, String combustivel) {

    public int getPotenciaCv() {
        return potenciaCv;
    }

    public int getPotenciaKw() {
        return potenciaKw;
    }

    public int getCilindrada() {
        return cilindrada;
    }

    public String getCombustivel() {
        return combustivel;
    }

    @Override
    public String toString(){
        return "Motor:\n" + "potencia (km)- " + potenciaKw + "; potencia (cv)- " + potenciaCv + "; cilindrada- " + cilindrada + "; combustivel- " + combustivel;
    }
}
