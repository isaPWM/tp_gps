package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Jante(InfoPeca infoPeca, String tamanho) {
    public String getTamanho() {
        return tamanho;
    }
    @Override
    public String toString() {
        return "Jante:\n" + infoPeca.toString() + "; tamanho- " + tamanho.toString();
    }
}
