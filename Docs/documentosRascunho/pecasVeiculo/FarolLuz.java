package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record FarolLuz(InfoPeca infoPeca, String cor) {
    public String getCor() {
        return cor;
    }
    @Override
    public String toString() {
        return "Farol de luz:\n" + infoPeca.toString() + "; cor- " + cor;
    }
}
