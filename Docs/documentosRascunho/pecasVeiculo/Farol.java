package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Farol(InfoPeca infoPeca, String tipo) {
    @Override
    public String toString() {
        return "Farol:\n" + infoPeca.toString() + "; tipo- " + tipo;
    }
}
