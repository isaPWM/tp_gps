package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

import java.io.Serializable;

public record InfoPeca(long ID, String marca, String modelo, double preco) implements Serializable {

    //---------- GETTERS & SETTERS ----------------------------------------
    public long getID() {
        return ID;
    }
    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public double getPreco() {
        return preco;
    }
}
