package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record PincasTravao(InfoPeca infoPeca, String tipo) {
    public String getTipo() {
        return tipo;
    }
    @Override
    public String toString() {
        return "Pincas de Travao:\n" + infoPeca.toString() + "; tipo- " + tipo;
    }
}
