package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record FiltroGas(InfoPeca informacao) {
    @Override
    public String toString(){
        return "Filtro do combustivel:\n" + informacao.toString();
    }
}
