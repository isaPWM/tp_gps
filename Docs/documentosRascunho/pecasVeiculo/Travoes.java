package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Travoes(InfoPeca infoPeca, String tipo, int numero, String tamanho) {
    public String getTipo() {
        return tipo;
    }

    public int getNumero() {
        return numero;
    }

    public String getTamanho() {
        return tamanho;
    }

    @Override
    public String toString() {
        return "Travoes:\n" + infoPeca + "; tipo- " + tipo + "; numero- " + numero + "; tamanho- " + tamanho;
    }
}
