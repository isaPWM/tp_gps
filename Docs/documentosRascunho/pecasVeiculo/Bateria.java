package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Bateria(InfoPeca informacao, String tamanho, int potenciaAmp) {
    public int getPotenciaAmp() {
        return potenciaAmp;
    }

    @Override
    public String toString(){
        return "Bateria:\n" + informacao.toString() + "; tamanho- " + tamanho + "; potencia- " + potenciaAmp;
    }
}
