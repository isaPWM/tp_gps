package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Vela(InfoPeca informacao) {

    @Override
    public String toString(){
        return "Vela:\n" + informacao.toString();
    }
}
