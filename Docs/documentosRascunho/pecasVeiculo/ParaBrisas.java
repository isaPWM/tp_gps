package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record ParaBrisas(InfoPeca infoPeca) {
    @Override
    public String toString() {
        return "Parabrisas:\n" + infoPeca.toString();
    }
}
