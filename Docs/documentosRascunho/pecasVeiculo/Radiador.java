package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Radiador(InfoPeca infoPeca) {
    @Override
    public String toString() {
        return "Radiador:\n" + infoPeca.toString();
    }
}
