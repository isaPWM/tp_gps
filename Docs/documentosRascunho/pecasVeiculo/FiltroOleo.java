package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record FiltroOleo(InfoPeca informacao) {
    @Override
    public String toString(){
        return "Filtro do oléo:\n" + informacao.toString();
    }
}
