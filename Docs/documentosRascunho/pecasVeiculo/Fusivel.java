package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Fusivel(InfoPeca informacao, String numero, int Amp) {
    @Override
    public String numero() {
        return numero;
    }

    @Override
    public int Amp() {
        return Amp;
    }

    @Override
    public String toString(){
        return "Fusível:\n" + informacao.toString() + "; numero- " + numero + "; amperes- " + Amp;
    }
}
