package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Pneu(InfoPeca infoPeca, String tipoEstacao, String tamanho) {
    public String getTipoEstacao() {
        return tipoEstacao;
    }
    public String getTamanho() {
        return tamanho;
    }

    @Override
    public String toString() {
        return "Pneu:\n" + infoPeca.toString() + "; tamanho- " + tamanho + "; tipo estacao- " + tipoEstacao;
    }
}
