package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record OutraPeca(String nome, InfoPeca infoPeca, String referencia, String tamanho) {
    public String getReferencia() {
        return referencia;
    }

    public String getTamanho() {
        return tamanho;
    }
    @Override
    public String toString() {
        return "" + nome + ":\n" + infoPeca.toString() + "; referencia- " + referencia + "; tamanho- " + tamanho;
    }
}
