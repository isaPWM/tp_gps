package pt.isec.tp_gps_digitalmechanics.Model.dataRecords.autoComp.pecasVeiculo;

public record Turbo(InfoPeca infoPeca, String tamanho, int peso, int potenciaBar) {
    public String getTamanho() {
        return tamanho;
    }
    public int getPeso() {
        return peso;
    }
    public int getPotenciaBar() {
        return potenciaBar;
    }
    @Override
    public String toString() {
        return "Turbo:\n: " + infoPeca.toString() + "; tamanho- " + tamanho + "; peso- " + peso + "; potencia- " + potenciaBar;
    }
}
