#########################################################################
**US4
| Cenário 1.1 | Adição de uma imagem a um registo de produto                                                   |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário clicou sobre um registo de produto/peça                                         |
| And         | O proprietário clicou em editar registo                                                        |
| When        | O proprietário inseriu a localização válida para uma imagem válida                             |
| And         | Clicar no botão de confirmar edição                                                            |
| Then        | O registo é adicionado à lista de peças                                                        |
| And         | O registo fica uma imagem associada                                                            |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 1.3  | Adição de uma imagem a um registo de produto                                                             |
|:-------------|:---------------------------------------------------------------------------------------------------------|
| Given        | O proprietário clicou sobre um registo de produto                                                        |
| And          | O proprietário clicou em editar registo                                                                  |
| When         | O proprietário inseriu uma localização inválida para uma imagem ou o documento da localização é inválido |
| And          | Clicar no botão de confirmar edição                                                                      |
| Then         | A edição não é efetuada                                                                                  |
| And          | O registo fica uma imagem associada                                                                      |
| And          | O documento de informações do veículo físico ao qual o bloco digital diz respeito mantém-se inalterado   |
#########################################################################
**US5

| Cenário 1.1 | Registo de um veículo com fotografia                                                           |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário clicou no botão de adicionar novo registo de veículo                            |
| And         | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| And         | Adicionou um ficheiro válido para imagem do veículo                                            |
| When        | Clicar no botão de confirmar "adicionar veículo"                                               |
| Then        | O registo é adicionado à lista de veículos                                                     |
| And         | O registo de veículo tem uma imagem associada como identificação                               |
| And         | A lista de veículos é atualizada                                                               |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 1.2 | Registo de um veículo com fotografia                                                                                   |
|:------------|:-----------------------------------------------------------------------------------------------------------------------|
| Given       | O proprietário no botão de adicionar novo registo                                                                      |
| And         | Nem todos os campos obrigatórios estão preenchidos ou válidos ou adicionou um ficheiro inválido para imagem do veículo |
| When        | Clicar no botão de confirmar "adicionar veículo"                                                                       |
| Then        | É avisado de que há campos em falta                                                                                    |
| And         | O registo não é adicionado à lista de veículos                                                                         |
| And         | O respetivo documento de informações físico mantém-se inalterado                                                       |

| Cenário 1.3 | Registo de um veículo sem fotografia                                                           |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O utilizador clicou no botão de adicionar novo registo                                         |
| And         | Todos os campos obrigatórios e não foi adicionado um ficheiro para imagem do veículo           |
| When        | Clicar no botão de confirmar "adicionar registo"                                               |
| Then        | O registo é adicionado à lista de veículos                                                     |
| And         | O registo de veículo não fica com uma imagem associada como identificação                      |
| And         | A lista de veículos é atualizada                                                               |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |
#########################################################################
**US7


| Cenário 1.1 | Acesso aos lembretes ativos                                                                    |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário clicou no toggle dos lembretes ativos                                           |
| And         | E não definiu um filtro para a data                                                            |
| When        | A lista de lembretes for filtrada segundo os filtros                                           |
| Then        | É mostrada uma lista de todos os lembretes ativos no ecrã                                      |
| Given       | O proprietário vê o lembrete que pretende abrir                                                |
| When        | O proprietário abrir esse lembrete                                                             |
| Then        | É mostrada a informação associada a esse lembrete                                              |
| And         | Esse lembrete passa a estar com estado "visto"                                                 |
| And         | Esse lembrete deixa de ser apresentado em lembretes ativos                                     |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 1.2 | Acesso a um lembrete por engano                                                                |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário abriu um lembrete ativo por engano                                              |
| And         | O lembrete ficou marcado como visto                                                            |
| When        | O proprietário marcar o lembrete como não visto                                                |
| Then        | O lembrete volta a estar com estado "ativo"                                                    |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | Acesso aos lembretes com filtro de data e ativos                                               |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário inseriu uma data no filtro                                                      |
| And       | E clicou no toggle dos lembretes ativos                                                        |
| When      | A lista de lembretes for filtrada segundo os filtros                                           |
| Then      | É mostrada uma lista de todos os lembretes ativos dentro do intervalo de tempo imposto no ecrã |
| Given     | O proprietário vê o lembrete que pretende abrir                                                |
| When      | O proprietário abrir esse lembrete                                                             |
| Then      | É mostrada a informação associada a esse lembrete                                              |
| And       | Esse lembrete passa a estar com estado "visto"                                                 |
| And       | Esse lembrete deixa de ser apresentado em lembretes ativos                                     |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 3 | Acesso aos lembretes perdidos                                                                  |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no toggle dos lembretes perdidos                                         |
| When      | A lista de lembretes for filtrada segundo ser "lembrete perdido"                               |
| Then      | É mostrada uma lista de todos os lembretes perdidos no ecrã                                    |
| Given     | O proprietário vê o lembrete que pretende abrir                                                |
| When      | O proprietário abrir esse lembrete                                                             |
| Then      | É mostrada a informação associada a esse lembrete                                              |
| And       | Esse lembrete passa a estar com estado "visto"                                                 |
| And       | Esse lembrete deixa de ser apresentado em lembretes ativos                                     |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 4 | Eliminação de um lembrete                                                                      |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário abriu o lembrete que pretende apagar                                            |
| When      | O proprietário clicar no botão para eliminar                                                   |
| Then      | O lembrete é removido da lista de lembretes                                                    |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |
#########################################################################
**US8


| Cenário 1  | Criar um lembrete para um registo                                                              |
|:-----------|:-----------------------------------------------------------------------------------------------|
| Given      | O proprietário inseriu o id do registo pretendido                                              |
| When       | Ele clicar no botão pesquisar                                                                  |
| Then       | O registo é aberto em modo edição                                                              |
| Given      | O proprietário escolheu uma data                                                               |
| And        | Clicou em gerar lembrete                                                                       |
| When       | O proprietário clicar no botão de confirmar edição                                             |
| Then       | O registo é alterado                                                                           |
| And        | É criado e adicionado um lembrete à lista de lembretes                                         |
| And        | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

#########################################################################
**US10


| Cenário 1 | Eu apago um registo                                                                            |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário abriu o registo que pretende eliminar                                           |
| When      | Clicar no botão para eliminar                                                                  |
| Then      | É preguntado ao utilizador se tem a certeza de que quer eliminar o registo                     |
| Given     | O proprietário pretende mesmo apagar o resgito                                                 |
| When      | O proprietário selecionar confirmar a eliminação                                               |
| Then      | O registo é eliminado                                                                          |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

#########################################################################
**US11


| Cenário 1.1 | Importar o bloco digital de um novo cliente                                                      |
|:------------|:-------------------------------------------------------------------------------------------------|
| Given       | O mecânico clicou em adicionar registo de veículo                                                |
| And         | Foi selecionado importar bloco                                                                   |
| And         | Foi inserida a localização do documento importado referente ao bloco de registos do cliente novo |
| And         | A senha de acesso inserida para importe do registo na aplicação está correta                     |
| When        | O mecânico clicar no botão "importar"                                                            |
| Then        | A aplicação criará um novo registo de veículo apartir do documento importado                     |
| And         | O documento só estará apto para ações de leitura                                                 |

| Cenário 1.2 | Importar o bloco digital de um novo cliente                                          |
|:------------|:-------------------------------------------------------------------------------------|
| Given       | O mecânico clicou em adicionar registo de veículo                                    |
| And         | Foi selecionado importar bloco                                                       |
| And         | Foi inserida ou uma localização do documento a importar ou senha de acesso inválida. |
| When        | O mecânico clicar no botão "importar"                                                |
| Then        | A aplicação não criará um novo registo importado de veículo                          |
| And         | O mecânico será avisado de que inseriu dados incorretos                              |

#########################################################################
**US12


| Cenário 1.1 | Importar o bloco digital de um antigo dono                                             |
|:------------|:---------------------------------------------------------------------------------------|
| Given       | O novo proprietário clicou em adicionar registo de veículo                             |
| And         | Foi selecionado importar bloco                                                         |
| And         | Foi inserida a localização do documento importado referente ao registo de veículo novo |
| And         | Foi inserida a senha correta de acesso para importe do registo na aplicação            |
| When        | O novo proprietário clicar no botão "importar"                                         |
| Then        | A aplicação criará um novo registo de veículo apartir do documento importado           |
| And         | O documento estará apto para ações de leitura e escrita                                |

| Cenário 1.2 | Importar o bloco digital de um antigo dono                                           |
|:------------|:-------------------------------------------------------------------------------------|
| Given       | O novo proprietário clicou em adicionar registo de veículo                           |
| And         | Foi selecionado importar bloco                                                       |
| And         | Foi inserida ou uma localização do documento a importar ou senha de acesso inválida. |
| When        | O novo proprietário clicar no botão "importar"                                       |
| Then        | A aplicação não criará um novo registo importado de veículo                          |
| And         | O novo proprietário será avisado de que inseriu dados incorretos                     |

#########################################################################
**US13

| Cenário 1 | Eliminar dados registados                                                                                    |
|:----------|:-------------------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a eliminar abrindo o perfil do registo             |
| When      | Clicar em eliminar registo                                                                                   |
| Then      | É perguntado se tem a certeza de que pretende eliminar o registo de veículo                                  |
| Given     | É mesmo pretendido proceder com a eliminação                                                                 |
| When      | O utilizador selecionar confirmar a eliminação                                                               |
| Then      | O registo de veículo é eliminado                                                                             |
| And       | O documento físico de informações do veículo ao qual o bloco digital diz respeito é eliminado do dispositivo |

#########################################################################
**US14

| Cenário 1 | Exportar bloco digital do veículo a mecânico                                                     |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a exportar abrindo o perfil do registo |
| When      | O utilizador clicar em exportar registo veículo                                                  |
| And       | Escolher exportar para leitura                                                                   |
| Then      | É gravada uma senha de importação no registo                                                     |
| And       | É ativada a flag de "leitura" no registo                                                         |
| And       | O ficheiro de registo de veículo é exportado para uma pasta zip                                  |
| And       | É exibida uma mensagem com a localização da pasta zip e a respetiva senha de acesso para importe |

#########################################################################
**US15


| Cenário 1 | Exportar bloco digital do veículo a novo proprietário                                            |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a exportar abrindo o perfil do registo |
| When      | O utilizador clicar em exportar registo veículo                                                  |
| And       | Escolher exportar para novo proprietário                                                         |
| Then      | É gravada uma senha de importação no registo                                                     |
| And       | É ativada a flag de "novo proprietário" no registo                                               |
| And       | O ficheiro de registo de veículo é exportado para uma pasta zip                                  |
| And       | É exibida uma mensagem com a localização da pasta zip e a respetiva senha de acesso para importe |

#########################################################################
**US16


#########################################################################
**US17



