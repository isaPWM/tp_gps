package pt.isec.GPS.DigitalMechanics.Model;

import org.junit.jupiter.api.Test;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.InfoRetorno;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class AppManagerTest {
    AppManager appManager = new AppManager();

    // ///////////////////// TESTES DE ADICAO DE INFO ///////////////////////

    /// ADICIONA REGISTO VEICULO
    @Test
    void adicionaRegistoVeiculo_CAMPOS_PREENCHIDOS() {
        appManager.eliminaRegistoVeiculo("11-11-UU");
        InfoRetorno info = appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-11-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        assertTrue(info.resultado());
    }
    @Test
    void adicionaRegistoVeiculo_PELO_MENOS_UM_CAMPO_NAO_PREENCHIDO() {
        //sem marca
        System.out.println("\nsem marca:");
        InfoRetorno info = appManager.adicionaRegistoVeiculo(null, "rav4", "11-11-DD", "gasolina", 2000, 1, "1000", "");
        assertFalse(info.resultado());

        //sem modelo
        System.out.println("\nsem modelo:");
        info = appManager.adicionaRegistoVeiculo("toyota", null, "11-11-YY", "gasolina", 2000, 1, "1000", "");
        assertFalse(info.resultado());

        //sem matricula
        System.out.println("\nsem matricula:");
        info = appManager.adicionaRegistoVeiculo("toyota", "rav4", null, "gasolina", 2000, 1, "1000", "");
        assertFalse(info.resultado());

        //sem combustivel
        System.out.println("\nsem combustivel:");
        info = appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-11-GG", null, 2000, 1, "1000", "");
        assertFalse(info.resultado());

        //sem quilometros
        System.out.println("\nsem quilometros:");
        info = appManager.adicionaRegistoVeiculo("clio", "slim", "11-11-LL", "gasolina", 2000, 1, null, "");
        assertFalse(info.resultado());
    }

    @Test
    void adicionaRegistoVeiculo_QUILOMETROS_NAO_NUMERICO_OU_NEGATIVO() {

        //quilometros não numérico
        System.out.println("\nquilometros não numérico:");
        InfoRetorno info = appManager.adicionaRegistoVeiculo("toyota", "rav4", "00-11-MM", "gasolina", 2000, 1, "33FS", "");
        assertFalse(info.resultado());

        //quilometros negativos
        System.out.println("\nquilometros negativos:");
        info = appManager.adicionaRegistoVeiculo("toyota", "rav4", "44-11-MM", "gasolina", 2000, 1, "-33", "");
        assertFalse(info.resultado());
    }

    @Test
    void adicionaRegistoVeiculo_MATRICULA_EXISTENTE() {
        System.out.println("\nmatricula existente:");
        //registo um
        appManager.eliminaRegistoVeiculo("11-99-II");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-99-II", "gasolina", 2000, 1, "1000", "");
        //tentativa de registo de veículo com matrícula igual
        assertFalse(appManager.adicionaRegistoVeiculo("fiat", "ponto", "11-99-II", "gasolina", 2000, 1, "1330", "").resultado());
        appManager.eliminaRegistoVeiculo("11-99-II");
    }
    @Test
    void adicionaRegistoVeiculo_FORMATO_MATRICULA_INVALIDA_PARA_O_ANO_DE_EMISSAO() {
        System.out.println("\n\n\nformato matricula invalida para o ano de emissao:");

        System.out.println("\nano < 1992 e formato diferente de AA-00-00");
        InfoRetorno info = appManager.adicionaRegistoVeiculo("fiat", "ponto", "99-PP-99", "gasolina", 1990, 1, "1330", "");
        assertFalse(info.resultado());

        System.out.println("\n1992 <= ano < 2005 e formato diferente de 00-00-AA");
        info = appManager.adicionaRegistoVeiculo("fiat", "ponto", "99-PP-99", "gasolina", 1990, 1, "1330", "");
        assertFalse(info.resultado());

        System.out.println("\n2005 <= ano < 2020 e formato diferente de 00-AA-00");
        info = appManager.adicionaRegistoVeiculo("fiat", "ponto", "MM-YY-99", "gasolina", 2020, 1, "1330", "");
        assertFalse(info.resultado());

        System.out.println("\nano >= 2020 e formato diferente de AA-00-00");
        info = appManager.adicionaRegistoVeiculo("fiat", "ponto", "99-yy-99", "gasolina", 2023, 1, "1330", "");
        assertFalse(info.resultado());
    }

    /// ADICIONA REGISTO ABASTECIMENTO
    @Test
    void adicionaAbastecimento_CAMPOS_PREENCHIDOS() {
        System.out.println("\ncampos preenchidos:");
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        InfoRetorno info = appManager.adicionaAbastecimento("10", "9", "21526", LocalDate.of(2023, 11, 16));
        assertTrue(info.resultado());
    }

    @Test
    void adicionaAbastecimento_PELO_MENOS_UM_CAMPO_NAO_PREENCHIDO() {
        //sem preço
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        System.out.println("\nsem preço:");
        InfoRetorno info = appManager.adicionaAbastecimento("", "9", "21526", LocalDate.EPOCH);
        assertFalse(info.resultado());

        //sem litros
        System.out.println("\nsem litros:");
        info = appManager.adicionaAbastecimento("10", "", "21526", LocalDate.EPOCH);
        assertFalse(info.resultado());

        //sem quilometros
        System.out.println("\nsem quilometros:");
        info = appManager.adicionaAbastecimento("10", "9", "", LocalDate.EPOCH);
        assertFalse(info.resultado());

        //sem data
        System.out.println("\nsem data:");
        info = appManager.adicionaAbastecimento("10", "9", "21526", null);
        assertFalse(info.resultado());
    }

    @Test
    void adicionaAbastecimento_PELO_MENOS_UM_CAMPO_NAO_NUMERICO() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        //preço não numérico
        System.out.println("\npreço não numérico:");
        InfoRetorno info = appManager.adicionaAbastecimento("jasd", "9", "21526", LocalDate.EPOCH);
        assertFalse(info.resultado());

        //litros não numéricos
        System.out.println("\nlitros não numéricos:");
        info = appManager.adicionaAbastecimento("10", "hdh", "21526", LocalDate.EPOCH);
        assertFalse(info.resultado());

        //quilometros não numéricos
        System.out.println("\nquilómetros não numéricos:");
        info = appManager.adicionaAbastecimento("10", "9", "jhd", LocalDate.EPOCH);
        assertFalse(info.resultado());
    }

    ///ADICIONA REGISTO MANUTENCAO
    @Test
    void adicionaManutencao(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        System.out.println("\ncampos preenchidos:");
        InfoRetorno info = appManager.adiciona_edita_Manutencao("troca pneus", LocalDate.EPOCH, "160", "trocado os 4 pneus!", null, false);
        assertTrue(info.resultado());
    }
    @Test
    void adicionaManutencaoUmCampoNaoPreenchido(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        System.out.println("\nsem tipo de manutencao:");
        InfoRetorno info = appManager.adiciona_edita_Manutencao(null, LocalDate.EPOCH, "43", "OK", null, false);
        if(info != null)
            assertFalse(info.resultado());

        System.out.println("\nsem data de manutencao:");
        info = appManager.adiciona_edita_Manutencao("troca pneus", null, "100", "ok", null, false);
        if(info != null)
            assertFalse(info.resultado());

        System.out.println("\nsem preco:");
        info = appManager.adiciona_edita_Manutencao("troca pneus", LocalDate.EPOCH, "", "ok", null, false);
        if(info != null)
            assertFalse(info.resultado());

        System.out.println("\nsem observaçoes:");
        info = appManager.adiciona_edita_Manutencao("troca pneus", LocalDate.EPOCH, "45", "", null, false);
        if(info != null)
            assertTrue(info.resultado());
    }

    @Test
    void adicionaManutencaoValorPrecoNaoNumerico(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        System.out.println("\npreco nao numerico");
        InfoRetorno info = appManager.adiciona_edita_Manutencao("troca pneu", LocalDate.EPOCH, "sad", "OK", null, false);
        if(info != null)
            assertFalse(info.resultado());
    }

    @Test
    void adicionManutencaoValorPrecoNegativo(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Manutenções");

        System.out.println("\npreco negativo");
        InfoRetorno info = appManager.adiciona_edita_Manutencao("troca pneu", LocalDate.EPOCH, "-0.1", "as", null, false);
        if(info != null)
            assertFalse(info.resultado());
    }

    /// ADICIONA REGISTO PECA
    @Test
    void adicionaPeca() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");
        InfoRetorno info = appManager.adiciona_edita_Peca("Turbo", "marca", "modelo", "100", "descricao", "loja", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), false);
        if(info != null)
            assertTrue(info.resultado());
    }

    @Test
    void adicionaPecaSemFoto() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");

        InfoRetorno info = appManager.adiciona_edita_Peca("Turbo", "marca", "modelo", "100", "descricao", "loja", LocalDate.EPOCH, "", false);
        if(info != null)
            assertTrue(info.resultado());
    }

    @Test
    void adicionaPecaComPathInvalido() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");

        InfoRetorno info = appManager.adiciona_edita_Peca("Turbo", "marca", "modelo", "100", "descricao", "loja", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIconsss/outra_peca.png")), false);
        if(info != null)
            assertTrue(info.resultado());
    }

    @Test
    void adicionaPeca_Campo_Obrigatorio() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");

        InfoRetorno info = appManager.adiciona_edita_Peca("", "", "modelo", "", "descricao", "loja", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), false);
        if(info != null)
            assertFalse(info.resultado());
    }

    @Test
    void adicionaPeca_Campo_NAO_Obrigatorio() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");

        InfoRetorno info = appManager.adiciona_edita_Peca("Turbo", "marca", "modelo", "100", "", "", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), false);
        if(info != null)
            assertTrue(info.resultado());
    }
    @Test
    void adicionaOleoCampoNaoObrigatorio() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Mudança Óleo");

        InfoRetorno info = appManager.adiciona_edita_MudancaOleo(LocalDate.EPOCH, "data","","45.00", null, false);
        if(info != null)
            assertTrue(info.resultado());
    }
    @Test
    void adicionaOleo(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Mudança Óleo");

        InfoRetorno info = appManager.adiciona_edita_MudancaOleo(LocalDate.EPOCH, "data",String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), "45.00", null, false);
        if(info != null)
            assertTrue(info.resultado());
    }
    @Test
    void adiconaOleoCampoObrigatorionaoPreenchido(){
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Mudança Óleo");

        InfoRetorno info = appManager.adiciona_edita_MudancaOleo(null, "data","", "45.00", null, false);
        if(info != null)
            assertFalse(info.resultado());
    }
    // ///////////////////// TESTES DE SELECAO ///////////////////////
    @Test
    void selecionaRegistoVeiculo() {
    }

    @Test
    void selecionaBlocoInformacao() {
        System.out.println("\n\n\nseleciona bloco informacao:");
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.adicionaBlocoInformacao("bloco2");
        assertTrue(appManager.selecionaBlocoInformacao("bloco2"));
    }

    //////////////////////// TESTES DE ELIMINAÇÃO ///////////////////////
    @Test
    void eliminaPeca() {
        System.out.println("\n\n\nelimina peca:");
        adicionaPeca();
        assertTrue(appManager.eliminaInfo(appManager.getListaRegistosDeBlocoAtual().get(0)));
    }

    @Test
    void editaPeca() {
        appManager.selecionaRegistoVeiculo("11-11-UU");
        appManager.selecionaBlocoInformacao("Peças");
        appManager.adiciona_edita_Peca("Turbo", "marca", "modelo", "100", "descricao", "loja", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), false);
        InfoRetorno info = appManager.adiciona_edita_Peca("Turbo","toyota", "supra", "1","ola","stand", LocalDate.EPOCH, String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")), true);
        if(info != null)
            assertTrue(info.resultado());
    }


    @Test
    void testeCriarTrackingViagem(){
        System.out.println("\n\n\ncriar tracking viagem:");
        appManager.selecionaRegistoVeiculo("11-11-UU");
        int nTrackings = appManager.getNomesTrackingViagens().length;
        appManager.novoTrackingViagem("Isabel vai de Viagem", "nop", "1000", "Espanha");
        assertEquals(nTrackings + 1, appManager.getNomesTrackingViagens().length);

        System.out.println("testeCriarTrackingViagemComNomeExistentePrecisamenteIgual");
        System.out.println("\n\n\ncriar tracking viagem com nome existente:");
        appManager.selecionaRegistoVeiculo("11-11-UU");
        nTrackings = appManager.getNumTrackingViagens();
        assertFalse(appManager.novoTrackingViagem("Isabel vai de Viagem", "nop", "1000", "Espanha").resultado());
        assertEquals(nTrackings, appManager.getNomesTrackingViagens().length);

        System.out.println("testeCriarTrackingViagemComNomeIgual_CaseInsensitiveEEspacos");
        appManager.selecionaRegistoVeiculo("11-11-UU");
        nTrackings = appManager.getNomesTrackingViagens().length;
        appManager.novoTrackingViagem("isabel vai de viagem", "nop", "1000", "Espanha");
        assertEquals(nTrackings, appManager.getNomesTrackingViagens().length);
    }

    @Test
    void exportaVeiculo() {
        System.out.println("\n\n\nexporta veiculo:");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-12-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        appManager.selecionaRegistoVeiculo("11-12-UU");
        Path path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/");
        assertTrue(appManager.exportarVeiculo(new File(path.toUri()), false));
    }
/*
    @Test
    void importaVeiculo() { //executar primeiro o exporta
        System.out.println("\n\n\nimporta veiculo:");
        appManager.eliminaRegistoVeiculo("11-12-UU");
        appManager.setSenhaDada("123");
        Path path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/11-12-UU/dados.txt");
        assertTrue(appManager.importaVeiculo(new File(path.toUri())));
    }*/
/*
    @Test
    void testeCriarEtapa(){
        System.out.println("\n\n\nteste criar etapa:");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-13-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.novoTrackingViagem("Viagem1", "nop", "100000", "França");
        appManager.selecionaViagem("Viagem1");
        int nEtapas = appManager.getEtapasTrackingViagens().length;
        assertTrue(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "12:30",  "Coimbra", "10000", false).resultado());
        assertEquals(nEtapas + 1, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa sem data:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(null, "12:30",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa sem localização atual:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "12:30", "", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com horas negativas:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "-12:30",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com horas > 23:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "24:30",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com horas não numéricas:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "vinte",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com minutos negativos:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:-30",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com minutos > 59:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:60",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com minutos não numéricos:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:trinta",  "Lisboa", "10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com quilometros negativos:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:30",  "Lisboa", "-10000", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com quilometros inferiores aos quilometros atuais:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:30", "Lisboa", "1", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);

        System.out.println("teste criar etapa com quilometros nao numericos:");
        appManager.selecionaRegistoVeiculo("11-12-UU");
        appManager.selecionaViagem("Viagem1");
        nEtapas = appManager.getEtapasTrackingViagens().length;
        assertFalse(appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "20:30",  "Lisboa", "mil", false).resultado());
        assertEquals(nEtapas, appManager.getEtapasTrackingViagens().length);
    }*/

/*
    @Test
    void testeImportarParaEdicao(){
        System.out.println("\n\n\nexporta veiculo:");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-12-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        appManager.selecionaRegistoVeiculo("11-12-UU");
        Path path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/");
        assertTrue(appManager.exportarVeiculo(new File(path.toUri()), false)); // como edição/novo prop

        System.out.println("\n\n\nimporta veiculo:");
        appManager.eliminaRegistoVeiculo("11-12-UU");
        appManager.setSenhaDada("123");
        path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/11-12-UU/dados.txt");
        assertTrue(appManager.importaVeiculo(new File(path.toUri())));
    }

    @Test
    void testeImportarParaLeitura(){
        System.out.println("\n\n\nexporta veiculo:");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-12-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        appManager.selecionaRegistoVeiculo("11-12-UU");
        Path path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/");
        assertTrue(appManager.exportarVeiculo(new File(path.toUri()), true)); // como leitura

        System.out.println("\n\n\nimporta veiculo:");
        appManager.eliminaRegistoVeiculo("11-12-UU");
        appManager.setSenhaDada("123");
        path = Path.of("src/test/java/pt/isec/GPS/DigitalMechanics/Model/11-12-UU/dados.txt");
        assertTrue(appManager.importaVeiculo(new File(path.toUri())));
    }
*/
    @Test
    void testeCriarPausa(){
        System.out.println("\n\n\nteste criar pausa:");
        appManager.adicionaRegistoVeiculo("toyota", "rav4", "11-14-UU", "gasolina", 2000, 1, "1000", "");
        appManager.save();
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.novoTrackingViagem("Viagem2", "nop", "100000", "França");
        appManager.selecionaViagem("Viagem2");
        appManager.acrescentarEtapa(LocalDate.of(2023, 12, 12), "12:30",  "Coimbra", "10000", false);
        appManager.selecionaEtapa("Coimbra");
        assertTrue(appManager.adicionaPausa(LocalDate.of(2023, 12, 12), LocalDate.of(2023, 12, 13), "12:33",  "12:46",  "nada").resultado());

       System.out.println("\n\n\nteste criar pausa sem data inicio:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(null, LocalDate.of(2023, 12, 13), "12:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa sem data termino:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 13), null, "12:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa sem ambas as datas:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(null, null, "12:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com data termino inferior a data inicio:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 13), LocalDate.of(2023, 12, 11), "12:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas inicio negativas:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 12), LocalDate.of(2023, 12, 13), "-12:33", "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas inicio > 23:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "24:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas inicio nao numericas:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "doze:33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com mins inicio negativos:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:-33",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com mins inicio > 59:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:60",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com mins inicio nao numericos:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:trinta",  "12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas termino negativas:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 12), LocalDate.of(2023, 12, 13), "12:33",  "-12:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas termino > 23:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "23:33",  "24:46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com horas termino nao numericas:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:33",  "doze:46",  "nada").resultado());



        System.out.println("\n\n\nteste criar pausa com mins termino negativos:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:33",  "12:-46",  "nada").resultado());

        System.out.println("\n\n\nteste criar pausa com mins termino > 59:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:59",  "12:60",  "nada").resultado());

       System.out.println("\n\n\nteste criar pausa com mins termino nao numericos:");
        appManager.selecionaRegistoVeiculo("11-14-UU");
        appManager.selecionaViagem("Viagem2");
        appManager.selecionaEtapa("Coimbra");
        assertFalse(appManager.adicionaPausa(LocalDate.of(2023, 12, 11), LocalDate.of(2023, 12, 13), "12:30",  "12:quarenta",  "nada").resultado());


    }
}