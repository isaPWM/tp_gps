module com.example.DigitalMechanics {

    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;

    opens pt.isec.GPS.DigitalMechanics to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics;
    opens pt.isec.GPS.DigitalMechanics.UI to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.UI;
    opens pt.isec.GPS.DigitalMechanics.Model to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.Model;
    opens pt.isec.GPS.DigitalMechanics.Model.dataClasses to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.Model.dataClasses;
    opens pt.isec.GPS.DigitalMechanics.Model.dataRecords to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.Model.dataRecords;
    opens pt.isec.GPS.DigitalMechanics.UI.Buttons to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.UI.Buttons;
    opens pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem;
}