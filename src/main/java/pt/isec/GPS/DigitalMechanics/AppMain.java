package pt.isec.GPS.DigitalMechanics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pt.isec.GPS.DigitalMechanics.Model.AppManager;
import java.io.IOException;

public class AppMain extends Application {
    public static AppManager appManager;
    static {
        appManager = new AppManager();
    }
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(AppMain.class.getResource("AppDigitalMechanicsView.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.getIcons().add(new Image(String.valueOf(AppMain.class.getResource("Logo/DM2-LOGO.PNG"))));
        stage.setTitle("Digital & Mechanics");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        stage.setOnCloseRequest(event -> appManager.save());
    }

    public static void main(String[] args) {
        launch();
    }
}