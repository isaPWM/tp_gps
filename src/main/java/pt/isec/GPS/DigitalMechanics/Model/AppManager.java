package pt.isec.GPS.DigitalMechanics.Model;

import pt.isec.GPS.DigitalMechanics.Model.dataClasses.AppDigitalMechanics;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.History;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.InfoRetorno;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.Lembrete;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class AppManager {
    private static final String FILE = "src/main/resources/appFiles/AppDigitalMechanics.txt";
    private AppDigitalMechanics appDigitalMechanics;
    private History history;
    private PropertyChangeSupport atualizaLembretes;

    public AppManager() {
        atualizaLembretes = new PropertyChangeSupport(this);
        load();
    }

    //--------------------------- PROPERTIES --------------------------

    public void addListaLembretesChangeListener(String propertyName, PropertyChangeListener listener) {
        atualizaLembretes.addPropertyChangeListener(propertyName, listener);
    }

    public void addNotificacoesLembretesChangeListener(String propertyName, PropertyChangeListener listener) {
        atualizaLembretes.addPropertyChangeListener(propertyName, listener);
    }

    //--------------------------- GETTERS --------------------------
    public int getValidadeLembrete(LocalDate localDate, String datafinal){
        return appDigitalMechanics.getValidadeLembrete(localDate, datafinal);
    }
    public String getDadosVeiculo() {
        return appDigitalMechanics.getDadosVeiculo();
    }

    public String getDadosEtapa() {
        return appDigitalMechanics.getDadosEtapa();
    }
    public boolean getIsEtapaAlteravel() {
        return appDigitalMechanics.getIsEtapaAlteravel();
    }

    public String[] getNomesRegistosVeiculos() {
        return appDigitalMechanics.getNomesRegistosVeiculos();
    }

    public String[] getNomesBlocosInfo() {
        return appDigitalMechanics.getNomesBlocosInfo();
    }

    public String[] getFotoVeiculo() {
        return appDigitalMechanics.getFotoVeiculo();
    }

    public int getNumVeiculos() {
        return appDigitalMechanics.getNumVeiculos();
    }

    public int getNumImportacoes() {
        return appDigitalMechanics.getNumImportacoes();
    }

    public int getNumExportacoes() {
        return appDigitalMechanics.getNumExportacoes();
    }

    public String getNomeBlocoInfoAtual() {
        return appDigitalMechanics.getNomeBlocoInfoAtual();
    }

    public ArrayList<Record> getListaRegistosDeBlocoAtual() {
        return appDigitalMechanics.getListaRegistosDeBlocoAtual();
    }

    public Lembrete[] getListaLembretes() {
        return appDigitalMechanics.getListaLembretes();
    }

    public int getNumNotificacoes() {
        return appDigitalMechanics.getNumNotificacoes();
    }

    public int getListaLembretesSize() {
        return appDigitalMechanics.getListaLembretesSize();
    }

    public Record getRegistoAcedido() {
        return appDigitalMechanics.getRegistoAcedido();
    }

    public boolean eliminaLembrete(int id) {
        boolean res = appDigitalMechanics.eliminaLembrete(id);
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        atualizaLembretes.firePropertyChange("lista", null, null);
        return res;
    }

    public void mudaParaVisto(int indice) {
        appDigitalMechanics.mudaParaVisto(indice);
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        atualizaLembretes.firePropertyChange("verInfo", null, null);
    }

    public void mudaParaNaoVisto(int indice) {
        appDigitalMechanics.mudaParaNaoVisto(indice);
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
    }

    public void eliminaLembretesVistos() {
        appDigitalMechanics.eliminaLembretesVistos();
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        atualizaLembretes.firePropertyChange("lista", null, null);
    }

    public void adicionaLembreteNaApp(String nome, LocalDate data, int id) {
        appDigitalMechanics.adicionaLembreteNaApp(nome, data, id);
    }

    public void eliTodosLembretes() {
        appDigitalMechanics.limparLembretes();
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        atualizaLembretes.firePropertyChange("lista", null, null);
    }


    //--------------------------- SETTERS --------------------------
    public void setRegistoAcedido(Record registoAcedido) {
        appDigitalMechanics.setRegistoAtual(registoAcedido);
    }

    //------------------ ADIÇÃO / REMOÇÃO DE INFO ------------------
    public InfoRetorno adicionaRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, String quilometros, String foto) {
        return appDigitalMechanics.adicionaRegistoVeiculo(marca, modelo, matricula, combustivel, ano, mes, quilometros, foto);
    }

    public boolean adicionaBlocoInformacao(String nomeBlocoInfo) {
        return appDigitalMechanics.adicionaBlocoInformacao(nomeBlocoInfo);
    }

    public InfoRetorno adicionaAbastecimento(String preco, String litros, String quilometros, LocalDate data) {
        return appDigitalMechanics.adicionaAbastecimento(preco, litros, quilometros, data);
    }

    public InfoRetorno adiciona_edita_Peca(String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto, boolean EDICAO) {
        return appDigitalMechanics.adicionaPeca(nome, marca, modelo, preco, descricao, loja, data, foto, EDICAO);
    }

    public InfoRetorno adiciona_edita_MudancaOleo(LocalDate data, String text, String foto, String Oleo, LocalDate lembrete, boolean EDICAO) {
        return appDigitalMechanics.adicionaMudancaOleo(data, text, foto, Oleo, lembrete, EDICAO);
    }

    public InfoRetorno adiciona_edita_Manutencao(String tipo, LocalDate data, String preco, String observacao, LocalDate lembrete, boolean EDICAO) {
        return appDigitalMechanics.adicionaManutencao(tipo, data, preco, observacao, lembrete, EDICAO);
    }


    public InfoRetorno adiciona_edita_RegistoPersonalizado(String nome, LocalDate data, String preco, String descricao, boolean EDICAO) {
        return appDigitalMechanics.adicionaRegistoPersonalizado(nome, data, preco, descricao, EDICAO);
    }

    //------------------------- SELEÇÕES ---------------------------
    public boolean selecionaRegistoVeiculo(String nomeRegisto) {
        boolean aux = appDigitalMechanics.selecionaRegistoVeiculo(nomeRegisto);
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        atualizaLembretes.firePropertyChange("lista", null, null);
        return aux;
    }

    public boolean selecionaBlocoInformacao(String nomeBlocoInfo) {
        return appDigitalMechanics.selecionaBlocoInformacao(nomeBlocoInfo);
    }

    public void saveListaLembretes() {
        appDigitalMechanics.saveLista();
    }

    //------------------ MANIPULAÇÃO DE FICHEIROS ------------------
    public void save() {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE))) {
            oos.writeObject(appDigitalMechanics);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void load() {
        File dir = new File("src/main/resources/appFiles");
        if(!dir.isDirectory()){
            dir.mkdir();
            appDigitalMechanics = new AppDigitalMechanics();
        }
        else {
            File file = new File(FILE);
            if (file.isFile()) {
                try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                    appDigitalMechanics = (AppDigitalMechanics) ois.readObject();

                    //Verifica existencia dos dados
                    appDigitalMechanics.verificaExistenciaDados();
                } catch (IOException e) {
                    e.printStackTrace();
                    appDigitalMechanics = new AppDigitalMechanics();
                } catch (ClassNotFoundException e) {
                    appDigitalMechanics = new AppDigitalMechanics();
                }
            } else {
                appDigitalMechanics = new AppDigitalMechanics();
            }
        }
        history = new History();
    }

    public TipoBloco getTipoBlocoInfo() {
        return appDigitalMechanics.getTipoBlocoInfo();
    }

    public boolean eliminaInfo(Record registo) {
        atualizaLembretes.firePropertyChange("notificacoes", null, null);
        history.save(appDigitalMechanics);
        //Guardar backup de ficheiros (dentro de eliminaInfo)
        return appDigitalMechanics.eliminaInfo(registo);
    }

    public String getLembreSelecionado() {
        return appDigitalMechanics.getLembreSelecionado();
    }

    public boolean eliminaRegistoVeiculo(String matricula) {
        return appDigitalMechanics.eliminaRegistoVeiculo(matricula);
    }

    public boolean adicionaSenha(String senha) throws NoSuchAlgorithmException {
        return appDigitalMechanics.adicionaSenha(senha);
    }

    public boolean importaVeiculo(File arquivoSelecionado) {
        return appDigitalMechanics.importaVeiculo(arquivoSelecionado);
    }

    public InfoRetorno novoTrackingViagem(String nome, String observacao, String quilometrosViagem, String destinoViagem) {
        return appDigitalMechanics.novoTrackingViagem(nome, observacao, quilometrosViagem, destinoViagem);
    }

    public String [] getNomesTrackingViagens() {
        return appDigitalMechanics.getNomesTrackingViagens();
    }
    public int getNumTrackingViagens(){
        return appDigitalMechanics.getNumTrackingViagens();
    }

    public InfoRetorno acrescentarEtapa(LocalDate inicioEtapa, String horasInicioEtapa, String localizacaoAtual, String quilometrosInicioEtapa, boolean isUltimaEtapa) {
        return appDigitalMechanics.acrescentarEtapa(inicioEtapa, horasInicioEtapa, localizacaoAtual, quilometrosInicioEtapa, isUltimaEtapa);
    }

    public InfoRetorno adicionaPausa(LocalDate inicioPausa, LocalDate fimPausa, String horasInicioPausa,  String horasFimPausa,  String razaoPausa) {
        return appDigitalMechanics.adicionaPausa(inicioPausa, fimPausa, horasInicioPausa,  horasFimPausa, razaoPausa);
    }

    public String [] getEtapasTrackingViagens() {
        return appDigitalMechanics.getEtapasTrackingViagens();
    }

    public Boolean [] getIsEtapaFinal() { return appDigitalMechanics.getIsEtapaFinal(); }

    public void selecionaViagem(String nomeViagem) {
        appDigitalMechanics.selecionaViagem(nomeViagem);
    }

    public void selecionaEtapa(String localizacaoAtual) {
        appDigitalMechanics.selecionaEtapa(localizacaoAtual);
    }

    public boolean exportarVeiculo(File pastaSelecionada, boolean leitura) {
        return appDigitalMechanics.exportarVeiculo(pastaSelecionada, leitura);
    }

    public String getUltimoRegistoVeiculoAcedido() {
        return appDigitalMechanics.getUltimoRegistoVeiculoAcedido();
    }

    public void setSenhaDada(String senha) {
        appDigitalMechanics.aSenhaDada(senha);
    }

    public boolean undo() {
        if (history.undo(appDigitalMechanics)) {
            //Ler do Backup
            appDigitalMechanics.moveBlocoInformacao();
            return true;
        }
        return false;
    }
    public boolean getUndo() {
        return history.getUndo();
    }

    public void eliminaTrackingViagem(String trackingViagem) {
        appDigitalMechanics.eliminaTrackingViagem(trackingViagem);
    }
}