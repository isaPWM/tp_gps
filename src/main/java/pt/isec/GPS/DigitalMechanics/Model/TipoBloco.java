package pt.isec.GPS.DigitalMechanics.Model;

public enum TipoBloco {
    ABASTECIMENTOS, MANUTENCOES, PECAS, MUDANCAOLEO, OUTRO
}
