package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record Peca (int id, String nome, String marca, String modelo, double preco, String descricao, String loja, String data, String foto) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return "Registo:" + nome + "\n" +
                "Marca: " + marca + "\n" +
                "Modelo: " + modelo + "\n" +
                "Preco: " + preco + "€\n" +
                "Descrição: " + descricao + "\n" +
                "Loja: " + loja + "\n" +
                "Data: " + data + "\n" +
                "Foto: " + foto;
    }
}
