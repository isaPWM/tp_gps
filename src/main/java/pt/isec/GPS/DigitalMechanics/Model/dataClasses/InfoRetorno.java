package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

public record InfoRetorno(Boolean resultado, Boolean [] validadeParametros, String mensagem) {
}
