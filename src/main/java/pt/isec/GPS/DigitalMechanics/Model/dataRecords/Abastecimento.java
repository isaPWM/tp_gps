package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record Abastecimento(int id, double preco, double litros, long quilometros, String data) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString(){
        return
                "Preco: " + preco + "€\n" +
                "Litros: " + litros + "\n" +
                "Quilometros: " + quilometros + "\n" +
                "Data: " + data;
    }
}
