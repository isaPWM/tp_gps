package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import java.util.Stack;

import static pt.isec.GPS.DigitalMechanics.UI.AppDMController.undoButton;

public class History {
    Stack<AppDigitalMechanics> history = new Stack<>();
    public void save(AppDigitalMechanics registo) {
        history.push(registo);
        undoButton.setValue(getUndo());
    }

    public boolean undo(AppDigitalMechanics appDigitalMechanics) {
        if (!history.isEmpty()) {
            appDigitalMechanics.reStorePreviousState(history.pop());
            undoButton.setValue(getUndo());
            return true;
        } else
            System.out.println("History is empty");
        return false;
    }

    public boolean getUndo() {
        return !history.isEmpty();
    }
}
