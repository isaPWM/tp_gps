package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.TipoBloco;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Manutencao;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.MudancaOleo;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.RegistoGeral;
import pt.isec.GPS.DigitalMechanics.UI.toolsAccess.Encryptation;

import java.io.*;
import java.nio.file.*;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class AppDigitalMechanics implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private ArrayList<InfoRegistoVeiculo> listaRegistosVeiculos;
    private int indiceRegistoAtual;
    private ListaLembretes listaLembretesAtual;
    private BlocoInformacao blocoInformacao;
    private Record registoAcedido;
    private String SenhaDada;

    // UTILIZAÇÃO INFO
    private int numImportacoes, numExportacoes, numVeiculos;
    ////////////////////////////////////////////////////////

    public AppDigitalMechanics() {
        listaRegistosVeiculos = new ArrayList<>();
        indiceRegistoAtual = 0;
        numImportacoes = 0;
        numExportacoes = 0;
        numVeiculos = 0;
        blocoInformacao = null;
    }

    public void verificaExistenciaDados() {
        if(listaRegistosVeiculos.isEmpty())
            return;
        listaRegistosVeiculos.removeIf(infoRegistoVeiculo -> !infoRegistoVeiculo.verificaExistenciaDados());
    }

    public void reStorePreviousState(AppDigitalMechanics pop) {
        this.listaRegistosVeiculos = pop.listaRegistosVeiculos;
        this.indiceRegistoAtual = pop.indiceRegistoAtual;
        this.numImportacoes = pop.numImportacoes;
        this.numExportacoes = pop.numExportacoes;
        this.numVeiculos = pop.numVeiculos;
        this.blocoInformacao = pop.blocoInformacao;
        this.registoAcedido = pop.registoAcedido;
    }

    /*-------------------------GETTERS---------------*/
    public int getValidadeLembrete(LocalDate localDate, String datafinal){
        return blocoInformacao.getValidadeLembrete(localDate,datafinal);
    }

    public String getDadosVeiculo() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getDadosVeiculo();
    }
    public String getDadosEtapa() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getDadosEtapa();
    }
    public boolean getIsEtapaAlteravel() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getIsEtapaAlteravel();
    }
    public int getNumImportacoes() {
        return numImportacoes;
    }
    public int getNumExportacoes() {
        return numExportacoes;
    }
    public int getNumVeiculos() {
        return numVeiculos;
    }
    public String getNomeBlocoInfoAtual() {
        return blocoInformacao.getNomeBlocoInfo();
    }
    public String[] getNomesRegistosVeiculos() {
        String [] lista = new String[listaRegistosVeiculos.size()];
        listaRegistosVeiculos.forEach(infoRegistoVeiculo -> lista[listaRegistosVeiculos.indexOf(infoRegistoVeiculo)] = infoRegistoVeiculo.getNomeRegisto());
        return lista;
    }
    public String[] getFotoVeiculo() {
        String [] lista = new String[listaRegistosVeiculos.size()];
        listaRegistosVeiculos.forEach(infoRegistoVeiculo -> lista[listaRegistosVeiculos.indexOf(infoRegistoVeiculo)] = infoRegistoVeiculo.getFoto());
        return lista;
    }
    public String[] getNomesBlocosInfo() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getNomeBlocosInfo();
    }
    public ArrayList<Record> getListaRegistosDeBlocoAtual() {
        return blocoInformacao.getListaRegistos();
    }
    public TipoBloco getTipoBlocoInfo() {
        return blocoInformacao.getTipoBlocoInfo();
    }
    public Lembrete[] getListaLembretes() {
        return listaLembretesAtual == null ? new Lembrete[]{} : listaLembretesAtual.toArray();
    }
    public int getNumNotificacoes() {
        return listaLembretesAtual.getNumNotificacoes();
    }
    public int getListaLembretesSize() {
        return listaLembretesAtual.size();
    }

    public InfoRegistoVeiculo getCarroAtualForUndo() {
        return listaRegistosVeiculos.get(indiceRegistoAtual);
    }

    //fora do model
    public boolean adicionaLembreteNaApp(String nomeLembrete, LocalDate data, int id) {
        if(data == null || data.isBefore(LocalDate.now()) || nomeLembrete == null || nomeLembrete.isBlank())
            return false;
        listaLembretesAtual.addLembrete(nomeLembrete, data, id);
        return true;
    }

    public boolean eliminaLembrete(int id) {
        return listaLembretesAtual.remove(id);
    }
    public void mudaParaVisto(int indice) {
        listaLembretesAtual.mudaParaVisto(indice);
    }
    public void mudaParaNaoVisto(int indice) {
        listaLembretesAtual.mudaParaNaoVisto(indice);
    }
    public void eliminaLembretesVistos() {
        listaLembretesAtual.removeVistos();
    }
    public void limparLembretes() {
        listaLembretesAtual.clear();
    }

    /////////////////////////////////////////////////////////
    /*--------------------setters--------------------------*/
    public void setRegistoAtual(Record registoAcedido){this.registoAcedido = registoAcedido;}
    public Record getRegistoAcedido(){return registoAcedido;}

    //////////////////////////////////////////////////////////
    /*-------------------------VERIFICAÇÕES---------------*/
    private boolean matriculaValida(String matricula, int ano)
    {
        if(   (ano < 1992 && matricula.matches("^[A-Z]{2}+\\-+[0-9]{2}+\\-+[0-9]{2}+$"))
           || (ano >= 1992 && ano < 2005 && matricula.matches("^[0-9]{2}+\\-+[0-9]{2}+\\-+[A-Z]{2}+$"))
           || (ano >= 2005 && ano < 2020 && matricula.matches("^[0-9]{2}+\\-+[A-Z]{2}+\\-+[0-9]{2}+$"))
           || (ano >= 2020 && matricula.matches("^[A-Z]{2}+\\-+[0-9]{2}+\\-+[A-Z]{2}+$"))) {
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////
    /*-------------------------ADICIONA REGISTO---------------*/

    /*------------------------------VEÍCULO-------------------*/
    public InfoRetorno adicionaRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, String quilometros, String foto) {
        if(listaRegistosVeiculos.stream().anyMatch(reg -> reg.getNomeRegisto().equals(matricula))) {
            return new InfoRetorno(false, null, "O veículo " + matricula + " já está registado.");
        }

        String resultadoInfo = "";
        Boolean [] resultado = new Boolean[] {true, true, true, true, true, true, true};

        if(marca == null || marca.isBlank()) {
            resultadoInfo += "marca indefinida, ";
            resultado[0] = false;
        }
        if(modelo == null || modelo.isBlank()) {
            resultadoInfo += "modelo indefinido, ";
            resultado[1] = false;
        }
        if(matricula == null || matricula.isBlank()) {
            resultadoInfo += "matricula obrigatória, ";
            resultado[2] = false;
        }
        else if(!matriculaValida(matricula, ano)) {
            resultadoInfo += "formato matricula e data de emissão não coincidem, ";
            resultado[2] = false;
            resultado[5] = false;
            resultado[6] = false;
        }
        if(combustivel == null || combustivel.isBlank()) {
            resultadoInfo += "combustivel indefinido, ";
            resultado[3] = false;
        }
        if(quilometros == null || quilometros.isBlank()) {
            resultadoInfo += "quilometros vazio, ";
            resultado[4] = false;
        }
        else {
            try {
                long num = Long.parseLong(quilometros);
                if(num < 0) {
                    resultadoInfo += "quilometros devem ser positivos, ";
                    resultado[4] = false;
                }
            } catch (NumberFormatException e){
                resultadoInfo += "quilometros devem ser numéricos, ";
                resultado[4] = false;
            }
        }

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            try {
                InfoRegistoVeiculo infoRegistoVeiculo = new InfoRegistoVeiculo(marca, modelo, matricula, combustivel, ano, mes, Long.parseLong(quilometros), foto);
                listaRegistosVeiculos.add(infoRegistoVeiculo);
                numVeiculos++;
            } catch (Exception e) {
                return new InfoRetorno(false, null, "Erro ao criar registo de veículo");
            }
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    /*---------------------------ABASTECIMENTO----------------*/
    public InfoRetorno adicionaAbastecimento(String preco, String litros, String quilometros, LocalDate data) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res = blocoInformacao.adicionaAbastecimento(preco, litros, quilometros, data, listaRegistosVeiculos.get(indiceRegistoAtual).getQuilometros(), listaRegistosVeiculos.get(indiceRegistoAtual).getID_REGISTOS());
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*--------------------------------PEÇA--------------------*/
    public InfoRetorno adicionaPeca(String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto, boolean EDITAR) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res = blocoInformacao.adicionaPeca(nome, marca, modelo, preco, descricao, loja, data, foto, EDITAR ? ((RegistoGeral)registoAcedido).id() : listaRegistosVeiculos.get(indiceRegistoAtual).getID_REGISTOS(), EDITAR);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*--------------------------MUDANÇA DE ÓLEO---------------*/
    public InfoRetorno adicionaMudancaOleo(LocalDate data, String text, String foto,String Oleo, LocalDate lembrete, boolean EDITAR) {
        if(blocoInformacao == null)
            return null;

        if (lembrete != null && lembrete.isBefore(LocalDate.now())) {
            return new InfoRetorno(false, new Boolean[]{true, true, true}, "lembrete com data inválida, tente uma data no futuro!");
        }
        int id = EDITAR ? ((MudancaOleo)registoAcedido).id() : listaRegistosVeiculos.get(indiceRegistoAtual).getID_REGISTOS();
        InfoRetorno res =  blocoInformacao.adicionaMudancaOleo(data, text, foto, Oleo, id, EDITAR);

        if(lembrete != null)
            listaLembretesAtual.addLembrete("Mudança de óleo:" +data.toString() , lembrete, EDITAR ? ((MudancaOleo)registoAcedido).id() : id);

        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*-----------------------------MANUTENÇÃO-----------------*/
    public InfoRetorno adicionaManutencao(String tipo, LocalDate data, String preco, String observacao, LocalDate lembrete, boolean EDITAR) {
        if(blocoInformacao == null)
            return null;

        if (lembrete != null && lembrete.isBefore(LocalDate.now())) {
            return new InfoRetorno(false, new Boolean[]{true, true, true}, "lembrete com data inválida, tente uma data no futuro!");
        }

        int id = EDITAR ? ((Manutencao)registoAcedido).id() : listaRegistosVeiculos.get(indiceRegistoAtual).getID_REGISTOS();
        InfoRetorno res =  blocoInformacao.adicionaManutencao(tipo, data, preco, observacao, id, EDITAR);
        if(lembrete != null)
            listaLembretesAtual.addLembrete("Manutenção: "+data.toString() + tipo, lembrete, id);

        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }
    /*----------------------------PERSONALIZADO---------------*/
    public InfoRetorno adicionaRegistoPersonalizado(String nome, LocalDate data, String preco, String descricao, boolean EDITAR) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res =  blocoInformacao.adicionaRegistoPersonalizado(nome, data, preco, descricao, EDITAR ? ((RegistoGeral)registoAcedido).id() : listaRegistosVeiculos.get(indiceRegistoAtual).getID_REGISTOS(), EDITAR);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    ////////////////////////////////////////////////////////////
    /*-------------------------ADICIONA BLOCOS---------------*/
    public boolean adicionaBlocoInformacao(String nomeBloco) {
        if(listaRegistosVeiculos == null)
            return false;
        return listaRegistosVeiculos.get(indiceRegistoAtual).adicionaBlocoInformacao(nomeBloco, TipoBloco.OUTRO);
    }

    public boolean resaveBlocoInformacao() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).saveBlocoInformacao(blocoInformacao);
    }

    ////////////////////////////////////////////////////////////
    /*----------------------------SELEÇÕES-------------------*/
    public boolean selecionaRegistoVeiculo(String nomeRegisto) {
        if (listaRegistosVeiculos.isEmpty())
            return false;
        for(InfoRegistoVeiculo infoRegistoVeiculo : listaRegistosVeiculos)
            if(infoRegistoVeiculo.getNomeRegisto().equals(nomeRegisto)){
                indiceRegistoAtual = listaRegistosVeiculos.indexOf(infoRegistoVeiculo);
                listaLembretesAtual = listaRegistosVeiculos.get(indiceRegistoAtual).getListaLembretes();
                return listaRegistosVeiculos.get(indiceRegistoAtual).getLeitura();
            }
        return false;
    }
    public void saveLista() {
        listaRegistosVeiculos.get(indiceRegistoAtual).resetListaLembretes(listaLembretesAtual);
    }
    public boolean selecionaBlocoInformacao(String nomeBloco) {
        if(listaRegistosVeiculos.isEmpty())
            return false;
        blocoInformacao = listaRegistosVeiculos.get(indiceRegistoAtual).getBlocoInformacao(nomeBloco);
        return blocoInformacao != null;
    }

    public boolean eliminaInfo(Record registo) {
        if(blocoInformacao == null)
            return false;
        boolean v = false;

        //Guardar uma copia
        if (!this.listaRegistosVeiculos.get(indiceRegistoAtual).guardaCopiaFicheiros())
            return false;

        int id = blocoInformacao.eliminaInfo(registo);
        if(id >= 0) {
            listaLembretesAtual.remove(id);
            v = resaveBlocoInformacao();
        }
        saveLista();
        return v;
    }

    public boolean eliminaRegistoVeiculo(String matricula){
            File registoVeiculo = new File("src/main/resources/appFiles/" + matricula);

            if(registoVeiculo.exists()){
                System.out.println("A apagar registo de veiculo " + matricula);
                deleteDirectory(registoVeiculo);
                if(registoVeiculo.delete())
                    listaRegistosVeiculos.removeIf(reg -> reg.getNomeRegisto().equals(listaRegistosVeiculos.get(indiceRegistoAtual).getNomeRegisto()));
                return true;
            }
        return false;
    }
    public static void deleteDirectory(File file)
    {
        for (File subfile : file.listFiles()) {
            if (subfile.isDirectory()) {
                deleteDirectory(subfile);
            }
            subfile.delete();
        }
    }

    public String getLembreSelecionado() {
        return listaLembretesAtual.getLembreSelecionado();
    }

    public boolean adicionaSenha(String senha) throws NoSuchAlgorithmException {
        return listaRegistosVeiculos.get(indiceRegistoAtual).adicionaSenha(senha);
    }

    public boolean importaVeiculo(File arquivoSelecionado) {
        if(arquivoSelecionado.isFile()) {
            try {
                InfoRegistoVeiculo infoRegistoVeiculo = loadVeiculoFile(arquivoSelecionado);
                if(infoRegistoVeiculo != null) {
                    if (SenhaDada == null || !SenhaDada.isBlank()) {
                        SenhaDada = new Encryptation().encrypt(SenhaDada);
                        if (infoRegistoVeiculo.getSenhaRegisto() != null && SenhaDada.equals(infoRegistoVeiculo.getSenhaRegisto())) {
                            //Antes de gravar o veiculo, verifica se a senha coincide, reatribuir o path
                            //Caso queremos usar fora da pasta do projeto
                            //infoRegistoVeiculo.setFilePath(arquivoSelecionado.getAbsolutePath());
                            File diretorioVeiculo = new File(infoRegistoVeiculo.getFilePath());
                            if (diretorioVeiculo.exists()) //Não pode existir já um veiculo com o mesmo nome no appFiles
                                return false;
                            String path = AppMain.class.getResource("Logo/DM2-LOGO.png").toString();
                            path = path.replace("file:/", "");
                            int indexPath = path.indexOf("target");
                            path = path.substring(0, indexPath);
                            path += infoRegistoVeiculo.getFilePath();
                            Files.createDirectory(Path.of(path));
                            //Files.copy(arquivoSelecionado.toPath(), Path.of(path), StandardCopyOption.REPLACE_EXISTING);
                            //Remover do path o nome do ficheiro
                            String pathOrigem = arquivoSelecionado.toString().replace(arquivoSelecionado.getName(), "");
                            copyFolderContents(Path.of(pathOrigem), Path.of(path));

                            if (listaRegistosVeiculos.add(infoRegistoVeiculo)) {
                                numImportacoes++;
                                return true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void copyFolderContents(Path source, Path destination) throws IOException {
        FileVisitOption[] options = new FileVisitOption[]{FileVisitOption.FOLLOW_LINKS};

        Files.walk(source, Integer.MAX_VALUE, options)
                .forEach(sourcePath -> {
                    Path destinationPath = destination.resolve(source.relativize(sourcePath));
                    try {
                        Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private InfoRegistoVeiculo loadVeiculoFile(File arquivoSelecionado) {
        File file = new File(arquivoSelecionado.getAbsolutePath());
        if(file.isFile()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                InfoRegistoVeiculo infoRegistoVeiculo = (InfoRegistoVeiculo) ois.readObject();

                //Verifica existencia de um mesmo veiculo
                if(listaRegistosVeiculos.contains(infoRegistoVeiculo))
                    return null;
                return infoRegistoVeiculo;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    ////////////////////////////////////////////////////////////
    // ------------------------- TRACKING VIAGENS -------------------
    public InfoRetorno novoTrackingViagem(String nome, String observacao, String quilometrosViagem, String destinoViagem) {

        Boolean [] resultados = new Boolean[]{true, true, true};
        String resultadoInfo = "";

        if(nome == null || nome.isBlank()) {
            resultados[0] = false;
            resultadoInfo += "o tracking deve ter um nome, ";
        }

        try {
            long num = Long.parseLong(quilometrosViagem);
            if(num < 0) {
                resultados[2] = false;
                resultadoInfo += "os quilometros devem ser positivos, ";
            }
        } catch (NumberFormatException e){
            resultados[2] = false;
            resultadoInfo += "quilometros devem ser numéricos, ";
        }

        if(destinoViagem == null || destinoViagem.isBlank()) {
            resultados[1] = false;
            resultadoInfo += "o deve inserir um destino, ";
        }

        if(Arrays.toString(resultados).contains("false")) {
            return new InfoRetorno(false, resultados, resultadoInfo);
        } else {
            String res = listaRegistosVeiculos.get(indiceRegistoAtual).criaNovoTrackingViagem(nome, observacao, Long.parseLong(quilometrosViagem), destinoViagem);
            if(res == null)
                return new InfoRetorno(true, resultados, "Tracking criado com sucesso!");
            else
                return new InfoRetorno(false, resultados, res);
            }
    }

    public String [] getNomesTrackingViagens() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getNomesTrackingViagens();
    }

    public int getNumTrackingViagens(){
        return !listaRegistosVeiculos.isEmpty() ? listaRegistosVeiculos.get(indiceRegistoAtual).getNumTrackingViagens() : 0;
    }

    public InfoRetorno acrescentarEtapa(LocalDate inicioEtapa, String horasInicioEtapa, String localizacaoAtual, String quilometrosInicioEtapa, boolean isUltimaEtapa) {
        if(!horasInicioEtapa.contains(":")){
            Boolean[] r = new Boolean[]{false};
            return new InfoRetorno(false, r, null);

        }
        String[] partes = horasInicioEtapa.split(":");
        Boolean [] resultados = new Boolean[]{true, true, true, true, true};
        String resultadoInfo = "";

        if(inicioEtapa == null) {
            resultados[0] = false;
            resultadoInfo += "é necessária a data de inicio da etapa, ";
        }

        if(localizacaoAtual == null || localizacaoAtual.isBlank()) {
            resultados[1] = false;
            resultadoInfo += "é necessária a localização atual, ";
        }

        try {
            int num = Integer.parseInt(partes[0]);
            if(num < 0) {
                resultados[2] = false;
                resultadoInfo += "as horas devem ser positivas, ";
            }
            if(num > 23) {
                resultados[2] = false;
                resultadoInfo += "as horas não podem passar as 23, ";
            }
        } catch (Exception e){
            resultados[2] = false;
            resultadoInfo += "horas devem ser numéricas, ";
        }

        try {
            int num = Integer.parseInt(partes[1]);
            if(num < 0) {
                resultados[3] = false;
                resultadoInfo += "os minutos devem ser positivos, ";
            }
            if(num > 59) {
                resultados[3] = false;
                resultadoInfo += "os minutos não podem passar os 59, ";
            }
        } catch (Exception e){
            resultados[3] = false;
            resultadoInfo += "minutos devem ser numéricos, ";
        }

        try {
            long num = Long.parseLong(quilometrosInicioEtapa);
            if(num < 0) {
                resultados[4] = false;
                resultadoInfo += "os quilometros devem ser positivos, ";
            }

        } catch (NumberFormatException e){
            resultados[4] = false;
            resultadoInfo += "quilometros devem ser numéricos, ";
        }


        if(Arrays.toString(resultados).contains("false")) {
            return new InfoRetorno(false, resultados, resultadoInfo);
        } else {
            String res = listaRegistosVeiculos.get(indiceRegistoAtual).acrescentarEtapa(inicioEtapa, Integer.parseInt(partes[0]), Integer.parseInt(partes[1]), localizacaoAtual, Long.parseLong(quilometrosInicioEtapa), isUltimaEtapa);
            if(res == null)
                return new InfoRetorno(true, resultados, "Etapa criada com sucesso!");
            else
                return new InfoRetorno(false, resultados, res);
        }
    }

    public InfoRetorno adicionaPausa(LocalDate inicioPausa, LocalDate fimPausa, String horasInicioPausa, String horasFimPausa, String razaoPausa) {
        String[] partes = horasInicioPausa.split(":");
        String[] partes2 = horasFimPausa.split(":");
        if(!horasInicioPausa.contains(":") || !horasFimPausa.contains(":")){
            Boolean [] r = new Boolean[]{false, false, false, false, false, false};

            return new InfoRetorno(false, r, "Numero sem ':'");

        }
        Boolean [] resultados = new Boolean[]{true, true, true, true, true, true};
        String resultadoInfo = "";

        if(inicioPausa == null && fimPausa == null) {
            resultados[0] = false;
            resultadoInfo += "é necessária a data de inicio da pausa, ";
            resultados[1] = false;
            resultadoInfo += "é necessária a data de término da pausa, ";
        }else if(inicioPausa == null) {
            resultados[0] = false;
            resultadoInfo += "é necessária a data de inicio da pausa, ";
        }else if(fimPausa == null) {
            resultados[1] = false;
            resultadoInfo += "é necessária a data de término da pausa, ";
        }

        if(inicioPausa != null && fimPausa != null)
            if(fimPausa.isBefore(inicioPausa)){
                resultados[1] = false;
                resultadoInfo += "a data de término tem de ser depois da data de inicio, ";
            }

        if(partes.length != 2 && partes2.length != 2) {
            resultados[2] = false;
            resultadoInfo += "horas e minutos obrigatórios.";
        }
        else {
            try {
                int num = Integer.parseInt(partes[0]);
                if (num < 0) {
                    resultados[2] = false;
                    resultadoInfo += "as horas iniciais devem ser positivas, ";
                }
                if (num > 23) {
                    resultados[2] = false;
                    resultadoInfo += "as horas iniciais não podem passar as 23, ";
                }
            } catch (NumberFormatException e) {
                resultados[2] = false;
                resultadoInfo += "horas iniciais devem ser numéricas, ";
            }

            try {
                int num = Integer.parseInt(partes[1]);
                if (num < 0) {
                    resultados[3] = false;
                    resultadoInfo += "os minutos iniciais devem ser positivos, ";
                }
                else {
                    if (num > 59) {
                        resultados[3] = false;
                        resultadoInfo += "os minutos iniciais não podem passar os 59, ";
                    }
                    else {

                        try {
                            num = Integer.parseInt(partes2[0]);
                            if ((Integer.parseInt(partes2[0]) < Integer.parseInt(partes[0])
                                    || (Integer.parseInt(partes2[0]) == Integer.parseInt(partes[0]) && (Integer.parseInt(partes2[1]) < Integer.parseInt(partes[1]))))) {
                                resultados[4] = false;
                                resultadoInfo += "as horas finais devem ser após as de início, ";
                            } else {
                                if (num > 23) {
                                    resultados[4] = false;
                                    resultadoInfo += "as horas finais não podem passar as 23, ";
                                } else {
                                    if (num < Integer.parseInt(partes2[0]) && inicioPausa.equals(fimPausa)) {
                                        resultados[4] = false;
                                        resultadoInfo += "as horas finais não podem ser menores que as horas iniciais, ";
                                    } else {
                                        try {
                                            num = Integer.parseInt(partes2[1]);
                                            if (num < 0) {
                                                resultados[5] = false;
                                                resultadoInfo += "os minutos finais devem ser positivos, ";
                                            }
                                            if (num > 59) {
                                                resultados[5] = false;
                                                resultadoInfo += "os minutos finais não podem passar os 59, ";
                                            }
                                            if (num < Integer.parseInt(partes2[1]) && inicioPausa.equals(fimPausa) && Integer.parseInt(partes[0]) == Integer.parseInt(partes2[0])) {
                                                resultados[5] = false;
                                                resultadoInfo += "os minutos finais não podem ser menores que os minutos iniciais, ";
                                            }
                                        } catch (NumberFormatException e) {
                                            resultados[5] = false;
                                            resultadoInfo += "minutos finais devem ser numéricos, ";
                                        }
                                    }
                                }
                            }
                        } catch (NumberFormatException e) {
                            resultados[4] = false;
                            resultadoInfo += "horas finais devem ser numéricas, ";
                        }
                    }
                }
            } catch (NumberFormatException e) {
                resultados[3] = false;
                resultadoInfo += "minutos iniciais devem ser numéricos, ";
            }
        }
        if(Arrays.toString(resultados).contains("false")) {
            return new InfoRetorno(false, resultados, resultadoInfo);
        } else {
            String res = listaRegistosVeiculos.get(indiceRegistoAtual).adicionaPausa(inicioPausa, fimPausa, Integer.parseInt(partes[0]), Integer.parseInt(partes[1]), Integer.parseInt(partes2[0]), Integer.parseInt(partes2[1]), razaoPausa);
            if(res == null)
                return new InfoRetorno(true, resultados, "Pausa criada com sucesso!");
            else
                return new InfoRetorno(false, resultados, res);
        }
    }

    public String [] getEtapasTrackingViagens() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getEtapasTrackingViagens();
    }
    public Boolean [] getIsEtapaFinal(){
        return listaRegistosVeiculos.get(indiceRegistoAtual).getIsEtapaFinal();
    }
    public void selecionaViagem(String nomeViagem) {
        listaRegistosVeiculos.get(indiceRegistoAtual).selecionaViagem(nomeViagem);
    }

    public void selecionaEtapa(String localizacaoAtual) {
        listaRegistosVeiculos.get(indiceRegistoAtual).selecionaEtapa(localizacaoAtual);
    }

    public boolean exportarVeiculo(File pastaSelecionada, boolean leitura) {
            try {
                if (listaRegistosVeiculos.get(indiceRegistoAtual).exportarVeiculo(pastaSelecionada, leitura)) {
                    numExportacoes++;
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
    }

    public void aSenhaDada(String senha) {
        SenhaDada = senha;
    }

    public String getUltimoRegistoVeiculoAcedido() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getNomeRegisto();
    }

    public void moveBlocoInformacao() {
        listaRegistosVeiculos.get(indiceRegistoAtual).moveBlocoInformacao();
    }

    public void eliminaTrackingViagem(String tracking) {
        listaRegistosVeiculos.get(indiceRegistoAtual).eliminaTrackingViagem(tracking);
    }
}