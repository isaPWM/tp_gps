package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ListaLembretes implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private Lembrete ultimoLembrete;
    private ArrayList<Lembrete> lista;

    public ListaLembretes() {
        lista = new ArrayList<>();
        ultimoLembrete = null;
    }
    public ListaLembretes(ListaLembretes listaLembretesAtual) {
        ultimoLembrete = listaLembretesAtual.ultimoLembrete;
        lista = new ArrayList<>();
        lista.addAll(listaLembretesAtual.lista);
    }

    public int size() {
        return lista.size();
    }

    public int getNumNotificacoes() {
        AtomicInteger num = new AtomicInteger();
        lista.forEach(lembrete -> num.addAndGet((lembrete.isEstadoAberto() ? 0 : 1)));
        return num.get();
    }

    public Lembrete[] toArray() {
        return lista.toArray(new Lembrete[0]);
    }

    public void addLembrete(String novo, LocalDate data, int idRegistoRel) {
        lista.add(new Lembrete(novo, data, idRegistoRel));
        lista.sort(Lembrete::compareTo);
    }

    public void mudaParaVisto(int id) {
        for(Lembrete lembrete : lista)
            if(lembrete.getId() == id) {
                lembrete.setEstadoAberto(true);
                ultimoLembrete = lembrete;
                break;
            }
    }
    public void mudaParaNaoVisto(int id) {
        for(Lembrete lembrete : lista)
            if(lembrete.getId() == id) {
                lembrete.setEstadoAberto(false);
                break;
            }
    }
    public boolean remove(int id) {
        return lista.removeIf(lembrete -> lembrete.getId() == id);
    }

    public void removeVistos() {
        lista.removeIf(Lembrete::isEstadoAberto);
    }

    public void clear() {
        lista.clear();
    }

    public String getLembreSelecionado() {
        return ultimoLembrete != null ? ultimoLembrete.toString() : null;
    }
}
