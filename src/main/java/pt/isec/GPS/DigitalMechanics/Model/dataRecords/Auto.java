package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record Auto(String marca, String modelo, String matricula, String combustivel, int ano, int mes, String foto) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString(){
        return "Marca: " + marca + "\n" +
                "Modelo: " + modelo + "\n" +
                "Matricula: " + matricula + "\n" +
                "Combustivel: " + combustivel + "\n" +
                "Ano: " + ano + "\n" +
                "Mes: " + mes +
                "Foto: " + foto;
    }
}
