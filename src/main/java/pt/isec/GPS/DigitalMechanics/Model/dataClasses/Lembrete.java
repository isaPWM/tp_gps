package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

public class Lembrete implements Serializable, Comparable<Lembrete> {
    @Serial
    private static final long serialVersionUID = 1L;
    private final String nomeLembrete;
    private LocalDate data;
    private Boolean estadoAberto;
    private final int id;

    public Lembrete(String nome, LocalDate data, int id){
        this.estadoAberto = false;
        this.nomeLembrete = nome;
        this.data = data;
        this.id = id;
    }
    //-------------- GETTERS
    public String getNomeLembrete(){
        return nomeLembrete;
    }
    public LocalDate getData() {
        return data;
    }
    public boolean isEstadoAberto() {
        return estadoAberto;
    }
    public int getId(){
        return id;
    }
    //-------------- SETTERS
    public void setEstadoAberto(Boolean novoEstado) {
        estadoAberto = novoEstado;
    }
    public void setData(LocalDate novaData) {
        data = novaData;
    }
    //----------- COMPARE TO
    @Override
    public int compareTo(Lembrete o) {
        int res1 = data.compareTo(o.data);
        return res1 == 0 ?
                (nomeLembrete.compareTo(o.nomeLembrete) == 0 ?
                        (id < o.getId() ? -1 : 1)
                        : nomeLembrete.compareTo(o.nomeLembrete)
                )
                : res1;
    }
    @Override
    public String toString() {
        return "Não se esqueça de [" + nomeLembrete + "] no dia " + data + "!!";
    }
}
