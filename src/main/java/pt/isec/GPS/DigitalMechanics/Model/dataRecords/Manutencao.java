package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record Manutencao(int id, String tipo, String data, double preco, String observacao) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString(){
        return "Tipo: " + tipo + "\n" +
                "Data: " + data + "\n" +
                "Preco: " + preco + "€\n" +
                "Observação: " + observacao;
    }
}
