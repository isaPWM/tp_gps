package pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class TrackingViagem implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String nome, destino, observacoes;
    private long quilometrosViagem;
    private ArrayList<Etapa> etapas;
    private int indiceEtapa;
    public TrackingViagem(String nome, String observacoes, long quilometrosViagem, String destino){
        this.nome = nome;
        this.destino = destino;
        this.observacoes = observacoes;
        this.quilometrosViagem = quilometrosViagem;
        etapas = new ArrayList<>();
        indiceEtapa = 0;
    }
//--------------------------- SETTERS --------------------------
    public void editaNome(String nome){
        this.nome = nome;
    }
//--------------------------- GETTERS --------------------------
    public String getNome() {
        return nome;
    }
    public String getDestino() {
        return destino;
    }
    public String getObservacoes() {
        return observacoes;
    }
    public long getQuilometrosViagem() {
        return quilometrosViagem;
    }

    public String [] getEtapas() {
        String [] etapasInfo = new String[this.etapas.size()];
        etapas.forEach(etapa -> etapasInfo[etapas.indexOf(etapa)] = etapa.getLocalizacaoAtual());
        return etapasInfo;
    }

    public String [] getNomesEtapas() {
        String [] etapasInfo = new String[this.etapas.size()];
        etapas.forEach(etapa -> etapasInfo[etapas.indexOf(etapa)] = etapa.getNomeEtapa());
        return etapasInfo;
    }

    public Boolean [] getIsEtapaFinal(){
        Boolean [] etapasIsFinal = new Boolean[this.etapas.size()];
        etapas.forEach(etapa -> etapasIsFinal[etapas.indexOf(etapa)] = etapa.getIsEtapaFinal());
        return etapasIsFinal;
    }

    public String getEtapaPausas(int index) {
        String [] etapaPausasInfo = etapas.get(index).getPausas();
        return etapas.get(index).toString();
    }
    //--------------------------- UPDATES --------------------------
    public void acrescentarEtapa(LocalDate inicioEtapa, int horasInicioEtapa, int minInicioEtapa, String localizacaoAtual, long quilometrosInicioEtapa, boolean isUltimaEtapa){
        etapas.add(new Etapa(inicioEtapa.atTime(horasInicioEtapa, minInicioEtapa), localizacaoAtual, quilometrosInicioEtapa, isUltimaEtapa, etapas.size()+1));
    }

    public void adicionaPausa(LocalDate inicioPausa, LocalDate fimPausa, int horasInicioPausa, int minInicioPausa, int horasFimPausa, int minFimPausa, String razaoPausa){
        etapas.get(indiceEtapa).adicionaPausa(inicioPausa, fimPausa, horasInicioPausa, minInicioPausa, horasFimPausa, minFimPausa, razaoPausa);
    }

    public void selecionaEtapa(String nomeEtapa) {
        if (etapas.isEmpty())
            return;
        for(Etapa etapa_loc : etapas)
            if(etapa_loc.getNomeEtapa().equals(nomeEtapa)){
                indiceEtapa = etapas.indexOf(etapa_loc);
            }
    }

    public String getDadosEtapa() {
        return etapas.get(indiceEtapa).getDadosEtapa();
    }

    public boolean getIsEtapaAlteravel() {
        return etapas.size() - (indiceEtapa + 1) == 0;
    }


}
