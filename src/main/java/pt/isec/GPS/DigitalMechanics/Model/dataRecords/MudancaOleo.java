package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record MudancaOleo(int id, String data , String marca, String foto,Double oleo) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return "Marca: " + marca + "\n" +
                "Data: " + data + "\n" +
                "Oleo: " + oleo + "\n" +
                "Foto: " + foto;
    }
    
    private String getMarca(){
        return marca;
    }
    
    private String getData(){
        return data;
    }
    
    private String getFoto(){
        return foto;
    }
    
    private Double getOleo(){
        return oleo();
    }
}
