package pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem;

import java.io.Serial;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;

public record Pausa(LocalDateTime horaInicio, LocalDateTime horaFim, String razaoPausa) implements Serializable {
     @Serial
     private static final long serialVersionUID = 1L;
     protected String getDuracaoPausa(){
        return Duration.between(horaInicio, horaFim).toString();
     }
}
