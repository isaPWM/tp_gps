package pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Etapa implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private LocalDateTime inicio, fim;
    private String localizacaoAtual;
    private long quilometrosInicioEtapa;
    private ArrayList<Pausa> pausas;
    private boolean isUltimaEtapa;
    private String nomeEtapa;

    protected Etapa(LocalDateTime inicio, String localizacaoAtual, long quilometrosInicioEtapa, boolean isUltimaEtapa, int nEtapa){
        this.inicio = inicio;
        this.localizacaoAtual = localizacaoAtual;
        this.quilometrosInicioEtapa = quilometrosInicioEtapa;
        this.isUltimaEtapa = isUltimaEtapa;
        nomeEtapa = localizacaoAtual + " (Etapa " + nEtapa + ")";
        pausas = new ArrayList<>();
    }

    //--------------------------- GETTERS --------------------------

    protected String getLocalizacaoAtual() {
        return localizacaoAtual;
    }
    protected String getNomeEtapa() {
        return nomeEtapa;
    }

    protected Boolean getIsEtapaFinal(){return isUltimaEtapa;}

    protected void adicionaPausa(LocalDate inicioPausa, LocalDate fimPausa, int horasInicioPausa, int minInicioPausa, int horasFimPausa, int minFimPausa, String razaoPausa){
        pausas.add(new Pausa(inicioPausa.atTime(horasInicioPausa, minInicioPausa), fimPausa.atTime(horasFimPausa, minFimPausa), razaoPausa));
    }

    protected String[] getPausas() {
        String [] infoPausas = new String[pausas.size()];
        pausas.forEach(p -> infoPausas[pausas.indexOf(p)] = p.toString());
        return infoPausas;
    }
    protected String getDadosEtapa(){
        String aux;
        if(pausas.size() > 0) {
            aux = "Data de inicio: " + inicio + "\n" +
                    "Localização atual: " + localizacaoAtual + "\n" +
                    "Quilómetros no inicio: " + quilometrosInicioEtapa + " km\n\nPAUSAS:\n";
            for (int i = 0; i < pausas.size(); i++) {
                aux += "Pausa " + (i + 1) + ": " + pausas.get(i).getDuracaoPausa() + "\n";
                if(!pausas.get(i).razaoPausa().isBlank())
                    aux += "Razão da pausa: " + pausas.get(i).razaoPausa();
                aux += "\n. . . . . . . . . . .\n ";
            }

        }else{
            aux = "Data de inicio: " + inicio + "\n" +
                    "Localização atual: " + localizacaoAtual + "\n" +
                    "Quilómetros no inicio: " + quilometrosInicioEtapa + " km\n";
        }

        return aux;
    }
}
