package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import pt.isec.GPS.DigitalMechanics.Model.TipoBloco;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

public class BlocoInformacao implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private final TipoBloco tipoBloco;
    private final String nome;
    private String resultadoInfo;
    private ArrayList<Record> listaRegistos;

    public BlocoInformacao(TipoBloco tipo, String nome) {
        tipoBloco = tipo;
        this.nome = nome;
        listaRegistos = new ArrayList<>();
    }

    //////////////////////////////////////////////////////////
    /*-------------------------GETTERS---------------*/
    public int getValidadeLembrete(LocalDate localDate,String datafinal) {
        for (int i = 0; i < listaRegistos.size(); i++) {
            Record registro = listaRegistos.get(i);
            System.out.println(listaRegistos.get(i));

            if (registro instanceof MudancaOleo) {
                MudancaOleo mudancaOleo = (MudancaOleo) registro;

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate dataMudancaOleo = LocalDate.parse(mudancaOleo.data(), formatter);
                int diasDeDiferenca = localDate.until(dataMudancaOleo).getDays();
                if(datafinal.contains(mudancaOleo.data())){
                    return diasDeDiferenca;
                }
                //System.out.println("Diferença de dias: " + diasDeDiferenca);
            } else if (registro instanceof  Manutencao) {
                System.out.println("Entrou");
                Manutencao manutencao = (Manutencao) registro;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate dataMudancaOleo = LocalDate.parse(manutencao.data(), formatter);
                int diasDeDiferenca = localDate.until(dataMudancaOleo).getDays();
                if(datafinal.contains(manutencao.data())){
                    return diasDeDiferenca;
                }

            }
        }
        return 0;
    }
    public TipoBloco getTipoBlocoInfo() {
        return tipoBloco;
    }
    public String getNomeBlocoInfo() {
        return nome;
    }
    public ArrayList<Record> getListaRegistos() {
        ArrayList<Record> aux = new ArrayList<>();
        if (!listaRegistos.isEmpty()) {
            aux.addAll(listaRegistos);
        }
        return aux;
    }

    //////////////////////////////////////////////////////////
    /*-------------------------VERIFICAÇÕES---------------*/
    public boolean verificacaoNumericoLong(String tipo, String input, Long comparacao) {
        if(input == null || input.isBlank()) {
            resultadoInfo += (tipo + " vazio, ");
            return false;
        }
        else {
            try {
                long num = Long.parseLong(input);
                if(num < 0) {
                    resultadoInfo += (tipo + " deve ser positivo, ");
                    return false;
                }
                if(num < comparacao) {
                    resultadoInfo += ("[" + tipo + "] deve ser superior a " + comparacao + ", ");
                    return false;
                }
            } catch (NumberFormatException e){
                resultadoInfo += (tipo + " não é numérico, ");
                return false;
            }
        }
        return true;
    }
    public boolean verificacaoNumericoDouble(String tipo, String input) {
        if(input == null || input.isBlank()) {
            resultadoInfo += (tipo +" vazio, ");
            return false;
        }
        else {
            try {
                double num = Double.parseDouble(input);
                if(num < 0) {
                    resultadoInfo += (tipo + " deve ser positivo, ");
                    return false;
                }
            } catch (NumberFormatException e){
                resultadoInfo += (tipo + " não é numérico, ");
                return false;
            }
        }
        return true;
    }
    public boolean verificacaoString(String parametro) {
        return !(parametro == null || parametro.isBlank());
    }

    ////////////////////////////////////////////////////////////
    /*-------------------------ADICIONA REGISTO---------------*/

    /*--------------------------ABASTECIMENTOS----------------*/
    public InfoRetorno adicionaAbastecimento(String preco, String litros, String quilometros, LocalDate data, long quilometrosAtuais, int id) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[4];
        String auxData = null;

        resultado[0] = verificacaoNumericoDouble("preço", preco);
        resultado[1] = verificacaoNumericoDouble("litros", litros);
        resultado[2] = verificacaoNumericoLong("quilometros", quilometros, quilometrosAtuais);

        if((resultado[3] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            listaRegistos.add(new Abastecimento(id, Double.parseDouble(preco), Double.parseDouble(litros), Long.parseLong(quilometros), auxData));
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }
    /*------------------------------PECAS---------------------*/
    public InfoRetorno adicionaPeca(String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto, int id, boolean EDICAO){
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[4];
        String auxData;

        resultado[0] = verificacaoString(nome);
        resultado[1] = verificacaoString(marca);
        resultado[2] = verificacaoString(modelo);
        resultado[3] = verificacaoNumericoDouble("preço", preco);

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (descricao  == null || descricao.isBlank())
                descricao = "Sem descrição registada";

            if (loja == null || loja.isBlank())
                loja = "Sem loja registada";

            if (data == null)
                auxData = "Sem data registada";
            else
                auxData = data.toString();

            if (foto == null || foto.isBlank())
                foto = "Sem foto registada";

            else if (foto.contains("file:/"))
                foto = foto.replace("file:/", "");

            //se for edicao
            if(EDICAO)
                listaRegistos.removeIf(r -> ((Peca)r).id() == id);

            listaRegistos.add(new Peca(id, nome, marca, modelo, Double.parseDouble(preco), descricao, loja, auxData, foto));
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    /*---------------------------MANUTENÇÕES------------------*/
    public InfoRetorno adicionaManutencao(String tipo, LocalDate data, String preco, String observacao, int id, boolean EDICAO){
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[3];
        String auxData = null;

        resultado[0] = verificacaoString(tipo);
        resultado[2] = verificacaoNumericoDouble("preco", preco);

        if((resultado[1] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (observacao == null || observacao.isBlank())
                observacao = "Sem descrição registada!";

            //se for edicao
            if(EDICAO)
                listaRegistos.removeIf(r -> ((Manutencao)r).id() == id);

            listaRegistos.add(new Manutencao(id, tipo, auxData, Double.parseDouble(preco), observacao));
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    /*-------------------------MUDANÇA DE ÓLEO----------------*/
    public InfoRetorno adicionaMudancaOleo(LocalDate data, String text, String foto,String Oleo, int id, boolean EDICAO) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[2];
        String auxData = null;

        if((resultado[0] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";
        resultado[1] = verificacaoString(text);

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (foto == null || foto.isBlank())
                foto = "Sem foto registada";
            else if (foto.contains("file:/"))
                foto = foto.replace("file:/", "");

            //se for edicao
            if(EDICAO)
                listaRegistos.removeIf(r -> ((MudancaOleo)r).id() == id);

            listaRegistos.add(new MudancaOleo(id, auxData, text,foto,Double.parseDouble(Oleo)));
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    /*------------------------------OUTRO---------------------*/
    public InfoRetorno adicionaRegistoPersonalizado(String nome, LocalDate data, String preco, String descricao, int id, boolean EDICAO) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[3];
        String auxData = null;

        resultado[0] = verificacaoString(nome);
        resultado[2] = verificacaoNumericoDouble("preco", preco);

        if((resultado[1] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";;

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (descricao == null || descricao.isBlank())
                descricao = "Sem descrição registada!";

            //se for edicao
            if(EDICAO)
                listaRegistos.removeIf(r -> ((RegistoGeral)r).id() == id);

            listaRegistos.add(new RegistoGeral(id, nome, auxData, Double.parseDouble(preco), descricao));
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    public int eliminaInfo(Record registo) {
        int id = -1;
        if(registo instanceof Abastecimento a) {
            listaRegistos.removeIf(r -> ((Abastecimento) r).id() == a.id());
            id = a.id();
        }if(registo instanceof Manutencao m) {
            listaRegistos.removeIf(r -> ((Manutencao) r).id() == m.id());
            id = m.id();
        }if(registo instanceof MudancaOleo o) {
            listaRegistos.removeIf(r -> ((MudancaOleo) r).id() == o.id());
            id = o.id();
        }if(registo instanceof Peca p) {
            listaRegistos.removeIf(r -> ((Peca) r).id() == p.id());
            id = p.id();
        }if(registo instanceof RegistoGeral p) {
            listaRegistos.removeIf(r -> ((RegistoGeral) r).id() == p.id());
            id = p.id();
        }
        System.out.println("ID: " + id);
        return id;
    }

    public TipoBloco getTipo() {
        return tipoBloco;
    }
}

/*
    public InfoRetorno editarRegistoP(String nome, LocalDate data, String preco, String descricao, int id) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[3];
        String auxData = null;

        resultado[0] = verificacaoString(nome);
        resultado[2] = verificacaoNumericoDouble("preco", preco);

        if((resultado[1] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";;

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (descricao == null || descricao.isBlank())
                descricao = "Sem descrição registada!";

        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    public InfoRetorno editarMudancaOleo(LocalDate data, String text, String foto,String Oleo, int id, boolean EDICAO) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[2];
        String auxData = null;

        if((resultado[0] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";
        resultado[1] = verificacaoString(text);

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (foto == null || foto.isBlank())
                foto = "Sem foto registada";
            else if (foto.contains("file:/"))
                foto = foto.replace("file:/", "");
            if (listaRegistos.remove(mudanca)){
                listaRegistos.add(new MudancaOleo(auxData, text, foto, Double.parseDouble(Oleo)));
            }
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }


    public InfoRetorno editaManutencao(String tipo, LocalDate data, String preco, String observacao, int id){
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[3];
        String auxData = null;

        resultado[0] = verificacaoString(tipo);
        resultado[2] = verificacaoNumericoDouble("preco", preco);

        if((resultado[1] = (data != null)))
            auxData = data.toString();
        else
            resultadoInfo += "data vazia, ";

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (observacao == null || observacao.isBlank())
                observacao = "Sem descrição registada!";

            if(listaRegistos.removeIf(m -> ((Manutencao) m).id() == id)) {
                listaRegistos.add(new Manutencao(id, tipo, auxData, Double.parseDouble(preco), observacao));
            }
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }

    public InfoRetorno editaPeca(Record peca,String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto, int id) {
        resultadoInfo = "";
        Boolean [] resultado = new Boolean[4];
        String auxData;

        resultado[0] = verificacaoString(nome);
        resultado[1] = verificacaoString(marca);
        resultado[2] = verificacaoString(modelo);
        resultado[3] = verificacaoNumericoDouble("preço", preco);

        if(Arrays.toString(resultado).contains("false")) {
            return new InfoRetorno(false, resultado, resultadoInfo);
        } else {
            if (descricao  == null || descricao.isBlank())
                descricao = "Sem descrição registada";

            if (loja == null || loja.isBlank())
                loja = "Sem loja registada";

            if (data == null)
                auxData = "Sem data registada";
            else
                auxData = data.toString();

            if (foto == null || foto.isBlank())
                foto = "Sem foto registada";

            else if (foto.contains("file:/"))
                foto = foto.replace("file:/", "");

            if(listaRegistos.remove(peca)) {
                listaRegistos.add(new Peca(id, nome, marca, modelo, Double.parseDouble(preco), descricao, loja, auxData, foto));
            }
        }
        return new InfoRetorno(true, resultado, resultadoInfo);
    }
    */