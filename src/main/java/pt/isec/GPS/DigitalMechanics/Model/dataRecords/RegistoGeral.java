package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record RegistoGeral(int id, String nomeRegisto, String data, double preco, String descricao) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString(){
        return "Nome: " + nomeRegisto + "\n" +
                "Data: " + data + "\n" +
                "Preco: " + preco + "€\n" +
                "Descricao: " + descricao;
    }
}
