package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import pt.isec.GPS.DigitalMechanics.Model.TipoBloco;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem.TrackingViagem;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Auto;
import pt.isec.GPS.DigitalMechanics.UI.toolsAccess.Encryptation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InfoRegistoVeiculo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String FILE = "src/main/resources/appFiles/";
    private String imagemPerfilRegisto;
    private Auto veiculo;
    private long quilometros;
    private Map<String, String> mapAcessoBlocosInfo;
    private ListaLembretes listaLembretes;
    private String senhaRegisto;
    private ArrayList<TrackingViagem> trackingViagens;
    private int ID_REGISTOS, indiceViagem;
    private boolean LEITURA;

    public InfoRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, Long quilometros, String foto) throws Exception {

        FILE += (matricula + "/");
        if (!new File(FILE).mkdirs())
            throw new Exception("Erro ao criar diretoria");
        veiculo = new Auto(marca, modelo, matricula, combustivel, ano, mes, foto);
        this.quilometros = quilometros;
        imagemPerfilRegisto = null;
        indiceViagem = 0;

        mapAcessoBlocosInfo = new HashMap<>();
        adicionaBlocoInformacao("Manutenções", TipoBloco.MANUTENCOES);
        adicionaBlocoInformacao("Mudança Óleo", TipoBloco.MUDANCAOLEO);
        adicionaBlocoInformacao("Peças", TipoBloco.PECAS);
        adicionaBlocoInformacao("Abastecimentos", TipoBloco.ABASTECIMENTOS);
        listaLembretes = new ListaLembretes();
        trackingViagens = new ArrayList<>();

        ID_REGISTOS = 0;
    }

    public HashMap<String, String> getHash() {
        return (HashMap<String, String>) mapAcessoBlocosInfo;
    }

    public void setHash(HashMap<String, String> hash) {
        mapAcessoBlocosInfo = hash;
    }

    public boolean verificaExistenciaDados(){
        if (!(new File(FILE).exists())) {
            return false;
        }

        if(mapAcessoBlocosInfo.isEmpty())
            return true;
        for (String ficheiroBlocoInformacao : mapAcessoBlocosInfo.values())
            if(!(new File(ficheiroBlocoInformacao)).isFile())
                mapAcessoBlocosInfo.remove(ficheiroBlocoInformacao);

        return true;
    }
    //--------------------------- GETTERS --------------------------
    public int getID_REGISTOS() {
        System.out.println("ID_REGISTOS: " + ID_REGISTOS);
        return ID_REGISTOS++;
    }
    public String getDadosVeiculo() {
        return veiculo.toString() + "\nQuilómetros atuais: " + quilometros;
    }
    public String getDadosEtapa() {
        return trackingViagens.get(indiceViagem).getDadosEtapa();
    }
    public boolean getIsEtapaAlteravel() {
        return trackingViagens.get(indiceViagem).getIsEtapaAlteravel();
    }
    public long getQuilometros() {
        return quilometros;
    }

    public String[] getNomeBlocosInfo(){
        String [] lista = new String[mapAcessoBlocosInfo.size()];
        mapAcessoBlocosInfo.keySet().toArray(lista);
        return lista;
    }

    public String getNomeRegisto(){
        return veiculo.matricula();
    }

    public String getFoto() { return veiculo.foto();}
    public BlocoInformacao getBlocoInformacao(String nomeBlocoInfo){
        File ficheiro = new File(mapAcessoBlocosInfo.get(nomeBlocoInfo));
        if(ficheiro.isFile()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ficheiro))) {
                return (BlocoInformacao) ois.readObject();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            catch (ClassNotFoundException e) {
                return null;
            }
        }
        return null;
    }

    public ListaLembretes getListaLembretes() {
        return new ListaLembretes(listaLembretes);
    }
    public void resetListaLembretes(ListaLembretes listaLembretesAtual) {
        listaLembretes = new ListaLembretes(listaLembretesAtual);
    }
    //------------------ ADIÇÃO / REMOÇÃO DE INFO ------------------
    public boolean adicionaBlocoInformacao(String nomeBlocoInfo, TipoBloco tipo) {
        try {
            mapAcessoBlocosInfo.put(nomeBlocoInfo, FILE + nomeBlocoInfo + ".txt");
            return saveBlocoInformacao(new BlocoInformacao(tipo, nomeBlocoInfo));
        } catch (Exception e) {
            return false;
        }
    }
    public boolean saveBlocoInformacao(BlocoInformacao bloco) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE + bloco.getNomeBlocoInfo() + ".txt"))){
            oos.writeObject(bloco);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean guardaCopiaFicheiros() {
        for (String nomeBlocoInfo : mapAcessoBlocosInfo.keySet()) {
            BlocoInformacao blocoInformacao = getBlocoInformacao(nomeBlocoInfo);
            if (blocoInformacao != null) {
                File destino = new File(FILE, this.veiculo.matricula() + "Backup");
                destino.mkdir();
                try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(destino.getPath() + "/" + blocoInformacao.getNomeBlocoInfo() + ".txt"))){
                    oos.writeObject(blocoInformacao);
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return true;
    }

    public boolean adicionaSenha(String senha) throws NoSuchAlgorithmException {
        //Encrypt
        Encryptation encrypt = new Encryptation();
        senha = encrypt.encrypt(senha);
        if(senha == null)
            return false;
        senhaRegisto = senha;
        return true;
    }

    public boolean verificaSenha(String senhaColacadaParaAdicionar) throws NoSuchAlgorithmException {
        Encryptation encrypt = new Encryptation();
        if (senhaColacadaParaAdicionar == null)
            return false;
        return encrypt.encrypt(senhaRegisto).equals(senhaColacadaParaAdicionar);
    }
    //--------------------------- TRACKING VIAGEM --------------------------
    public String criaNovoTrackingViagem(String nome, String observacoes, long quilometrosViagem, String destino){
        String finalNome = nome.trim();
        if(trackingViagens.stream().anyMatch(tv -> tv.getNome().equalsIgnoreCase(finalNome)))
            return "Já existe uma viagem com esse nome!";
        trackingViagens.add(new TrackingViagem(finalNome, observacoes, quilometrosViagem, destino));
        return null;
    }

    public String[] getNomesTrackingViagens(){
        String [] lista = new String[trackingViagens.size()];
        trackingViagens.forEach(tv -> lista[trackingViagens.indexOf(tv)] = tv.getNome());
        return lista;
    }
    public int getNumTrackingViagens(){
        return trackingViagens.size();
    }

    public String acrescentarEtapa(LocalDate inicioEtapa, int horasInicioEtapa, int minInicioEtapa, String localizacaoAtual, long quilometrosInicioEtapa, boolean isUltimaEtapa){
        for(String aux : trackingViagens.get(indiceViagem).getEtapas())
            if(aux.equals(localizacaoAtual))
                return "Já existe uma etapa com essa localização!";
        trackingViagens.get(indiceViagem).acrescentarEtapa(inicioEtapa, horasInicioEtapa, minInicioEtapa, localizacaoAtual, quilometrosInicioEtapa, isUltimaEtapa);
        return null;
    }

    public String adicionaPausa(LocalDate inicioPausa, LocalDate fimPausa, int horasInicioPausa, int minInicioPausa, int horasFimPausa, int minFimPausa, String razaoPausa){
        trackingViagens.get(indiceViagem).adicionaPausa(inicioPausa, fimPausa, horasInicioPausa, minInicioPausa, horasFimPausa, minFimPausa, razaoPausa);
        return null;
    }

    public String[] getEtapasTrackingViagens(){
        return trackingViagens.get(indiceViagem).getNomesEtapas();
    }

    public Boolean[] getIsEtapaFinal(){
        return trackingViagens.get(indiceViagem).getIsEtapaFinal();
    }

    public void selecionaViagem(String nomeViagem) {
        if (trackingViagens.isEmpty())
            return;
        for(TrackingViagem viagem : trackingViagens)
            if(viagem.getNome().equals(nomeViagem)){
                indiceViagem = trackingViagens.indexOf(viagem);
            }
    }

    public void selecionaEtapa(String localizacaoAtual) {
        trackingViagens.get(indiceViagem).selecionaEtapa(localizacaoAtual);
    }

    public boolean exportarVeiculo(File pastaSelecionada, boolean leitura) {
        try {
            String nomePasta = this.veiculo.matricula();
            this.senhaRegisto = new Encryptation().encrypt("123"); //teste only
            File destino = new File(pastaSelecionada, nomePasta);
            destino.mkdir();

            File arquivoDestino = new File(destino, "dados.txt");
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(arquivoDestino))) {
                LEITURA = leitura;
                oos.writeObject(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (String nomeBlocoInfo : mapAcessoBlocosInfo.keySet()) {
                File arquivoOrigem = new File(mapAcessoBlocosInfo.get(nomeBlocoInfo));
                File arquivoDestino2 = new File(destino, nomeBlocoInfo + ".txt");
                Files.copy(arquivoOrigem.toPath(), arquivoDestino2.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
            return true;
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void setFilePath(String path) {
        FILE = path;
    }
    public String getFilePath() {
        return FILE;
    }

    public String getSenhaRegisto() {
        return senhaRegisto;
    }

    public void moveBlocoInformacao() {
        for (String nomeBlocoInfo : mapAcessoBlocosInfo.keySet()) {
            File arquivoOrigem = new File(FILE + this.veiculo.matricula() + "Backup/" + nomeBlocoInfo + ".txt");
            File arquivoDestino = new File(FILE, nomeBlocoInfo + ".txt");
            try {
                Files.move(arquivoOrigem.toPath(), arquivoDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getLeitura() {return LEITURA;}

    public void eliminaTrackingViagem(String tracking) {
        trackingViagens.removeIf(tv -> tv.getNome().equals(tracking));
    }
}