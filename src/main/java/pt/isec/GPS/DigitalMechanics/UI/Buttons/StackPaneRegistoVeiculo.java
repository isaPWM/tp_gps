package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import pt.isec.GPS.DigitalMechanics.AppMain;
import javafx.scene.control.Button;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

import java.io.File;

public class StackPaneRegistoVeiculo extends StackPane {
    private final FlowPane listaBlocosInfoFlowPane, listaRegistosInfoScrollPane;
    private final Pane blocosInfoPane, listaRegistosInfoPane;
    private final Label tipoRegisto, textoInfoBox;
    private final VBox infoBox;
    private Button btnEditar, btnRegistoVeiculo;
    private BorderPane editarRegistoVeiculoPane;

    public StackPaneRegistoVeiculo(String registo, String foto, Pane blocosInfoPane, FlowPane listaBlocosInfoFlowPane, Pane listaRegistosInfoPane, FlowPane listaRegistosInfoScrollPane, Label tipoRegisto, Label textoInfoBox, VBox infoBox, BorderPane editarRegistoVeiculoPane){
        this.listaRegistosInfoPane = listaRegistosInfoPane;
        this.blocosInfoPane = blocosInfoPane;
        this.listaBlocosInfoFlowPane = listaBlocosInfoFlowPane;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.tipoRegisto = tipoRegisto;
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        this.editarRegistoVeiculoPane = editarRegistoVeiculoPane;
        createViews(registo, foto);
        registerHandlers();
        update();
    }
    private void createViews(String registo, String foto) {
        btnEditar = new Button();
        btnEditar.setPrefSize(30, 30);
        btnEditar.setStyle("-fx-background-color: transparent");
        ImageView iv = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/info2.png"))));
        iv.setFitHeight(30);
        iv.setFitWidth(30);
        btnEditar.setGraphic(iv);

        btnRegistoVeiculo = new Button();

        File image = new File(foto);
        ImageView imageView;
        if (image.exists()) {
            imageView = new ImageView(new Image(image.toURI().toString()));
            imageView.setFitHeight(60);
            imageView.setPreserveRatio(true);
            this.getChildren().add(imageView);
            setAlignment(imageView, Pos.CENTER);
        }
        btnRegistoVeiculo.setPrefHeight(90);
        btnRegistoVeiculo.setPrefWidth(120);
        btnRegistoVeiculo.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 18px;-fx-border-radius: 20%; -fx-border-color: BLACK; -fx-background-color: transparent; -fx-text-fill: BLACK;");
        btnRegistoVeiculo.setText(registo);
        setAlignment(Pos.TOP_RIGHT);
        setMargin(btnRegistoVeiculo, new Insets(10, 10, 0, 0));
        setFocusTraversable(true);
        this.getChildren().addAll(btnRegistoVeiculo, btnEditar);
    }

    private void registerHandlers() {
        btnRegistoVeiculo.setOnAction(e -> {
            AppDMController.UIleitura.set(AppMain.appManager.selecionaRegistoVeiculo(btnRegistoVeiculo.getText()));
            listaBlocosInfoFlowPane.getChildren().clear();
            String[] lista = AppMain.appManager.getNomesBlocosInfo();
            if (lista.length == 0) {
                listaBlocosInfoFlowPane.getChildren().add(new Label("Sem blocos de informação registados"));
            } else {
                for (String registo : lista) {
                    listaBlocosInfoFlowPane.getChildren().add(new BlocoInfoButton(registo, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegisto, textoInfoBox, infoBox));
                }
            }
            blocosInfoPane.setVisible(true);
        });

        btnEditar.setOnAction(e -> {
            AppMain.appManager.selecionaRegistoVeiculo(btnRegistoVeiculo.getText());
            AppDMController.editarVeiculo.set(true);
        });
    }
    private void update() {
    }
}
