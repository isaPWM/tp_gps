package pt.isec.GPS.DigitalMechanics.UI.toolsAccess;

import java.io.File;
import java.util.List;

public class ExtensionImg {
    // Verifica se o arquivo possui uma extensão de imagem válida
    public boolean isExtensaoImagem(File arquivo, List<String> extensoesImagem) {
        String extensao = getFileExtension(arquivo.getName()).toLowerCase();
        return extensoesImagem.contains(extensao);
    }
    // Obtém a extensão do ficheiro
    private String getFileExtension(String nomeArquivo) {
        int posicaoPonto = nomeArquivo.lastIndexOf(".");
        if (posicaoPonto == -1) {
            return "";
        }
        return nomeArquivo.substring(posicaoPonto + 1);
    }
}
