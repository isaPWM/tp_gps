package pt.isec.GPS.DigitalMechanics.UI.toolsAccess;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ItemComboBox {
    private String text;
    private Image image;

    public ItemComboBox(String text, String imagePath) {
        this.text = text;
        this.image = new Image(imagePath);
    }

    public String getText() {
        return text;
    }

    public Image getImage() {
        return image;
    }

    public static class ListCell extends javafx.scene.control.ListCell<ItemComboBox> {
        private final ImageView imageView = new ImageView();

        public ListCell() {
            imageView.setFitWidth(30);
            imageView.setFitHeight(30);
        }

        @Override
        protected void updateItem(ItemComboBox item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
                setText(null);
                setGraphic(null);
            } else {
                setText(item.getText());
                imageView.setImage(item.getImage());
                setGraphic(imageView);
            }
        }
    }
}
