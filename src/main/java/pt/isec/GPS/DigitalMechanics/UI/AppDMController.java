package pt.isec.GPS.DigitalMechanics.UI;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.InfoRetorno;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.Lembrete;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Manutencao;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.MudancaOleo;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.RegistoGeral;
import pt.isec.GPS.DigitalMechanics.UI.Buttons.*;
import pt.isec.GPS.DigitalMechanics.UI.toolsAccess.ItemComboBox;

import java.awt.*;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class AppDMController {

    public static SimpleBooleanProperty editarRegisto = new SimpleBooleanProperty(false),
                                        editarVeiculo = new SimpleBooleanProperty(false),
                                        mostrarEtapaInfo = new SimpleBooleanProperty(false),
                                        UIleitura = new SimpleBooleanProperty(false);
    public static SimpleStringProperty trackingViagem = new SimpleStringProperty(null);

    public static SimpleBooleanProperty undoButton = new SimpleBooleanProperty(false);

    private final String STYLE_DEFAULT_TF = "-fx-background-color: white; -fx-border-color: #a8accc";
    @FXML
    public Button btnPassword;
    @FXML
    public BorderPane passWordForm;
    @FXML
    public TextField password;
    @FXML
    public Button btnUndo1;
    /////////////////////////// NODES QUE SÃO BUSCADOS AO FXML VIEW ///////////////////////////
    @FXML
    private Button btnImportaVeiculo;
    @FXML
    private Button eliLembretesVistos, eliTodosLembretes;
    @FXML
    public ImageView imageViewPeca, imageViewOleo, imageViewVeiculo;
    @FXML
    private ComboBox<ItemComboBox> comboBox;
    @FXML
    private HBox hBoxBtnsTrackingViagem, hBoxBtnsEliminaLembretes, hBoxTracking;
    @FXML
    private VBox infoText, erroBox, infoBox, novoBlocoNomeForm, lembreteInfo,lembreteBox, confirmarERV, eliTrackingSinal, vBoxNovoRegisto;
    @FXML
    private Button info, btnConfirmarPeca, btnConfirmarManut, btnMudanca, btnRegistoP, btnNovaEtapa,
            btnExportaVeiculo, btnNovoBloco, btnLeitura, btnNovoProprietario, novaPausa;
    @FXML
    private Label tipoRegistos, numeroRegistos, numeroImportacoes, numeroExportacoes,
                  textoInfoBox, registoPersonalizadoLabel, labelInfoLembrete,
                  dadosVeiculo, lembretewarning, trackingViagemAcedido, dadosEtapa;
    @FXML
    private FlowPane flowPaneRegistosVeiculo, listaBlocosInfoFlowPane, listaRegistosInfoScrollPane,
                     lembretesFlowPane, etapasTrackingViagemFlowPane, listaTrackingsVieagemFlowPane;
    @FXML
    private BorderPane homePane, blocosInfoPane, formRegistoPersonalizado, formCriacaoVeiculo,
            formPeca, formmudancaOleoNovo, formManutencao, lembretesBPane, formAbastecimento,
            novoLembrete, editarRegistoVeiculoPane,
            trackingViagemListaBPane, trackingViagemForm, etapasTrackingViagemBP, etapaForm, infoEtapa,
            pausaForm, exportarVeiculoPan;

    @FXML
    private Pane listaRegistosInfoPane;
    @FXML
    private TextField marcaV, modeloV, matriculaV, quilometrosV,
                      precoA, litrosA, quilometrosA,
                      nomePeca, marcaPeca, modeloPeca, precoPeca, lojaPeca,
                      marcaOleo, LitrosOleo,
                      tipoManutencao, precoManutencao,
                      nomeNovoBloco, nomeRegistoTF, precoPTF, descPTP, nomeLembrete,
                      nomeTrackingViagem, quilometrosTrackingViagem, destinoTrackingViagem,
                        localizacaoAtual, quilometrosInicioEtapa, horasInicioEtapa,
                        horasInicioPausa, horasFimPausa;

    @FXML
    private TextArea descricaoPeca, observacaoManutencao, documentosManutencao, observacoesTrackingViagem,
                    razaoPausa;
    @FXML
    private DatePicker dataPeca, dataMudancaDatePicker, dataAbastecimento, dataManutencao,
                       dataRP,lembreteDatePicker,lembreteManutencaoDatePicker,editarLembrete,
                       lim1Data, lim2Data,dataLembrete, inicioEtapa, inicioPausa, fimPausa;
    @FXML
    private Spinner<Integer> anoV, mesV;
    @FXML
    private ComboBox<String> combustivelV, estadoLembrete;
    @FXML
    private Label informacaoErroBoxLabel, notificacoes;
    @FXML
    private CheckBox lembreteCheckBox,lembreteManutencaoCheckBox, ultimaEtapa;
    private boolean selectedComboPeca = false;
    private static final String caminhoImg = String.valueOf(AppMain.class.getResource("imgIcons/"));
    private File ficheiro;

    /////////////////////////// A EXECUTAR NA INICIALIZAÇÃO ///////////////////////////
    @FXML
    protected void initialize() {
        this.combustivelV.getItems().setAll("Eletricidade", "Gasolina Comum", "Gasolina Aditiva", "Gasolina Premium",
                                          "Etanol", "Etanol Aditivo", "GNV", "GPL", "Diesel Comum",
                                          "Diesel Aditivo", "Diesel Premium");
        this.combustivelV.setValue(" ");
        this.flowPaneRegistosVeiculo.setPadding(new Insets(10));
        this.flowPaneRegistosVeiculo.setHgap(10);
        this.flowPaneRegistosVeiculo.setVgap(10);
        this.btnUndo1.setDisable(!AppMain.appManager.getUndo());
        atualizaFlowPaneRegistosVeiculos();
        ///////////////////// COMBOBOX LEMBRETE /////////////////////////////////////

        estadoLembrete.getItems().addAll("Todos", "Aberto", "Fechado");
        estadoLembrete.setValue("Todos");
        lim1Data.setValue(LocalDate.now());
        lim2Data.setValue(lim1Data.getValue().plusYears(2));

        /////////////////////// COMBOBOX PECA ///////////////////////////////////////

        comboBox.getItems().addAll(
                new ItemComboBox("Turbo",caminhoImg+"turbo.png"),
                new ItemComboBox("Pneu",caminhoImg+"pneu.png"),
                new ItemComboBox("Vela",caminhoImg+"vela.png"),
                new ItemComboBox("Bateria",caminhoImg+"bateria.png"),
                new ItemComboBox("Amortecedor",caminhoImg+"suspensao.png"),
                new ItemComboBox("Lampada",caminhoImg+"luz_para_farois.png"),
                new ItemComboBox("Relé",caminhoImg+"rele.png"),
                new ItemComboBox("Pastilha",caminhoImg+"pinca_travoes.png"),
                new ItemComboBox("Radiador",caminhoImg+"radiador.png"),
                new ItemComboBox("Para-Brisas",caminhoImg+"para_brisas.png"),
                new ItemComboBox("Faróis",caminhoImg+"farois.png"),
                new ItemComboBox("Fusível",caminhoImg+"fusivel.png"),
                new ItemComboBox("Travões",caminhoImg+"travoes.png"),
                new ItemComboBox("Embraiagem",caminhoImg+"disco_embreagem.png"),
                new ItemComboBox("Correia de distribuição",caminhoImg+"correia_distribuicao.png"),
                new ItemComboBox("Correia de alternador",caminhoImg+"correia_alternador.png"),
                new ItemComboBox("Filtro do ar",caminhoImg+"filtro_ar_comum.png"),
                new ItemComboBox("Filtro do óleo",caminhoImg+"filtro_oil.png"),
                new ItemComboBox("Filtro do combustível",caminhoImg+"filtro_gas.png"),
                new ItemComboBox("Outro",caminhoImg+"outra_peca.png")
        );
        comboBox.setCellFactory(param -> new ItemComboBox.ListCell());
        comboBox.setButtonCell(new ItemComboBox.ListCell());
        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                selectedComboPeca = !newValue.getImage().getUrl().contains(caminhoImg + "outra_peca.png");
        });

        editarRegisto.addListener(e-> edicaoDeFormsDeRegisto());
        editarVeiculo.addListener(e -> preencherDadosVeiculo());
        mostrarEtapaInfo.addListener(e -> informacaoEtapa());
        AppMain.appManager.addListaLembretesChangeListener("lista", e -> {
            atualizaFlowPaneLembretes();
        });

        UIleitura.addListener(e -> {
            hBoxBtnsEliminaLembretes.setVisible(!UIleitura.getValue());
            vBoxNovoRegisto.setVisible(!UIleitura.getValue());
            hBoxBtnsTrackingViagem.setVisible(!UIleitura.getValue());
            btnNovaEtapa.setVisible(!UIleitura.getValue());
            btnNovoBloco.setVisible(!UIleitura.getValue());
            btnExportaVeiculo.setVisible(!UIleitura.getValue());
            novaPausa.setVisible(!UIleitura.getValue());
            hBoxTracking.setVisible(!UIleitura.getValue());
        });

        trackingViagem.addListener(e -> visibilidadeEtapasTrackingViagemBP());

        AppMain.appManager.addNotificacoesLembretesChangeListener("verInfo", e -> {
            String lembreteI = AppMain.appManager.getLembreSelecionado();
            if(lembreteI != null) {
                labelInfoLembrete.setText(lembreteI);
            }
            visibilidadeLembreteInfo();
        });
        btnImportaVeiculo.setOnAction(e -> {
            ficheiro = getFicheiro();
            if (ficheiro != null)
                visibilidadePassword();
        });
        btnPassword.setOnAction(e -> {
            if (password.getText().isEmpty() || password.getText().isBlank())
                msgBox("Insira uma password");
            else {
                AppMain.appManager.setSenhaDada(password.getText());
                importaVeiculo(ficheiro);
                visibilidadePassword();
            }
        });
        btnLeitura.setOnAction(e -> {
            exportarVeiculoLeitura();
            visibilidadeExport();
        });
        btnNovoProprietario.setOnAction(e -> {
            exportarVeiculoEdicao();
            visibilidadeExport();
        });
        mostrarlembretes();
        undoButton.addListener(e -> btnUndo1.setDisable(!undoButton.getValue()));
    }
    private void preencherDadosVeiculo() {
        if(editarVeiculo.getValue()){
            dadosVeiculo.setText(AppMain.appManager.getDadosVeiculo());
            editarRegistoVeiculoPane.setVisible(true);
        }
        else
            editarRegistoVeiculoPane.setVisible(false);
    }

    private boolean isFinalizada(){
        Boolean [] etapasFinal = AppMain.appManager.getIsEtapaFinal();
        return etapasFinal.length > 0 && etapasFinal[etapasFinal.length - 1];
    }
    private void informacaoEtapa() {
        novaPausa.setDisable(!AppMain.appManager.getIsEtapaAlteravel() || isFinalizada());
        if(mostrarEtapaInfo.getValue()){
            dadosEtapa.setText(AppMain.appManager.getDadosEtapa());
            infoEtapa.setVisible(true);
        }
        else
            infoEtapa.setVisible(false);
    }
    private void atualizaFlowPaneLembretes(){
        lembretesFlowPane.getChildren().clear();
        Lembrete[] lista = AppMain.appManager.getListaLembretes();
        if(lista.length == 0) {
            lembretesFlowPane.getChildren().add(new Label("Sem Lembretes"));
        }
        else {
            for (Lembrete lembrete : lista) {
                lembretesFlowPane.getChildren().add(new ButtonLembrete(lembrete));
            }
        }
        eliLembretesVistos.setDisable(AppMain.appManager.getListaLembretesSize() - AppMain.appManager.getNumNotificacoes() == 0);
        eliTodosLembretes.setDisable(AppMain.appManager.getListaLembretesSize() == 0);

    }
/////////////////////////// INFO ADICIONAL, ERRO BOX, VOLTAR ///////////////////////////
    @FXML
    public void info() {
        infoText.setVisible(!infoText.isVisible());
        numeroExportacoes.setText("" + AppMain.appManager.getNumExportacoes());
        numeroImportacoes.setText("" + AppMain.appManager.getNumImportacoes());
        numeroRegistos.setText("" + AppMain.appManager.getNumVeiculos());
    }
    @FXML
    public void fecharErroBox() {
        erroBox.setVisible(false);
    }

    @FXML
    public void fecharInfoBox() {
        infoBox.setVisible(false);
    }
    @FXML
    public void voltar() {
        blocosInfoPane.setVisible(false);
        AppMain.appManager.saveListaLembretes();
    }

    @FXML
    public void voltarEditPane() {
        editarVeiculo.set(false);
    }

    @FXML
    public void sairEtapaInfo() {
        mostrarEtapaInfo.set(false);
    }

    public void voltarRegistPane() {
        if (listaRegistosInfoPane.isVisible()) {
            listaRegistosInfoPane.setVisible(false);
        }
    }

/////////////////////////// REGISTO VEÍCULO ////////////////////////////////////////////////////

    /////////////////////////// FUNÇÃO PARA PÔR O FORM DE CRIA REGISTO VISÍVEL ////////////////
    @FXML
    protected void visibilidadeRegistoVeiculoForm() {
        formCriacaoVeiculo.setVisible(!formCriacaoVeiculo.isVisible());
        homePane.setDisable(!homePane.isDisabled());
        imageViewVeiculo.setImage(null);
    }

    /////////////////////////// FUNÇÕES REGISTO DE VEÍCULO ///////////////////////////
    @FXML
    protected void adicionaRegistoVeiculo() {
        String auxImage = "";
        if(imageViewVeiculo.getImage() != null) {
            auxImage = imageViewVeiculo.getImage().getUrl().substring(6);
            //imageViewVeiculo.fitHeightProperty().setValue(500);
            //imageViewVeiculo.fitWidthProperty().setValue(500);
        }

        InfoRetorno res = AppMain.appManager.adicionaRegistoVeiculo(marcaV.getText(), modeloV.getText(),
                matriculaV.getText(), combustivelV.getValue(),
                anoV.getValue(), mesV.getValue(), quilometrosV.getText(), auxImage);
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else
        {
            if(res.validadeParametros() == null)
                msgBox(res.mensagem());
            else {
                if (res.resultado()) {
                    visibilidadeRegistoVeiculoForm();
                    atualizaFlowPaneRegistosVeiculos();
                    marcaV.clear(); modeloV.clear(); matriculaV.clear(); quilometrosV.clear();
                } else {
                    msgBox(res.mensagem());
                }
                if(res.validadeParametros() != null) {
                    marcaV.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    modeloV.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    matriculaV.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    combustivelV.setStyle(res.validadeParametros()[3] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    quilometrosV.setStyle(res.validadeParametros()[4] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    mesV.setStyle(res.validadeParametros()[5] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                    anoV.setStyle(res.validadeParametros()[6] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
                }
            }
        }
    }
    private void atualizaFlowPaneRegistosVeiculos(){
        flowPaneRegistosVeiculo.getChildren().clear();
        String[] lista = AppMain.appManager.getNomesRegistosVeiculos();
        String[] fotos = AppMain.appManager.getFotoVeiculo();
        if(lista.length == 0)
            flowPaneRegistosVeiculo.getChildren().add(new Label("Sem Registos de Veículos"));
        else {
            for (int i = 0; i < lista.length; i++) {
                flowPaneRegistosVeiculo.getChildren().add(new StackPaneRegistoVeiculo(lista[i], fotos[i], blocosInfoPane, listaBlocosInfoFlowPane, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegistos, textoInfoBox, infoBox, editarRegistoVeiculoPane));
            }
        }
    }


/////////////////////////// LEMBRETES ////////////////////////////////////////////////////
private void mostrarlembretes(){
    int yaaa=0;
    Lembrete[] lista = AppMain.appManager.getListaLembretes();
    Lembrete help = null;
    int tempo;

    for (Lembrete lembrete : lista) {
        if(lembrete.getNomeLembrete().contains("Mudança de óleo")) {
            tempo=AppMain.appManager.getValidadeLembrete(lembrete.getData(), lembrete.getNomeLembrete());
            if(tempo>0){yaaa++;help = lembrete;}
        }else if(lembrete.getNomeLembrete().contains("Manutenção")){
            tempo=AppMain.appManager.getValidadeLembrete(lembrete.getData(), lembrete.getNomeLembrete());
            System.out.println("AQUI -> "+ tempo);
            if(tempo>0){yaaa++;help = lembrete;}
        }
        else if (lembrete.getData() != null && lembrete.getData().isEqual(LocalDate.now())) {
            yaaa++;
            help = lembrete;
        }
    }
    if (yaaa == 1) {
        lembretewarning.setText("Tem um lembrete: " + help.getNomeLembrete() + " para hoje! \n No veiculo:" + AppMain.appManager.getDadosVeiculo());
        lembreteBox.setVisible(true);
    } else if (yaaa > 1) {
        lembretewarning.setText("Possui varios lembretes para hoje");
        lembreteBox.setVisible(true);
    }
}

    @FXML
    public void visibilidadeLembretes() {
        lembretesBPane.setVisible(!lembretesBPane.isVisible());
    }
    @FXML
    public void visibilidadeLembreteInfo(){
        lembreteInfo.setVisible(!lembreteInfo.isVisible());
        lembretesBPane.setDisable(lembreteInfo.isVisible());
    }
    @FXML
    public void visibilidadeFormLembrete() {
        novoLembrete.setVisible(true);

    }
    public void cancelarLembrete() {
        dataLembrete.setValue(null);
        nomeLembrete.clear();
        novoLembrete.setVisible(false);
    }

    public void criarLembrete() {
        AppMain.appManager.adicionaLembreteNaApp(nomeLembrete.getText(), dataLembrete.getValue(), -1);
        novoLembrete.setVisible(false);
        atualizaFlowPaneLembretes();

    }
    @FXML
    public void eliLembretesVistos() {
        AppMain.appManager.eliminaLembretesVistos();
    }
    @FXML
    public void eliTodosLembretes() {
        AppMain.appManager.eliTodosLembretes();
    }
    public void filtraListaLembretes() {
        for(Node l : lembretesFlowPane.getChildren()) {
            ButtonLembrete btnLembrete = (ButtonLembrete) l;
            if(btnLembrete.getData().isAfter(lim1Data.getValue()) && btnLembrete.getData().isBefore(lim2Data.getValue())
                    && ((estadoLembrete.getValue().equals("Todos") ||
                    (estadoLembrete.getValue().equals("Aberto") && btnLembrete.isEstadoAberto()) ||
                    (estadoLembrete.getValue().equals("Fechado") && !btnLembrete.isEstadoAberto())))){
                btnLembrete.setVisible(true);
                btnLembrete.setManaged(true);
            }
            else {
                btnLembrete.setVisible(false);
                btnLembrete.setManaged(false);
            }
        }
    }

/////////////////////////// BLOCOS INFORMAÇÃO ////////////////////////////////////////////////////
    @FXML
    public void cancelarvisibilidadeDeFormsDeRegisto() {
        if(editarRegisto.getValue())
            editarRegisto.setValue(false);
        visibilidadeDeFormsDeRegisto();
    }
    @FXML
    public void visibilidadeDeFormsDeRegisto() {
        switch (AppMain.appManager.getTipoBlocoInfo()) {
            case PECAS -> {
                btnConfirmarPeca.setText(editarRegisto.getValue() ? "Editar" : "Criar");
                nomePeca.setStyle(STYLE_DEFAULT_TF);
                marcaPeca.setStyle(STYLE_DEFAULT_TF);
                modeloPeca.setStyle(STYLE_DEFAULT_TF);
                precoPeca.setStyle(STYLE_DEFAULT_TF);
                formPeca.setVisible(!formPeca.isVisible());
            }
            case ABASTECIMENTOS -> {
                precoA.setStyle(STYLE_DEFAULT_TF);
                litrosA.setStyle(STYLE_DEFAULT_TF);
                quilometrosA.setStyle(STYLE_DEFAULT_TF);
                dataAbastecimento.setStyle(STYLE_DEFAULT_TF);
                formAbastecimento.setVisible(!formAbastecimento.isVisible());
            }
            case MUDANCAOLEO -> {
                btnMudanca.setText(editarRegisto.getValue() ? "Editar" : "Criar");
                dataMudancaDatePicker.setStyle(STYLE_DEFAULT_TF);
                marcaOleo.setStyle(STYLE_DEFAULT_TF);
                formmudancaOleoNovo.setVisible(!formmudancaOleoNovo.isVisible());
            }
            case MANUTENCOES -> {
                btnConfirmarManut.setText(editarRegisto.getValue() ? "Editar" : "Criar");
                tipoManutencao.setStyle(STYLE_DEFAULT_TF);
                dataManutencao.setStyle(STYLE_DEFAULT_TF);
                precoManutencao.setStyle(STYLE_DEFAULT_TF);
                formManutencao.setVisible(!formManutencao.isVisible());
            }
            case OUTRO -> {
                btnRegistoP.setText(editarRegisto.getValue() ? "Editar" : "Criar");
                nomeRegistoTF.setStyle(STYLE_DEFAULT_TF);
                dataRP.setStyle(STYLE_DEFAULT_TF);
                precoPTF.setStyle(STYLE_DEFAULT_TF);
                registoPersonalizadoLabel.setText("Registos de " + AppMain.appManager.getNomeBlocoInfoAtual());
                formRegistoPersonalizado.setVisible(!formRegistoPersonalizado.isVisible());
            }
        }
        listaRegistosInfoPane.setDisable(!listaRegistosInfoPane.isDisabled());
    }

    @FXML
    public void edicaoDeFormsDeRegisto() {
        if(editarRegisto.getValue()) {
            visibilidadeDeFormsDeRegisto();
            switch (AppMain.appManager.getTipoBlocoInfo()) {
                case PECAS -> {
                    Peca registo = (Peca) AppMain.appManager.getRegistoAcedido();
                    nomePeca.setText(registo.nome());
                    marcaPeca.setText(registo.marca());
                    modeloPeca.setText(registo.modelo());
                    precoPeca.setText(registo.preco() + "");
                    lojaPeca.setText(registo.loja());
                    LocalDate date = LocalDate.parse(registo.data());
                    dataPeca.setValue(date);
                    descricaoPeca.setText(registo.descricao());
                }
                case MUDANCAOLEO -> {
                    MudancaOleo registo = (MudancaOleo) AppMain.appManager.getRegistoAcedido();
                    LocalDate date = LocalDate.parse(registo.data());
                    dataMudancaDatePicker.setValue(date);
                    marcaOleo.setText(registo.marca());
                    litrosA.setText(registo.oleo().toString());
                    lembreteCheckBox.setDisable(true);
                    dataMudancaDatePicker.setVisible(false);
                }
                case MANUTENCOES -> {
                    Manutencao registo = (Manutencao) AppMain.appManager.getRegistoAcedido();
                    tipoManutencao.setText(registo.tipo());
                    observacaoManutencao.setText(registo.observacao());
                    precoManutencao.setText(String.valueOf(registo.preco()));
                    LocalDate date = LocalDate.parse(registo.data());
                    dataManutencao.setValue(date);
                    lembreteManutencaoCheckBox.setDisable(true);
                    dataMudancaDatePicker.setVisible(false);
                }
                case OUTRO -> {
                    RegistoGeral registo = (RegistoGeral) AppMain.appManager.getRegistoAcedido();
                    nomeRegistoTF.setText(registo.nomeRegisto());
                    precoPTF.setText(String.valueOf(registo.preco()));
                    descPTP.setText(registo.descricao());
                    LocalDate date = LocalDate.parse(registo.data());
                    dataRP.setValue(date);
                }
            }
        }
        else {
            lembreteManutencaoCheckBox.setDisable(false);
            lembreteCheckBox.setDisable(false);
        }
    }
    /////////////////////////// FUNÇÃO ADICIONAR BLOCO INFO PERSONALIZADO ////////////////////////

    @FXML
    public void visibilidadeNomeDeNovoBlocoRP() {
        novoBlocoNomeForm.setVisible(!novoBlocoNomeForm.isVisible());
        blocosInfoPane.setDisable(!blocosInfoPane.isDisabled());
    }
    private void atualizaFlowPaneBlocosInfo(String nomeBlocoInfoNovo){
        listaBlocosInfoFlowPane.getChildren().add(new BlocoInfoButton(nomeBlocoInfoNovo, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegistos, textoInfoBox, infoBox));
        blocosInfoPane.setVisible(true);
    }
    @FXML
    public void adicionaBlocoInformacao() {
        if(AppMain.appManager.adicionaBlocoInformacao(nomeNovoBloco.getText())) {
            visibilidadeNomeDeNovoBlocoRP();
            atualizaFlowPaneBlocosInfo(nomeNovoBloco.getText());
        }
        else {
            erroBox.setVisible(true);
        }
    }

/////////////////////////// REGISTOS DE INFORMAÇÃO ////////////////////////////////////////////////////
    private void msgBox(String msg){
        erroBox.setVisible(true);
        informacaoErroBoxLabel.setText(msg);
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE ABASTECIMENTO ////////////////////////
    @FXML
    public void adicionaAbastecimento() {
        InfoRetorno res = AppMain.appManager.adicionaAbastecimento(precoA.getText(), litrosA.getText(), quilometrosA.getText(), dataAbastecimento.getValue());
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                cancelarvisibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                precoA.clear(); litrosA.clear(); quilometrosA.clear(); dataAbastecimento.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            precoA.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            litrosA.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            quilometrosA.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataAbastecimento.setStyle(res.validadeParametros()[3] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE PECA ////////////////////////
    @FXML
    public void adicionaPeca() {
        String auxImage; InfoRetorno res;
        if (imageViewPeca.getImage() != null)
            auxImage = imageViewPeca.getImage().getUrl();
        else if (selectedComboPeca)
            auxImage = comboBox.getValue().getImage().getUrl();
        else
            auxImage = caminhoImg + "outra_peca.png";

        res = AppMain.appManager.adiciona_edita_Peca(nomePeca.getText(), marcaPeca.getText(), modeloPeca.getText(), precoPeca.getText(),
                    descricaoPeca.getText(), lojaPeca.getText(), dataPeca.getValue(), auxImage, editarRegisto.getValue());

        if (res == null) {
            msgBox("Tente novamente...");
        } else {
            if (res.resultado()) {
                cancelarvisibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                nomePeca.clear(); marcaPeca.clear(); modeloPeca.clear(); precoPeca.clear();
                descricaoPeca.clear(); lojaPeca.clear(); dataPeca.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            nomePeca.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            marcaPeca.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            modeloPeca.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoPeca.setStyle(res.validadeParametros()[3] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    ///////////////////////// FUNÇÃO MANUT ///////////////////////////////////
    @FXML
    public void adicionaManutencao(){
        InfoRetorno res;

        res = AppMain.appManager.adiciona_edita_Manutencao(tipoManutencao.getText(), dataManutencao.getValue(),
                    precoManutencao.getText(), observacaoManutencao.getText(), lembreteManutencaoCheckBox.isSelected() ? lembreteManutencaoDatePicker.getValue() : null, editarRegisto.getValue());

        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                cancelarvisibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                tipoManutencao.clear(); dataManutencao.setValue(null); precoManutencao.clear(); observacaoManutencao.clear();
            } else {
                msgBox(res.mensagem());
            }
            tipoManutencao.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataManutencao.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoManutencao.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
        atualizaFlowPaneLembretes();
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE MUDANCA DE OLEO ////////////////////////
    @FXML
    public void mudancaOleoRegisto() {
        String auxImage; InfoRetorno res;
        if(imageViewOleo.getImage() != null)
            auxImage = imageViewOleo.getImage().getUrl();
        else
            auxImage = caminhoImg + "outra_peca.png";

        res = AppMain.appManager.adiciona_edita_MudancaOleo(dataMudancaDatePicker.getValue(),marcaOleo.getText(),auxImage,LitrosOleo.getText(), lembreteCheckBox.isSelected() ? lembreteDatePicker.getValue() : null, editarRegisto.getValue());


        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                cancelarvisibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                marcaOleo.clear(); dataMudancaDatePicker.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            dataMudancaDatePicker.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            marcaOleo.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
        atualizaFlowPaneLembretes();
    }
    public void handleLembreteCheckBoxAction(ActionEvent actionEvent) {
        if (lembreteCheckBox.isSelected()) {
            lembreteDatePicker.setVisible(true);
        }else lembreteDatePicker.setVisible(false);
    }
    public void handleLembreteManutencaoCheckBoxAction(ActionEvent actionEvent) {
        if(lembreteManutencaoCheckBox.isSelected()){
            lembreteManutencaoDatePicker.setVisible(true);
        }else lembreteManutencaoDatePicker.setVisible(false);
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO P ////////////////////////
    @FXML
    public void adicionaRegistoPersonalizado(){
        InfoRetorno res;

        res = AppMain.appManager.adiciona_edita_RegistoPersonalizado(nomeRegistoTF.getText(), dataRP.getValue(), precoPTF.getText(), descPTP.getText(), editarRegisto.getValue());

        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                cancelarvisibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                nomeRegistoTF.clear(); dataRP.setValue(null); precoPTF.clear(); descPTP.clear();
            } else {
                msgBox(res.mensagem());
            }
            nomeRegistoTF.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataRP.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoPTF.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ATUALIZAR LISTA DE REGISTOS DE INFORMAÇÃO //////////////
    @FXML
    private void atualizaListaRegistosInfoScrollPane() {
        listaRegistosInfoScrollPane.getChildren().clear();
        ArrayList<Record> lista = AppMain.appManager.getListaRegistosDeBlocoAtual();
        if (!lista.isEmpty()) {
            Collections.sort(lista, new Comparator<Record>() {
                @Override
                public int compare(Record registo1, Record registo2) {
                    return registo2.toString().compareTo(registo1.toString());
                }
            });
            for (Record registo : lista) {
                if (registo instanceof Peca) {
                    listaRegistosInfoScrollPane.getChildren().add(new HBoxRegistoPeca(registo, textoInfoBox, infoBox, listaRegistosInfoScrollPane));
                }
                else  {
                    listaRegistosInfoScrollPane.getChildren().add(new HboxRegisto(registo, textoInfoBox, infoBox, listaRegistosInfoScrollPane));
                }
            }
        } else
            listaRegistosInfoScrollPane.getChildren().add(new Label("Sem Registos Adicionados"));
        listaRegistosInfoPane.setVisible(true);
    }


    //////////////////////////// FUNÇÃO ABRIR FILE CHOOSER /////////////////////////////////////////
    @FXML
    public void abrirFileChooser() {
        FileChooser fileChooser = new FileChooser();

        // Configuração para exibir apenas arquivos de imagem
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Imagens", "*.png", "*.jpg", "*.jpeg", "*.gif");
        fileChooser.getExtensionFilters().add(extFilter);

        // Mostra a janela de seleção do arquivo e obtém o arquivo selecionado
        Stage stage = (Stage) (blocosInfoPane).getScene().getWindow();
        File arquivoSelecionado = fileChooser.showOpenDialog(stage);

        if (arquivoSelecionado != null) {
            // Carrega a imagem selecionada e exibe na ImageView
            Image imagem = new Image(arquivoSelecionado.toURI().toString());
            imageViewPeca.setImage(imagem);
            imageViewOleo.setImage(imagem);
            imageViewVeiculo.setImage(imagem);
        }
    }

    ////////////////////////////////// FUNÇÃO ELIMINA REGISTO VEICULO //////////////////////
    @FXML
    public void eliminaRegistoVeiculo(){
        AppMain.appManager.eliminaRegistoVeiculo(AppMain.appManager.getUltimoRegistoVeiculoAcedido());
        atualizaFlowPaneRegistosVeiculos();
        visibilidadeConfirmaERV();
        editarVeiculo.set(false);
    }

    @FXML
    public void visibilidadeConfirmaERV() {
        confirmarERV.setVisible(!confirmarERV.isVisible());
    }

    ///////////////////////////////// IMPORTAR VEICULO ///////////////////////////////////////
    public File getFicheiro() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Ficheiros", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        Stage stage = (Stage) (blocosInfoPane).getScene().getWindow();
        return fileChooser.showOpenDialog(stage);
    }

    public boolean importaVeiculo(File arquivoSelecionado) {
        if (arquivoSelecionado != null) {
            if (AppMain.appManager.importaVeiculo(arquivoSelecionado)) {
                atualizaFlowPaneRegistosVeiculos();
                msgBox("Veículo importado com sucesso");
                return true;
            } else {
                msgBox("Erro ao importar veículo");
                return false;
            }
        } else
            msgBox("Erro ao importar veículo, o ficheiro é inválido");
        return false;
    }

    public void visibilidadeExport(){
        exportarVeiculoPan.setVisible(!exportarVeiculoPan.isVisible());
        homePane.setDisable(!homePane.isDisabled());
        imageViewVeiculo.setImage(null);
    }

    public boolean exportarVeiculoLeitura(){
        return exportarVeiculo(true);
    }

    public boolean exportarVeiculoEdicao(){
        return exportarVeiculo(false);
    }

    public boolean exportarVeiculo(boolean leitura) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = (Stage) (blocosInfoPane).getScene().getWindow();
        File pastaSelecionada = directoryChooser.showDialog(stage);
        if (pastaSelecionada != null) {

            if (!AppMain.appManager.exportarVeiculo(pastaSelecionada, leitura)) {
                msgBox("Erro ao exportar veículos");
            } else {
                msgBox("Veículos exportados com sucesso na pasta 12-12-RD");
            }
        } else {
            msgBox("Erro ao exportar veículos, pasta inválida");
        }

        return false;
    }

    public void fecharLembreteBox(MouseEvent mouseEvent) {
        lembreteBox.setVisible(false);
    }
/*
    /// AppDMController
    public confirmaCriacaoLembrete() {
        AppMain.appManager.criaLembrete(labelMiniform.getText(), dataMiniform.getValue());
    }
*/
    ///////////////////////////////// TRACKING VIAGENS ///////////////////////////////////////
    @FXML
    public void visibilidadeTrackingViagensBPane(){
        trackingViagemListaBPane.setVisible(!trackingViagemListaBPane.isVisible());
        atualizaTrackingViagemListaBPane();
    }
    @FXML
    public void visibilidadeTrackingViagemForm(){
        trackingViagemForm.setVisible(!trackingViagemForm.isVisible());
    }
    @FXML
    public void visibilidadeEtapaForm(){ etapaForm.setVisible(!etapaForm.isVisible()); clearEtapaForm(); }
    public void clearEtapaForm() { localizacaoAtual.clear(); quilometrosInicioEtapa.clear();}
    @FXML
    public void sairEtapasTrackingViagemBP(){
        trackingViagem.setValue(null);
    }
    @FXML
    public void visibilidadeEtapasTrackingViagemBP() {
        trackingViagemAcedido.setText(trackingViagem.getValue());
        etapasTrackingViagemBP.setVisible(trackingViagem.getValue() != null);
        verificaEtapaFinal();
    }

    private void verificaEtapaFinal(){
        boolean aux = false;
        Boolean [] etapasFinal = AppMain.appManager.getIsEtapaFinal();
        if(etapasFinal.length > 0)
            aux = etapasFinal[etapasFinal.length - 1];

        btnNovaEtapa.setDisable(aux);
    }
    @FXML
    public void adicionarTrackingViagem() {
        InfoRetorno res;
        res = AppMain.appManager.novoTrackingViagem(nomeTrackingViagem.getText(), observacoesTrackingViagem.getText(),
                                                    quilometrosTrackingViagem.getText(), destinoTrackingViagem.getText());

        if (res.resultado()) {
            visibilidadeTrackingViagemForm();
            atualizaTrackingViagemListaBPane();
            nomeTrackingViagem.clear(); observacoesTrackingViagem.clear(); quilometrosTrackingViagem.clear(); destinoTrackingViagem.clear();
        } else {
            msgBox(res.mensagem());
        }
        nomeTrackingViagem.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        destinoTrackingViagem.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        quilometrosTrackingViagem.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
    }

    private void atualizaTrackingViagemListaBPane() {
        listaTrackingsVieagemFlowPane.getChildren().clear();
        String [] nomesTV = AppMain.appManager.getNomesTrackingViagens();
        if (nomesTV.length > 0) {
            for (String nomeTV : nomesTV) {
                listaTrackingsVieagemFlowPane.getChildren().add(new TrackingViagemButton(nomeTV, etapasTrackingViagemFlowPane));
            }
        } else
            listaTrackingsVieagemFlowPane.getChildren().add(new Label("Sem Trackings de Viagem Adicionados"));
    }

    @FXML
    public void acrescentarEtapa() {
        InfoRetorno res;

            res = AppMain.appManager.acrescentarEtapa(inicioEtapa.getValue(), horasInicioEtapa.getText(), localizacaoAtual.getText(), quilometrosInicioEtapa.getText(), ultimaEtapa.isSelected() ? true : false);

            if (res.resultado()) {
                visibilidadeEtapaForm();
                atualizaTrackingViagemEtapasFlowPane();
                verificaEtapaFinal();
                horasInicioEtapa.clear();
                localizacaoAtual.clear();
                quilometrosInicioEtapa.clear();
                ultimaEtapa.setSelected(false);
            } else {
                msgBox(res.mensagem());
            }

            inicioEtapa.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            localizacaoAtual.setStyle(res.validadeParametros()[3] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            quilometrosInicioEtapa.setStyle(res.validadeParametros()[4] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            horasInicioEtapa.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
    }

    @FXML
    public void adicionaPausa(){
        InfoRetorno res;

            res = AppMain.appManager.adicionaPausa(inicioPausa.getValue(), fimPausa.getValue(), horasInicioPausa.getText(), horasFimPausa.getText(), razaoPausa.getText());

            if (res.resultado()) {
                visibilidadePausaForm();
                informacaoEtapa();
                razaoPausa.clear();
                horasFimPausa.clear();
                horasInicioPausa.clear();
            } else {
                msgBox(res.mensagem());
            }

            inicioPausa.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            fimPausa.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            horasInicioPausa.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            horasFimPausa.setStyle(res.validadeParametros()[4] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");


    }

    @FXML
    public void visibilidadePausaForm() {
        pausaForm.setVisible(!pausaForm.isVisible());
        razaoPausa.clear();
    }

    private void atualizaTrackingViagemEtapasFlowPane(){
        etapasTrackingViagemFlowPane.getChildren().clear();
        String [] etapas = AppMain.appManager.getEtapasTrackingViagens();
        if (etapas.length > 0) {
            for (String etapa : etapas){
                etapasTrackingViagemFlowPane.getChildren().add(new EtapaButton(etapa));
            }
        } else
            etapasTrackingViagemFlowPane.getChildren().add(new Label("Sem Etapas Adicionadas"));
    }

    public void visibilidadePassword() {
        passWordForm.setVisible(!passWordForm.isVisible());
    }

    public void adicionaPassword() {
        visibilidadePassword();
    }

    public void undo() {
        if(!AppMain.appManager.undo())
            msgBox("Não é possível fazer undo, não existem mais alterações para desfazer");
        else
            voltarRegistPane();
        // para atualizar a lista de registos, pois o objeto appDigital em JAVA ainda nao foi atualizado
        //faz com que o utilizador tbm nao faça muitos UNDOs seguidos :)
    }

    public void visibilidadeConfirmaEliTracking() {
        eliTrackingSinal.setVisible(!eliTrackingSinal.isVisible());
    }

    public void eliminarTraking() {
        AppMain.appManager.eliminaTrackingViagem(trackingViagemAcedido.getText());
        etapasTrackingViagemBP.setVisible(false);
        atualizaTrackingViagemListaBPane();
        visibilidadeConfirmaEliTracking();
    }
}