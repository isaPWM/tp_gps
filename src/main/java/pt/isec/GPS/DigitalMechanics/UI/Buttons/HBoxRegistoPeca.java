package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

import java.io.File;

public class HBoxRegistoPeca extends HBox {
    private Peca registo;
    private Label textoInfoBox;
    private VBox infoBox;
    private Button botaoMaininfo, botaoEliminar, botaoEditar;
    private FlowPane listaRegistosInfoScrollPane;
    public HBoxRegistoPeca(Record registo, Label textoInfoBox, VBox infoBox, FlowPane listaRegistosInfoScrollPane){
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.registo = (Peca)registo;
        createViews();
        registerHandlers();
        update();
    }

    private void createViews() {
        botaoMaininfo = new Button(registo.nome() + " em " + registo.data());

        botaoEliminar = new Button();
        botaoEliminar.setPrefSize(30, 30);
        botaoEliminar.setStyle("-fx-background-color: transparent");
        ImageView iv = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/delete.png"))));
        iv.setFitHeight(30);
        iv.setFitWidth(30);
        botaoEliminar.setGraphic(iv);

        botaoEditar = new Button();
        botaoEditar.setPrefSize(30, 30);
        botaoEditar.setStyle("-fx-background-color: transparent");
        ImageView ivEdi = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/pen.png"))));
        ivEdi.setFitHeight(30);
        ivEdi.setFitWidth(30);
        botaoEditar.setGraphic(ivEdi);

        botaoMaininfo.setPrefSize(Region.USE_COMPUTED_SIZE, 30);
        botaoMaininfo.setMinWidth(300);
        botaoMaininfo.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 18px; -fx-background-radius: 25px; -fx-background-color: #E8EAF6; -fx-text-fill: #201033;");
        HBox.setHgrow(botaoMaininfo, javafx.scene.layout.Priority.ALWAYS);

        this.setStyle("-fx-border-radius: 25px;-fx-background-radius: 25px; -fx-background-color: #E8EAF6; -fx-border-color: #6e76aa; -fx-padding: 5px;");

        File image = new File(registo.foto());
        ImageView imageView;
        if (image.exists())
            imageView = new ImageView(new Image(image.toURI().toString()));
        else
            imageView = new ImageView(String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")));
        imageView.setFitHeight(35);
        imageView.setFitWidth(35);

        this.setMinWidth(300);
        setSpacing(10);
        this.getChildren().addAll(botaoMaininfo, botaoEliminar, botaoEditar, imageView);

        botaoEliminar.setVisible(!AppDMController.UIleitura.getValue());
        botaoEditar.setVisible(!AppDMController.UIleitura.getValue());
    }

    private void registerHandlers() {
        botaoMaininfo.setOnAction(e -> {
            textoInfoBox.setText(registo.toString());
            infoBox.setVisible(true);
        });
        botaoEliminar.setOnAction(e -> {
            if(!AppMain.appManager.eliminaInfo(registo)) {
                textoInfoBox.setText("Não foi possível eliminar o registo!");
                infoBox.setVisible(true);
            } else {
                listaRegistosInfoScrollPane.getChildren().remove(this);
            }
        });
        botaoEditar.setOnAction(e -> {
            AppMain.appManager.setRegistoAcedido(registo);
            AppDMController.editarRegisto.setValue(true);
        });
    }

    private void update() {
    }
}
