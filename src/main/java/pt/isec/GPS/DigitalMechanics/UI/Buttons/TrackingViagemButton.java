package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

import java.io.File;
import java.util.Random;

public class TrackingViagemButton extends Button {
    private final FlowPane etapasTrackingViagemFlowPane;
    public TrackingViagemButton(String nomeTV, FlowPane etapasTrackingViagemFlowPane) {
        super(nomeTV);
        this.etapasTrackingViagemFlowPane = etapasTrackingViagemFlowPane;
        createViews();
        registerHandlers();
        update();
    }
    private void createViews() {
        setMinSize(100, 70);
        setStyle("-fx-background-color: #546E7A; -fx-background-radius: 25px; -fx-font-size: 20px; -fx-text-fill: #ffffff; -fx-padding: 5px 10px;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            AppMain.appManager.selecionaViagem(this.getText());
            AppDMController.trackingViagem.setValue(this.getText());
            preencheListaEtapas();
        });
    }
    private void update() {
    }

    private void preencheListaEtapas(){
        etapasTrackingViagemFlowPane.getChildren().clear();
        String [] etapas = AppMain.appManager.getEtapasTrackingViagens();
        if (etapas.length > 0) {
            for (String etapa : etapas)
                etapasTrackingViagemFlowPane.getChildren().add(new EtapaButton(etapa));
        } else
            etapasTrackingViagemFlowPane.getChildren().add(new Label("Sem Etapas Adicionadas"));
    }
}
