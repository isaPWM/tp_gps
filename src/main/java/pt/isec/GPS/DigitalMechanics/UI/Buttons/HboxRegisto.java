package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Abastecimento;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Manutencao;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.MudancaOleo;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.RegistoGeral;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

public class HboxRegisto extends HBox {
    private Button btnEliminar, btnRegistoMainInfo, btnEditar;
    private Record registo;
    private Label textoInfoBox;
    private VBox infoBox;
    private FlowPane listaRegistosInfoScrollPane;
    public HboxRegisto(Record registo, Label textoInfoBox, VBox infoBox, FlowPane listaRegistosInfoScrollPane){
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.registo = registo;
        createViews();
        registerHandlers();
        update();
    }

    /*
        btnLembrete.setOnAction(e -> {
        labelMiniform.setText(registo.nome());
        miniform.setVisible(true);
        });
    */
    /*
    receber forms por parâmetro Forms
        btnEditar.setOnAction(e -> {
            consoante o tipo de registo, chamava-se o form correspondente
            ///
            preenche dados form;
            textield.setText(registo.blablabla());
            ///
        });
    */

    private void createViews() {
        btnEliminar = new Button();
        btnEliminar.setPrefSize(30, 30);
        btnEliminar.setStyle("-fx-background-color: transparent");
        ImageView iv = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/delete.png"))));
        iv.setFitHeight(30);
        iv.setFitWidth(30);
        btnEliminar.setGraphic(iv);

        btnRegistoMainInfo = new Button();
        if (registo instanceof Abastecimento a) {
            btnRegistoMainInfo.setText("Abastecimento de " + a.data());
        }
        else if (registo instanceof Manutencao m) {
            btnRegistoMainInfo.setText(m.tipo() + " em " + m.data());
        }
        else if (registo instanceof MudancaOleo mo) {
            btnRegistoMainInfo.setText(mo.oleo() + " Litros em " + mo.data());
        }
        else {
            btnRegistoMainInfo.setText(((RegistoGeral)registo).nomeRegisto() + " em " + ((RegistoGeral)registo).data());
        }
        btnRegistoMainInfo.setPrefSize(Region.USE_COMPUTED_SIZE, 30);
        btnRegistoMainInfo.setMinWidth(350);
        btnRegistoMainInfo.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 18px; -fx-background-radius: 25px; -fx-background-color: #E8EAF6; -fx-text-fill: #201033;");

        HBox.setHgrow(btnRegistoMainInfo, javafx.scene.layout.Priority.ALWAYS);
        this.setStyle("-fx-border-radius: 25px;-fx-background-radius: 25px; -fx-background-color: #E8EAF6; -fx-border-color: #6e76aa; -fx-padding: 5px;");

        this.getChildren().addAll(btnRegistoMainInfo, btnEliminar);
        if(!(registo instanceof Abastecimento)) {
            btnEditar = new Button();
            btnEditar.setPrefSize(30, 30);
            btnEditar.setStyle("-fx-background-color: transparent");
            ImageView ivEdi = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/pen.png"))));
            ivEdi.setFitHeight(30);
            ivEdi.setFitWidth(30);
            btnEditar.setGraphic(ivEdi);
            this.getChildren().add(btnEditar);
        }

        btnEliminar.setVisible(!AppDMController.UIleitura.getValue());
        if(!(registo instanceof Abastecimento))
            btnEditar.setVisible(!AppDMController.UIleitura.getValue());
    }

    private void registerHandlers() {
        btnRegistoMainInfo.setOnAction(e -> {
            textoInfoBox.setText(registo.toString());
            infoBox.setVisible(true);
        });
        btnEliminar.setOnAction(e -> {
            if(!AppMain.appManager.eliminaInfo(registo)) {
                textoInfoBox.setText("Não foi possível eliminar o registo!");
                infoBox.setVisible(true);
            }
            else {
                textoInfoBox.setText("Registo eliminado com sucesso!");
                infoBox.setVisible(true);
                listaRegistosInfoScrollPane.getChildren().remove(this);
            }
        });
        if(!(registo instanceof Abastecimento)) {
            btnEditar.setOnAction(e -> {
                AppMain.appManager.setRegistoAcedido(registo);
                AppDMController.editarRegisto.setValue(true);
            });
        }
    }

    private void update() {
    }
}
