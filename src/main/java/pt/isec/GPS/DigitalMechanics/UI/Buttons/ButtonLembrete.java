package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.Lembrete;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

import java.time.LocalDate;

public class ButtonLembrete extends HBox {
    private final Lembrete lembrete;
    private Button btnEliminar, visto;
    private final String [] style = new String[]{"-fx-background-color: #546E7A;", "-fx-background-color: #90A4AE;"};

    public ButtonLembrete(Lembrete lembrete){
        this.lembrete = lembrete;
        createViews();
        registerHandlers();
    }

    private void createViews() {
        visto = new Button("mudar n/visto");
        visto.setVisible(lembrete.isEstadoAberto());
        visto.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 10px; -fx-background-radius: 25px; -fx-background-color: #546E7A; -fx-text-fill: WHITE;");
        btnEliminar = new Button();
        btnEliminar.setPrefSize(30, 30);
        btnEliminar.setStyle("-fx-background-color: transparent");
        ImageView iv = new ImageView(new Image(String.valueOf(AppMain.class.getResource("imgIcons/delete.png"))));
        iv.setFitHeight(30);
        iv.setFitWidth(30);
        btnEliminar.setGraphic(iv);

        Label infoLembrete = new Label(lembrete.getNomeLembrete() + " em " + lembrete.getData());
        infoLembrete.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 16px;  -fx-text-fill: WHITE;");

        setStyle(style[lembrete.isEstadoAberto() ? 1 : 0]);
        setPrefWidth(390);
        this.setAlignment(Pos.CENTER);
        this.setSpacing(30);
        this.setPadding(new javafx.geometry.Insets(10));
        this.setMinWidth(400);
        this.getChildren().addAll(visto, infoLembrete, btnEliminar);
    }

    private void registerHandlers() {
        this.setOnMouseClicked(e -> {
            if(e.getClickCount() == 1) {
                AppMain.appManager.mudaParaVisto(lembrete.getId());
                setStyle(style[1]);
                visto.setVisible(true);
            }
        });

        visto.setOnAction(e -> update());

        btnEliminar.setOnAction(e -> {
            AppMain.appManager.eliminaLembrete(lembrete.getId());
        });
    }

    private void update() {
        AppMain.appManager.mudaParaNaoVisto(lembrete.getId());
        setStyle(style[0]);
        visto.setVisible(false);
    }

    public LocalDate getData() {
        return lembrete.getData();
    }

    public boolean isEstadoAberto() {
        return lembrete.isEstadoAberto();
    }
}