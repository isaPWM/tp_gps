package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class BlocoInfoButton extends Button {
    private final Pane listaRegistosInfoPane;
    private final FlowPane listaRegistosInfoScrollPane;
    private final Label tipoRegisto, textoInfoBox;
    private final VBox infoBox;

    public BlocoInfoButton(String tipoInfo, Pane listaRegistosInfoPane, FlowPane listaRegistosInfoScrollPane, Label tipoRegisto, Label textoInfoBox, VBox infoBox){
        super(tipoInfo);
        this.listaRegistosInfoPane = listaRegistosInfoPane;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.tipoRegisto = tipoRegisto;
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        createViews();
        registerHandlers();
        update();
    }

    private void createViews() {
        setPrefHeight(70);
        setMinWidth(100);
        setStyle("-fx-font-family: Arial Bold; -fx-font-size: 16px;-fx-background-radius: 25px; -fx-background-color: #37474F; -fx-text-fill: WHITE;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            listaRegistosInfoScrollPane.getChildren().clear();
            preencheListaRegistos();
        });
    }

    private void update() {
    }

    private void preencheListaRegistos() {
        if (AppMain.appManager.selecionaBlocoInformacao(getText())) {
            ArrayList<Record> lista = AppMain.appManager.getListaRegistosDeBlocoAtual();
            if (!lista.isEmpty()) {
                Collections.sort(lista, new Comparator<Record>() {
                    @Override
                    public int compare(Record registo1, Record registo2) {
                        return registo2.toString().compareTo(registo1.toString());
                    }
                });

                for (Record registo : lista) {
                        if (registo instanceof Peca) {

                            listaRegistosInfoScrollPane.setHgap(0);
                            listaRegistosInfoScrollPane.getChildren().add(new HBoxRegistoPeca(registo, textoInfoBox, infoBox, listaRegistosInfoScrollPane));


                        }
                        else  {
                            listaRegistosInfoScrollPane.getChildren().add(new HboxRegisto(registo, textoInfoBox, infoBox, listaRegistosInfoScrollPane));
                        }
                    }
            } else
                listaRegistosInfoScrollPane.getChildren().add(new Label("Sem Registos Adicionados"));
            listaRegistosInfoPane.setVisible(true);
            tipoRegisto.setText("Registos de " + getText());
        }
    }
}
