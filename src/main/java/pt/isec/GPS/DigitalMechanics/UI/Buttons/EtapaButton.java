package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.trackingViagem.Etapa;
import pt.isec.GPS.DigitalMechanics.UI.AppDMController;

public class EtapaButton extends Button{

    private String etapa;
    public EtapaButton(String etapa){
        this.etapa = etapa;
        createViews();
        registerHandlers();
        update();
    }

    private void createViews() {
        setText(etapa);
        setStyle("-fx-background-color: #546E7A; -fx-background-radius: 25px; -fx-font-size: 15px; -fx-text-fill: #ffffff; -fx-padding: 5px 10px;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            AppMain.appManager.selecionaEtapa(this.getText());
            AppDMController.mostrarEtapaInfo.set(true);
        });
    }

    private void update() {
    }
}
