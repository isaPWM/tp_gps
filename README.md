# Digital & Mechanics

## Contents
- [Team](#team)
- [Requirements](#requirements)
- [Vision and Scope](#vision-and-scope)
    - [Use case diagram](#use-case-diagram)
    - [Mockups](#mockups)
    - [User stories](#user-stories)
- [Definition of Done](#definition-of-done)
- [Architecture and Design](#architecture-and-design)
    - [Domain Model](#domain-model)
- [Risk Plan](#risk-plan)
- [Pre-Game](#pre-game)
- [Release Plan](#release-plan)
    - [Release 1](#release-1)
    - [Release 2](#release-1)
- [Increments](#increments)
    - [Sprint 1](#sprint-1)
    - [Sprint 2](#sprint-2)
    - [Sprint 3](#sprint-3)

## Team

- Daniel Rodrigues a2021142013@isec.pt - Nº: 2021142013
- Flávio Rodrigues flaviospr02@gmail.com - Nº: 2020137206
- Francisco Mota a21240369@isec.pt - Nº: 2014008556
- Isabel Prieto a2021133536@isec.pt - Nº: 2021133536
- João Costa joaomiguelcostac@gmail.com - Nº: 2021141594

***

## Vision and Scope

#### Problem Statement
##### Project background
Este projeto visa solucionar desafios de organização no histórico de manutenção de veículos, promovendo fácil acesso e registro preciso de intervenções para otimizar a gestão sobre as informações relativas aos carros dos utilizadores.

##### Stakeholders

Proprietário de Veículos e mecânicos:

Os proprietários de veículos são as uma das principais partes interessadas que utilizam a aplicação para armazenar e gerir de forma segura as informações do seu veículo digitalmente, procurando conveniência, organização e facilidade de acesso sem precisar de carregar documentos físicos. Os os mecânicos são a outra parte que interessa-se no acesso e visualização dos registos dos seus clientes.

##### Users

**Proprietário:**
    Aceder a veículo;
    Aceder a informaçãos e históricos de um veiculo registado.
    Registrar info ao histórico de manutenção, trocas de óleo, abastecimentos e peças/produtos comprados;
    Ter lembretes para a manutenção programada, como troca de óleo, inspeção, etc;
    Criar lembretes para a ações sobre o veículo;
    Registrar informações sobre peças substituídas e custos associados;
    Importar registo de cliente;
    Exportar registo de cliente.

**Mecânico:**
    Aceder ao histórico do veiculo.
    Importar regiso de cliente.

***

#### Vision & Scope of the Solution

##### Vision statement
O objetivo é criar um registo digital que permite armazenar informações de um ou mais veículos do proprietário.


##### List of features

**Registo de automóvel:**
    - Permite registar um ou mais automóveis na plataforma.

**Registo de manutenção/revisão:**
    - Permite registar as mudanças periódicas do automóvel.

**Registo de peças mudadas:**
    - Permite registar, com devido nome, fotografia da peça reposta e o respetivo preço, valor pelo qual custou.

**Registo de intervenções ao carro:**
    - A cada ida à bomba de gasolina é possível criar um registo com: data, preço total, litros e kilometros do carro; 
    - Adicionar registos sobre outras intervenções como mudança de óleo, revisões, etc.

**Gráfico geral de gastos no carro:**
    - Apresenta mensalmente e anualmente os gastos efetuados em manutenção e peças compradas para o respetivo automóvel.

**Notificações importantes:**
    - Apresenta ao utilizador importantes informações, para evitar futuros problemas no seu veiculo.

**Categorização e Etiquetagem de Documentos:**
    - Permitir que os utilizadores classifiquem e etiquetem os seus documentos com base no tipo de documento, data de validade ou outros critérios personalizados para uma gestão eficiente e recuperação fácil.

**Carregamento de Documentos:**
    - A funcionalidade que permite aos utilizadores carregar documentos na aplicação para adicionar estes mesmos aos registos.

**Exportação e Importação de registo de veículo via documentos para outros dispositivos:**
    - Permite partilha de registos entre utilizadores.
    - Permite definir que acesso se terá ao registo quando exportado.


##### Features that will not be developed

**Assistência técnica:**
    - Auxiliar os utilizadores na navegação e utilização eficaz do sistema;
    - Resolver problemas técnicos ou fornecer informações adicionais quando necessário.

**Captura de imagens:**
    - Funcionalidade que permite aos utilizadores de aceder à câmara apartir da aplicação para as adicionar aos registos.

**Exportar registo de veículo via cloud para outros dispositivos:**
    - Permite dar esta informação a outro utilizador via cloud ligada à aplicação.
    - Permitir que os utilizadores partilhem documentos ou deem permições específicas de forma segura com partes autorizadas, recorrende a internete e à aplicação.

##### Assumptions

Preparação do utilizador:
    Utilizadores estarem comfortaveis com a utilização de telemoveis e suas aplicações, permitindo uma boa adaptação com o gerenciamento das informações.

Compatibilidade com Dispositivos:
    A aplicação é compatível com uma ampla gama de smartphones e sistemas operativos para acomodar os diversos dispositivos utilizados pelos potenciais utilizadores.

Preocupações com a Privacidade dos Dados:
    Os utilizadores confiam que a aplicação irá empregar medidas de segurança robustas para proteger os seus dados sensíveis, abordando preocupações relacionadas com a privacidade dos dados e o acesso não autorizado.

***

## Requirements

#### Use Case Diagram


![Visualização do Caso de Uso](imgs/DCU_DM.png)

##### Mockups


Painel inicial:

[<img src="imgs/imagem1.png" width="300"/>](image.png)

-------------------------------------------------------------------------

Criação de registos:

- Mudança de Óleo - US 1:
- 
  [<img src="imgs/mockupUS1.png" width="300"/>](imgs/mockupUS1.png)
  [<img src="imgs/mockupUS1_1.png" width="300"/>](imgs/mockupUS1_1.png)

-Manutenção - US 2:

[<img src="imgs/mockupUS2.png" width="300"/>](imgs/mockupUS2.png)
[<img src="imgs/mockupUS2_1.png" width="300"/>](imgs/mockupUS2_1.png)

-Abastecimento - US 3:

[<img src="imgs/mockupUS3.png" width="300"/>](imgs/mockupUS3.png)
[<img src="imgs/mockupUS3_1.png" width="300"/>](imgs/mockupUS3_1.png)

-Peça e edição de imagem - US 9 e US 4:

[<img src="imgs/mockupUS9_US4.png" width="300"/>](imgs/mockupUS9_US4.png)

-------------------------------------------------------------------------

Inserção de imagem perfil veículo e eliminação de registo de vículo - US 5 e US 13:

[<img src="imgs/mockupUS5_13.png" width="300"/>](imgs/mockupUS5_13.png)
[<img src="imgs/mockupUS5_13_1.png" width="300"/>](imgs/mockupUS5_13_1.png)

-------------------------------------------------------------------------

Painel blocos de informação registos - US 6:

[<img src="imgs/mockupUS6.png" width="300"/>](imgs/mockupUS6.png)

-------------------------------------------------------------------------

Acesso a lembretes - US 7:

[<img src="imgs/mockupUS7.png" width="300"/>](imgs/mockupUS7.png)

-------------------------------------------------------------------------

Edição de Registo - US 8:

[<img src="imgs/mockupUS8.png" width="300"/>](imgs/mockupUS8.png)
[<img src="imgs/mockupUS8_1.png" width="300"/>](imgs/mockupUS8_1.png)

-------------------------------------------------------------------------

Eliminação de registos - US 10:

[<img src="imgs/mockupUS10.png" width="300"/>](imgs/mockupUS10.png)
[<img src="imgs/mockupUS10_1.png" width="300"/>](imgs/mockupUS10_1.png)

-------------------------------------------------------------------------

Eliminação de registos - US 11 e US12:

[<img src="imgs/mockupUS11_12.png" width="300"/>](imgs/mockupUS11_12.png)

-------------------------------------------------------------------------

Exportação de Registos - US 14 e US15:

[<img src="imgs/mockupUS14_15.PNG" width="300"/>](imgs/mockupUS14_15.PNG)


***

##### User Stories

 - User story 1 (isaPWM/tp_gps#6)
 - User story 2 (isaPWM/tp_gps#7)
 - User story 3 (isaPWM/tp_gps#36)
 - User story 4 (isaPWM/tp_gps#37)
 - User story 5 (isaPWM/tp_gps#48)
 - User story 6 (isaPWM/tp_gps#54)
 - User story 7 (isaPWM/tp_gps#40)
 - User story 8 (isaPWM/tp_gps#41)
 - User story 9 (isaPWM/tp_gps#42)
 - User story 10 (isaPWM/tp_gps#43)
 - User story 11 (isaPWM/tp_gps#44)
 - User story 12 (isaPWM/tp_gps#55)
 - User story 13 (isaPWM/tp_gps#93)
 - User story 14 (isaPWM/tp_gps#94)
 - User story 15 (isaPWM/tp_gps#95)
 - User story 16 (isaPWM/tp_gps#108)
 - User story 17 (isaPWM/tp_gps#109)
 - User story 18 (isaPWM/tp_gps#110)

***

##### Definition of done
(This section is already written, do not edit) It is a collection of criteria that must be completed for a User Story to be considered “done.”

 - All tasks done:
 - CI – built, tested (Junit), reviewed (SonarCloud)
 - Merge request to qa (code review)
 - Acceptance tests passed
 - Accepted by the client
 - Code merged to main

***

##### User Story 1

Descrição: Como proprietário de um veículo, eu pretendo registar os dados da última mudança de óleo digitalmente, para que não me caia no esquecimento esta última intervenção, podendo ser criado um lembrete para a próxima mudança automaticamente, caso o pretenda.

###### Acceptance Criteria

| Cenário 1   | Registo de uma nova mudança de óleo campos válidos e preenchidos                               |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário clicou no botão de adicionar novo registo                                       |
| And         | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| When        | Clicar no botão de confirmar "adicionar registo"                                               |
| Then        | O registo é adicionado à lista de mudanças de óleo                                             |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2   | Registo de uma nova mudança de óleo pelo menos um campo inválido ou não preenchido             |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O proprietário clicou no botão de adicionar novo registo                                       |
| And         | Nem todos os campos obrigatórios estão preenchidos ou válidos                                  |
| When        | Clicar no botão de confirmar "adicionar registo"                                               |            
| Then        | É avisado de que há campos em falta                                                            |
| And         | O registo não é adicionado à lista de mudanças de óleo                                         |
| And         | O respetivo documento de informações físico mantém-se inalterado                               |

| Cenário 3 | Registo duma nova mudança de óleo com lembrete automático                                        |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | o proprietário clicou no botão de adicionar novo registo                                         |
| And       | Todos os campos obrigatórios estão preenchidos  e válidos                                        |
| And       | A opção de gerar lembrete para a próxima mudança está selecionado                                |
| When      | Clicar no botão de confirmar "adicionar registo"                                                 |
| Then      | O registo é adicionado à lista de mudanças de óleo                                               |
| And       | É criado um lembrete para a próxima mudança                                                      |
| And       | A lista de lembretes é atualizada com o novo lembrete                                            |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado   |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário o cliente poder registar a mudança de óleo.

------------------------------------------
##### User Story 2
###### Descrição

Como proprietário de veículos, pretendo realizar registos de todas as manutenções de forma a ter um seguimento das intervenções	que cada veículo meu teve.

###### Acceptance Criteria

| Cenário 1 | Registo de uma nova manutenção campos válidos e preenchidos                                    |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou no botão de adicionar novo registo                                         |
| And       | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| When      | Clicar no botão de confirmar "adicionar registo"                                               |
| Then      | O registo é adicionado à lista de manutenções                                                  |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | Registo de uma nova manutenção pelo menos um campo não preenchido ou inválido |
|:----------|:------------------------------------------------------------------------------|
| Given     | Cliquei no botão de adicionar novo registo                                    |
| And       | Nem todos os campos obrigatórios estão preenchidos ou válidos                 |
| When      | Clicar no botão de confirmar "adicionar registo"                              |
| Then      | É avisado de que há campos em falta                                           |
| And       | O registo não é adicionado à lista de manutenções efetuadas no veiculo        |
| And       | O respetivo documento de informações físico mantém-se inalterado              |

| Cenário 3 | Registo de uma nova manutenção com lembrete automático                                         |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou no botão de adicionar novo registo                                         |
| And       | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| And       | A opção de gerar lembrete para a próxima mudança está selecionada                              |
| When      | Clicar no botão de confirmar "adicionar registo"                                               |
| Then      | O registo é adicionado à lista de manutenções                                                  |
| And       | É criado um lembrete para a próxima manutenção                                                 |
| And       | A lista de lembretes é atualizada                                                              |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

###### Story Points

Story Points - Large

###### MoSCoW

**Must Have**. É necessário o cliente poder registar as devidas manutenções que realizou no seu veículo.

------------------------------------------
##### User Story 3
###### Descrição

Como proprietário de uma carrinha, quero realizar o registo de cada reabastecimento de combustível de maneira a conseguir perceber os gastos que tenho e os quilómetros que conduzi a fim de um tempo.

###### Acceptance Criteria

| Cenário 1 | O utilizador cria um novo registo de abastecimento com inserção válida e completa de dados     |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no botão de adicionar novo registo                                       |
| And       | Todos os campos seobrigatórios estão preenchidos e válidos                                       |
| When      | Clicar no botão de confirmar "adicionar registo"                                               |
| Then      | O registo é adicionado à lista de abastecimentos                                               |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | O utilizador cria um novo registo de abastecimento sem preencher todos os campos ou com informação inválida  |
|:----------|:-------------------------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no botão de adicionar novo registo                                                     |
| And       | Nem todos os campos obrigatórios estão preenchidos ou válidos                                                |
| When      | Clicar no botão de confirmar "adicionar registo"                                                             |
| Then      | É avisado de que há campos em falta                                                                          |
| And       | O registo não é adicionado à lista de abastecimentos do veículo                                              |
| And       | O respetivo documento de informações físico mantém-se inalterado                                             |

###### Story Points

Story Points - Small

###### MoSCoW

**Must Have**. É necessário o cliente poder registar as vezes que abasteceu o veículo.

------------------------------------------
##### User Story 4
###### Descrição

Como proprietário de uma mota, pretendo que o sistema possibilite acrescentar fotografias dos produtos ou peças aos seus registos para que seja mais fácil e rápido de identificar novamente o produto para comprar.

###### Acceptance Criteria

| Cenário 1 | O Utilizador adiciona de uma imagem a um registo de produto                                    |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou sobre um registo de produto/peça                                         |
| And       | O proprietário clicou em editar registo                                                        |
| When      | O proprietário inseriu a localização válida para uma imagem válida                             |
| And       | Clicar no botão de confirmar edição                                                            |
| Then      | O registo é adicionado à lista de peças                                                        |
| And       | O registo fica uma imagem associada                                                            |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | O Utilizador adiciona uma imagem a um registo de produto                                                 |
|:----------|:---------------------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou sobre um registo de produto                                                        |
| And       | O proprietário clicou em editar registo                                                                  |
| When      | O proprietário inseriu uma localização inválida para uma imagem ou o documento da localização é inválido |
| And       | Clicar no botão de confirmar edição                                                                      |
| Then      | A edição não é efetuada                                                                                  |
| And       | O registo fica uma imagem associada                                                                      |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito mantém-se inalterado   |


###### Story Points

Story Points - Small

###### MoSCoW

**Should Have**. É importante a existência de fotografias para uma mais fácil identificação dos produtos mas já que essa informação está presente de outras formas, o que não é vital.

------------------------------------------
##### User Story 5
###### Descrição

Como proprietário de veículos, pretendo que o sistema me permita escolher uma fotografia como capa de cada bloco digital com vista a identificar mais facilmente o bloco do veículo ao qual pretendo aceder.

###### Acceptance Criteria

| Cenário 1 | O Utilizador regista um veículo com fotografia                                                  |
|:----------|:------------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no botão de adicionar novo registo de veículo                             |
| And       | Todos os campos obrigatórios estão preenchidos e válidos                                        |
| And       | Adicionou um ficheiro válido para imagem do veículo                                             |
| When      | Clicar no botão de confirmar "adicionar veículo"                                                |
| Then      | O registo é adicionado à lista de veículos                                                      |
| And       | O registo de veículo tem uma imagem associada como identificação                                |
| And       | A lista de veículos é atualizada                                                                |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado. |

| Cenário 2 | O Utilizador regista um veículo com fotografia                                                                         |
|:----------|:-----------------------------------------------------------------------------------------------------------------------|
| Given     | O proprietário no botão de adicionar novo registo                                                                      |
| And       | Nem todos os campos obrigatórios estão preenchidos ou válidos ou adicionou um ficheiro inválido para imagem do veículo |
| When      | Clicar no botão de confirmar "adicionar veículo"                                                                       |
| Then      | É avisado de que há campos em falta                                                                                    |
| And       | O registo não é adicionado à lista de veículos                                                                         |
| And       | O respetivo documento de informações físico mantém-se inalterado                                                       |

| Cenário 3 | O Utilizador regista um veículo sem fotografia                                                 |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou no botão de adicionar novo registo                                         |
| And       | Todos os campos obrigatórios e não foi adicionado um ficheiro para imagem do veículo           |
| When      | Clicar no botão de confirmar "adicionar registo"                                               |
| Then      | O registo é adicionado à lista de veículos                                                     |
| And       | O registo de veículo não fica com uma imagem associada como identificação                      |
| And       | A lista de veículos é atualizada                                                               |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |


###### Story Points

Story Points - Small

###### MoSCoW

**Should Have**. É importante a existência de um identificador visual para facilitar a identificação dos veículos mas já que essa informação está presente de outras formas, o que não é vital.

------------------------------------------
##### User Story 6
###### Descrição

Como utilizador de Digital&Mechanics, pretendo ver os registos de veículos de forma digital, para reduzir o espaço ocupado por todos os documentos e papeis relacionados com o veículo.

###### Acceptance Criteria

| Cenário 1 | O utilizador seleciona um bloco de informação não vazio de um registo de veículo |
|:----------|:---------------------------------------------------------------------------------|
| Given     | O utilizador clicou num registo de veiculo                                       |
| And       | Há informação registada                                                          |
| When      | Selecionar num bloco de informação                                               |
| Then      | As informações sobre o carro que estão registadas nesse bloco tornam-se visíveis |

| Cenário 2 | O utilizador seleciona um bloco de informação vazio de um registo de veículo     |
|:----------|:---------------------------------------------------------------------------------|
| Given     | O utilizador num registo de veiculo                                              |
| And       | Não há informação registada                                                      |
| When      | Selecionar num bloco de informação                                               |
| Then      | Será mostrado um aviso "sem registos neste bloco"                                |

###### Story Points

Story Points - Small

###### MoSCoW

**Must Have**. É necessário o cliente poder visualizar as informações do seu veículo.

------------------------------------------
##### User Story 7
###### Descrição

Como proprietário de um veículo, pretendo aceder aos meus lembretes para estar ciente das notificações perdidas ou por ver e não esquecer datas importantes.

###### Acceptance Criteria


| Cenário 1 | O Utilizador acede aos lembretes ativos                                                        |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no toggle dos lembretes ativos                                           |
| And       | E não definiu um filtro para a data                                                            |
| When      | A lista de lembretes for filtrada segundo os filtros                                           |
| Then      | É mostrada uma lista de todos os lembretes ativos no ecrã                                      |
| Given     | O proprietário vê o lembrete que pretende abrir                                                |
| When      | O proprietário abrir esse lembrete                                                             |
| Then      | É mostrada a informação associada a esse lembrete                                              |
| And       | Esse lembrete passa a estar com estado "visto"                                                 |
| And       | Esse lembrete deixa de ser apresentado em lembretes ativos                                     |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | O Utilizador acede a um lembrete por engano                                                    |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário abriu um lembrete ativo por engano                                              |
| And       | O lembrete ficou marcado como visto                                                            |
| When      | O proprietário marcar o lembrete como não visto                                                |
| Then      | O lembrete volta a estar com estado "ativo"                                                    |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 3 | O Utilizador acede aos lembretes com filtro de data e ativos                                     |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O proprietário inseriu uma data no filtro                                                        |
| And       | E clicou no toggle dos lembretes ativos                                                          |
| When      | A lista de lembretes for filtrada segundo os filtros                                             |
| Then      | É mostrada uma lista de todos os lembretes ativos dentro do intervalo de tempo imposto no ecrã   |
| Given     | O proprietário vê o lembrete que pretende abrir                                                  |
| When      | O proprietário abrir esse lembrete                                                               |
| Then      | É mostrada a informação associada a esse lembrete                                                |
| And       | Esse lembrete passa a estar com estado "visto"                                                   |
| And       | Esse lembrete deixa de ser apresentado em lembretes ativos                                       |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado   |

| Cenário 5 | O Utilizador elimina um lembrete                                                                 |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O proprietário abriu o lembrete que pretende apagar                                              |
| When      | O proprietário clicar no botão para eliminar                                                     |
| Then      | O lembrete é removido da lista de lembretes                                                      |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado   |


###### Story Points

Story Points - Small

###### MoSCoW

**Must Have**. É necessário o cliente poder aceder à lista de lembretes, visto a não perder nenhuma data importante.

------------------------------------------
##### User Story 8
###### Descrição

Como proprietário de um veículo, pretendo conseguir editar registos adicionando lembretes antes não gerados de forma a receber a respetiva notificação futuramente.

###### Acceptance Criteria

| Cenário 1  | O Utilizador cria um lembrete para um registo escolhendo data                                  |
|:-----------|:-----------------------------------------------------------------------------------------------|
| Given      | O proprietário inseriu o id do registo pretendido                                              |
| When       | Ele clicar no botão pesquisar                                                                  |
| Then       | O registo é aberto em modo edição                                                              |
| Given      | O proprietário escolheu uma data                                                               |
| And        | Clicou em gerar lembrete                                                                       |
| When       | O proprietário clicar no botão de confirmar edição                                             |
| Then       | O registo é alterado                                                                           |
| And        | É criado e adicionado um lembrete à lista de lembretes                                         |
| And        | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2  | O Utilizador cria um lembrete para um registo sem escolher data                                |
|:-----------|:-----------------------------------------------------------------------------------------------|
| Given      | O proprietário inseriu o id do registo pretendido                                              |
| When       | Ele clicar no botão pesquisar                                                                  |
| Then       | O registo é aberto em modo edição                                                              |
| Given      | O proprietário não escolheu uma data                                                           |
| And        | Clicou em gerar lembrete                                                                       |
| When       | O proprietário clicar no botão de confirmar edição                                             |
| Then       | O registo é alterado                                                                           |
| And        | É criado um lembrete para daquia 6 meses e adicionado um lembrete à lista de lembretes         |
| And        | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário o cliente poder editar registos com informação importante e adicionar novos lembretes.

------------------------------------------
##### User Story 9
###### Descrição

Como proprietário de um carro, pretendo registar uma peça comprada, para não perder informação sobre a compra do produto facilitando próximas compras.

###### Acceptance Criteria

| Cenário 1 | O Utilizador regista um novo produto com fotografia com campos preenchidos e válidos           |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no botão de adicionar novo registo de produto                            |
| And       | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| And       | Adicionou um ficheiro válido para fotografia do produto                                        |
| When      | Clicar no botão de confirmar "adicionar peça"                                                  |
| Then      | O registo é adicionado à lista de peças                                                        |
| And       | O registo tem uma imagem associada                                                             |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

| Cenário 2 | O Utilizador regista um novo produto com fotografia com campos em falta ou inválidos                                       |
|:----------|:---------------------------------------------------------------------------------------------------------------------------|
| Given     | O proprietário no botão de adicionar novo registo de produto                                                               |
| And       | Nem todos os campos obrigatórios estão preenchidos ou válidos ou adicionou um ficheiro inválido para fotografia do produto |
| When      | Clicar no botão de confirmar "adicionar peça"                                                                              |
| Then      | É avisado de que há campos em falta                                                                                        |
| And       | O registo não é adicionado à lista de peças do veiculo                                                                     |
| And       | O respetivo documento de informações físico mantém-se inalterado                                                           |

| Cenário 3 | O Utilizador regista um novo produto sem fotografia com campos preenchidos e válidos           |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário clicou no botão de adicionar novo registo de produto                            |
| And       | Todos os campos obrigatórios estão preenchidos e válidos                                       |
| And       | Não adicionou um ficheiro válido para do produto                                               |
| When      | Clicar no botão de confirmar "adicionar peça"                                                  |
| Then      | O registo é adicionado à lista de peças                                                        |
| And       | O registo não tem uma imagem associada                                                         |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário o cliente registar as peças comprou e mudou no seu veiculo.

------------------------------------------
##### User Story 10
###### Descrição

Como proprietário de um veículo, pretendo eliminar registos de maneira a ter um bloco digital sem informação desnecessária.

###### Acceptance Criteria

| Cenário 1 | O utilizador apaga um registo                                                                  |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O proprietário abriu o registo que pretende eliminar                                           |
| When      | Clicar no botão para eliminar                                                                  |
| Then      | É preguntado ao utilizador se tem a certeza de que quer eliminar o registo                     |
| Given     | O proprietário pretende mesmo apagar o resgito                                                 |
| When      | O proprietário selecionar confirmar a eliminação                                               |
| Then      | O registo é eliminado                                                                          |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |


###### Story Points

Story Points - Small

###### MoSCoW

**Must Have**. É necessário o cliente poder apagar registos que já não são necessários de modo a não confundir o utilizador que acede a esses registos, com informação incorreta/demasiado antiga/desnecessária.

------------------------------------------
##### User Story 11
###### Descrição

Como mecânico, quero importar um bloco digital de um cliente novo de maneira a aceder às informações do seu veículo.

###### Acceptance Criteria

| Cenário 1.1 | Importar o bloco digital de um novo cliente                                                      |
|:------------|:-------------------------------------------------------------------------------------------------|
| Given       | O mecânico clicou em adicionar registo de veículo                                                |
| And         | Foi selecionado importar bloco                                                                   |
| And         | Foi inserida a localização do documento importado referente ao bloco de registos do cliente novo |
| And         | A senha de acesso inserida para importe do registo na aplicação está correta                     |
| When        | O mecânico clicar no botão "importar"                                                            |
| Then        | A aplicação criará um novo registo de veículo apartir do documento importado                     |
| And         | O documento só estará apto para ações de leitura                                                 |

| Cenário 1.2 | Importar o bloco digital de um novo cliente                                          |
|:------------|:-------------------------------------------------------------------------------------|
| Given       | O mecânico clicou em adicionar registo de veículo                                    |
| And         | Foi selecionado importar bloco                                                       |
| And         | Foi inserida ou uma localização do documento a importar ou senha de acesso inválida. |
| When        | O mecânico clicar no botão "importar"                                                |
| Then        | A aplicação não criará um novo registo importado de veículo                          |
| And         | O mecânico será avisado de que inseriu dados incorretos                              |

###### Story Points

Story Points - Large

###### MoSCoW

**Must Have**. É necessário os mecânicos conseguirem colecionar os registos dos clientes tendo a informação à mão.

------------------------------------------
##### User Story 12
###### Descrição

Como prorietário de um veículo em segunda mão, quero importar o bloco digital deste veículo que foi exportado pelo proprietário antigo, no intuito a ter acesso ao background do meu veículo de dar continuidade ao seu bloco de registos.

###### Acceptance Criteria

| Cenário 1.1 | Importar o bloco digital de um antigo dono                                             |
|:------------|:---------------------------------------------------------------------------------------|
| Given       | O novo proprietário clicou em adicionar registo de veículo                             |
| And         | Foi selecionado importar bloco                                                         |
| And         | Foi inserida a localização do documento importado referente ao registo de veículo novo |
| And         | Foi inserida a senha correta de acesso para importe do registo na aplicação            |
| When        | O novo proprietário clicar no botão "importar"                                         |
| Then        | A aplicação criará um novo registo de veículo apartir do documento importado           |
| And         | O documento estará apto para ações de leitura e escrita                                |

| Cenário 1.2 | Importar o bloco digital de um antigo dono                                           |
|:------------|:-------------------------------------------------------------------------------------|
| Given       | O novo proprietário clicou em adicionar registo de veículo                           |
| And         | Foi selecionado importar bloco                                                       |
| And         | Foi inserida ou uma localização do documento a importar ou senha de acesso inválida. |
| When        | O novo proprietário clicar no botão "importar"                                       |
| Then        | A aplicação não criará um novo registo importado de veículo                          |
| And         | O novo proprietário será avisado de que inseriu dados incorretos                     |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário os prorietários de um carro em segunda-mão tenham acesso ao histórico do veículo que passou a ser sua proriedade.

------------------------------------------
##### User Story 13
###### Descrição

Como utilizador de Digital & Mecanics, quero poder eliminar um bloco digital de um veículo com vista a não acumular informação desnecessária no meu disco.

###### Acceptance Criteria

| Cenário 1 | O Utilizador elimina dados registados                                                                        |
|:----------|:-------------------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a eliminar abrindo o perfil do registo             |
| When      | Clicar em eliminar registo                                                                                   |
| Then      | É perguntado se tem a certeza de que pretende eliminar o registo de veículo                                  |
| Given     | É mesmo pretendido proceder com a eliminação                                                                 |
| When      | O utilizador selecionar confirmar a eliminação                                                               |
| Then      | O registo de veículo é eliminado                                                                             |
| And       | O documento físico de informações do veículo ao qual o bloco digital diz respeito é eliminado do dispositivo |

###### Story Points

Story Points - Small

###### MoSCoW

**Must Have**. É necessário que os utilizadores possam eliminar os registos de que já não tenham mais necessidade de ter.

------------------------------------------
##### User Story 14
###### Descrição

Como proprietário, quero poder exportar um bloco digital de um veículo de que sou proprietário para que seja possível outros poderem analizá-lo remotamente.

###### Acceptance Criteria

| Cenário 1 | Exportar bloco digital do veículo a mecânico                                                     |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a exportar abrindo o perfil do registo |
| When      | O utilizador clicar em exportar registo veículo                                                  |
| And       | Escolher exportar para leitura                                                                   |
| Then      | É gravada uma senha de importação no registo                                                     |
| And       | É ativada a flag de "leitura" no registo                                                         |
| And       | O ficheiro de registo de veículo é exportado para uma pasta zip                                  |
| And       | É exibida uma mensagem com a localização da pasta zip e a respetiva senha de acesso para importe |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário que os utilizadores possam partilhar os registos com outros, deoutra forma não seria possível acedê-lo sem ser no dispositivo onde ele foi criado.

------------------------------------------
##### User Story 15
###### Descrição

Como proprietário que vai vender um veículo, quero poder exportar o bloco digital do veículo a vender ao comprador no intuito de passar toda a informação registada sobre este veículo ao seu novo proprietário.

###### Acceptance Criteria

| Cenário 1 | Exportar bloco digital do veículo a novo proprietário                                            |
|:----------|:-------------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou duplamente sobre o registo de veículo a exportar abrindo o perfil do registo |
| When      | O utilizador clicar em exportar registo veículo                                                  |
| And       | Escolher exportar para novo proprietário                                                         |
| Then      | É gravada uma senha de importação no registo                                                     |
| And       | É ativada a flag de "novo proprietário" no registo                                               |
| And       | O ficheiro de registo de veículo é exportado para uma pasta zip                                  |
| And       | É exibida uma mensagem com a localização da pasta zip e a respetiva senha de acesso para importe |

###### Story Points

Story Points - Medium

###### MoSCoW

**Must Have**. É necessário que os ex-proprietários possam partilhar os registos com os novos proprietários do veículo de forma a evitar perdas de informação e desconhecimento sobre o veículo adquirido.

------------------------------------------
##### User Story 16
###### Descrição

Como proprietário que vai realizar viagens, quero criar um tracking de viagem para poder registar os kms realizados e os gastos de combustível por cada paragem e abastecimento da viagem.

###### Acceptance Criteria

| Cenário 1.1 | Registar a viagem                                                                              |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O utilizador clicou sobre criar viagem                                                         |
| And         | O utilizador clicou inseriu o nome, destino, quilometros atuais e observações(opt)                                                         |
| When        | O utilizador confirmar a criação do tracking de viagem                                         |
| Then        | É criado um novo tracking de viagem                                                            |
| And         | A lista de viagens é atualizada                                                                |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

###### Story Points

Story Points - Large

###### MoSCoW

**Should Have**. O utilizador deve poder criar um registo de viagem caso o pretenda.

------------------------------------------
##### User Story 17
###### Descrição

Como proprietário, quero atualizar um tracking de viagem, para ter um tracking de viagem detalhado.

###### Acceptance Criteria

| Cenário 1   | Registar nova etapa                                                                            |
|:------------|:-----------------------------------------------------------------------------------------------|
| Given       | O utilizador clicou sobre o tracking de viagem pretendido                                      |
| And         | O utilizador clicou em adicionar etapa                                                         |
| And         | O utilizador marcou os kms do carro atuais, a sua localização e horas                          |
| When        | O utilizador confirmar a adição de etapa                                                       |
| Then        | É adicionada uma nova etapa ao tracking de viagem                                              |
| And         | O registo de viagens é atualizado                                                              |
| And         | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |


| Cenário 2 | Registar última etapa                                                                          |
|:----------|:-----------------------------------------------------------------------------------------------|
| Given     | O utilizador clicou sobre o tracking de viagem pretendido                                      |
| And       | O utilizador clicou em adicionar última etapa                                                  |
| And       | O utilizador marcou os kms do carro atuais, a sua localização e horas                          |
| When      | O utilizador confirmar a adição de etapa                                                       |
| Then      | É adicionada a última etapa ao tracking de viagem                                              |
| And       | O tracking de viagem é marcado como concluído e atualizado                                     |
| And       | O tracking passa a ser imutável                                                                |
| And       | O documento de informações do veículo físico ao qual o bloco digital diz respeito é atualizado |

###### Story Points

Story Points - Large

###### MoSCoW

**Should Have**. Deve ser possível que o utilizador atualize e termine um registo de viagem.

***
##### User Story 18
###### Descrição

Como proprietário, quero criar um novo registo de veículo, para registar tudo relacionada ao meu veículo.

###### Acceptance Criteria

| Cenário 1 | O utilizador cria um novo registo de veículo com inserção válida e completa de    |
|:----------|:----------------------------------------------------------------------------------|
| Given     | O novo proprietário clicou em adicionar registo de veículo                        |
| And       | Foi selecionado criar bloco                                                       |
| And       | Foram inseridos todos os dados identificativos do veículo                         |
| And       | Os dados inseridos têm um formato válido                                          |
| And       | Não existe um registo com a mesma matrícula                                       |
| When      | O novo proprietário clicar no botão "criar"                                       |
| Then      | A aplicação criará um novo registo de veículo                                     |
| And       | O documento estará apto para ações de leitura e escrita                           |

| Cenário 2 | O utilizador cria um novo registo de veículo inserindo uma matrícula já registada |
|:----------|:----------------------------------------------------------------------------------|
| Given     | O novo proprietário clicou em adicionar registo de veículo                        |
| And       | Foi selecionado criar bloco                                                       |
| And       | Foram inseridos todos os dados identificativos do veículo                         |
| And       | Os dados inseridos têm um formato válido                                          |
| And       | Já existe um registo com a mesma matrícula                                        |
| When      | O novo proprietário clicar no botão "criar"                                       |
| Then      | A aplicação não criará um novo registo de veículo                                 |

| Cenário 3 | O utilizador cria um novo registo de veículo inserindo dados inválidos            |
|:----------|:----------------------------------------------------------------------------------|
| Given     | O novo proprietário clicou em adicionar registo de veículo                        |
| And       | Foi selecionado criar bloco                                                       |
| And       | Foram inseridos todos os dados identificativos do veículo                         |
| And       | Nem todos dados inseridos têm um formato válido                                   |
| When      | O novo proprietário clicar no botão "criar"                                       |
| Then      | A aplicação não criará um novo registo de veículo                                 |

| Cenário 4 | O utilizador cria um novo registo de veículo sem preencher todos os campos        |
|:----------|:----------------------------------------------------------------------------------|
| Given     | O novo proprietário clicou em adicionar registo de veículo                        |
| And       | Foi selecionado criar bloco                                                       |
| And       | Não foram inseridos todos os dados identificativos do veículo                     |
| When      | O novo proprietário clicar no botão "criar"                                       |
| Then      | A aplicação não criará um novo registo de veículo                                 |

###### Story Points

Story Points - Large

###### MoSCoW

**Must Have**. Deve ser possível que crie um registo de veículo para o seu carro.

***

## Architecture and Design

#### Domain Model

![Visualização do destaque](imgs/ModeloDominioV1.png)
***

#### Risk Plan

#### Threshhold of Success

Ao entregar a aplicação ao cliente:
 - Todas as features "Must" devem estar totalmente implementadas e funcionais;
 - Todas as features "Should" devem estar totalmente implementadas e funcionais;
A aplicação deve ir ao encontro do propósito do projeto.

#### Risk List

- RISK 1 - [ PxI: 2*4=8 ] A equipa não tem experiência em trabalhar com grupos desta dimensão, o que pode causar problemas de organização resultando numa divisão desequilibrada de tarefas.
- RISK 2 - [ PxI: 2*3=6 ] É a primeira vez que a equipa realiza um projeto desta dimensão, o que pode significar falta de experiência para tomadas de decisão corretas resultando em perdas de tempo e atrasos na conclusão das tarefas.
- RISK 3 - [ PxI: 5*5=25] Devido a outros trabalhos e projetos externos, pode não ser investido o tempo planeado neste projeto provocando a entrega de uma aplicação que não vai de encontro ao pretendido.

#### Mitigation Actions

- RISK 3 -(MS): Manter a comunicação transparente para garantir a cooperação entre elemnetos do grupo caso
algum esteja com menos tempo que posso inverter na realização das suas tarefas.

***

#### Pre-Game

#### Sprint 0 Plan

 - Goal: Ter tudo aprovado pelo cliente e os recursos necessários para avançar com a implementação.
 - Dates: De 12/10/2023 a 26/10/2023, 2 semanas.
 - Sprint 0 Backlog (don't edit this list):
   - Task1 – Write Team;
   - Task2 – Write V&S;
   - Task3 – Write Requirements;
   - Task4 – Write DoD;
   - Task5 – Write Architecture&Design;
   - Task6 – Write Risk Plan;
   - Task7 – Write Pre-Gane;
   - Task8 – Write Release Plan;
   - Task9 – Write Product Increments;
   - Task10 – Create Product Board;
   - Task11 – Create Sprint 0 Board;
   - Task12 – Write US in PB, estimate (SML), prioritize (MoSCoW), sort;
   - Task13 – Create repository with “GPS Git” Workflow.

## Release Plan

#### Release 1

Objetivo: Apresentar um Minimum value Product ao Cliente.

Funcionalidades requeridas pelo cliente:

| Data prevista | Prioridade(MoSCow) | Estimativa(T-shirt size) | Funcionalidade                                                                            |
|:--------------|:-------------------|:-------------------------|:------------------------------------------------------------------------------------------|
| 16/Nov        | Must               | Small                    | Um utilizador da aplicação deve conseguir aceder às informaçãos que constam no registo.   |
| 16/Nov        | Must               | Small                    | O proprietário deve poder registar um abastecimento feito à lista de abastecimentos.      |
| 16/Nov        | Must               | Medium                   | O proprietário deve poder registar uma mudança de óleo feita à lista de mudanças de óleo. |
| 16/Nov        | Must               | Medium                   | O proprietário deve conseguir registar uma peça comprada à lista de peças.                |
| 16/Nov        | Must               | Large                    | O proprietário deve conseguir registar manutenções e revisões à respetiva lista.          |
| 30/Nov        | Must               | Small                    | O proprietário deve conseguir eliminar lembretes.                                         |
| 30/Nov        | Must               | Medium                   | O proprietário deve conseguir adicionar lembretes.                                        |
| 30/Nov        | Must               | Small                    | O proprietário deve conseguir eliminar registos.                                          |
| 30/Nov        | Must               | Medium                   | O proprietário deve conseguir editar registos de informação ou veículo + fotografias.     |
| 30/Nov        | Must               | Small                    | O utilizador deve poder eliminar um registo de veículo registado na aplicação.            |
| 16/Nov        | Must               | Large                    | Um utilizador deve poder criar um registo de veículo.                                     |

Partes da arquitatura relevantes:
Interface com o utilizador deve ser reproduzir de forma intuitiva as funcionalidades esperadas para esta release.


Team capacity: 3 * 5 * 4 = 60 horas [horas/semana, membros equipa, número semana]	

Data da Release: 30/11/2023

Versão da Release: V0.1


#### Release 2

Objetivo: Apresentar o produto final ao Cliente com todas as features por ele estipuladas.

Funcionalidades requeridas pelo cliente:

| Due Time | Prioridade(MoSCow) | Estimativa(T-shirt size) | Funcionalidade                                          |
|:---------|:-------------------|:-------------------------|:--------------------------------------------------------|
| 14/Dec   | Must               | Large                    | O utilizador deve poder importar um registo de veículo. |
| 14/Dec   | Must               | Medium                   | O utilizador deve poder exportar um registo de veículo. |
| 14/Dec   | Should             | Large                    | O proprietário deve poder criar um tracking de viagem.  |
| 14/Dec   | Should             | Large                    | O proprietário deve poder atualizar tracking de viagem. |

Partes da arquitatura relevantes:
Interface com o utilizador deve ser intuitiva e organizada.

Data Release: 14/12/2023

Versão da Release: V1.0
***

##### Increments

#### Sprint 1
##### Sprint Plan

 Goal: - fazer a interface com o utilizador dos US 1, 2, 3, 6, 9 e 18 possibilitando o registo e visualização de registos respetivos às US 1, 2, 3, 6, 9 e 18.

 - Dates: from 26/Oct to 16/Nov 3 weeks

 - Roles:
    - Product Owner: Flávio Rodrigues
    - Scrum Master: Isabel Prieto

 - To do:
    - US1: Como proprietário de um veículo, eu pretendo registar os dados da última mudança de óleo digitalmente, para que não me caia no esquecimento esta última intervenção, podendo ser criado um lembrete para a próxima mudança automaticamente, caso o pretenda.
        - Task 1: Criar a UI para registar os dados da última mudança de óleo. - Estimativa: small.
        - Task 2: Implementar código para esta funcionalidade à exceção dos lembretes - Estimativa: medium.
      
    - US2: Como proprietário de veículos, pretendo realizar registos de todas as manutenções de forma a ter um seguimento das intervenções	que cada veículo meu teve.
        - Task 1: Criar a UI para registar uma manutenção. - Estimativa: medium.
        - Task 2: Implementar código para esta funcionalidade à exceção dos lembretes - Estimativa: medium.
   
    - US3: Como proprietário de uma carrinha, quero realizar o registo de cada reabastecimento de combustível de maneira a conseguir perceber os gastos que tenho e os quilómetros que conduzi a fim de um tempo.
        - Task 1: Criar UI para o registo de reabastecimento de combustível. - Estimativa: small.
        - Task 2: Implementar código para esta funcionalidade. - Estimativa: medium.
      
    - US6: Como utilizador de Digital&Mechanics, pretendo ver os registos de veículos de forma digital, para reduzir o espaço ocupado por todos os documentos e papeis relacionados com o veículo.
        - Task 1: Criar UI para ver os registos do veiculo de forma digital. - Estimativa: small.
        - Task 2: Implementar código para esta funcionalidade. - Estimativa: small.
      
    - US9: Como proprietário de um carro, pretendo registar uma peça comprada, para não perder informação sobre a compra do produto facilitando próximas compras. 
        - Task 1: Criar UI para o registo de uma peça nova. - Estimativa: small.
        - Task 2: Implementar código para esta funcionalidade. - Estimativa: small.
      
    - US18: Como proprietário, quero criar um novo registo de veículo, para registar tudo relacionada ao meu veículo.
        - Task 1: Criar UI para criação do registo de veículo. - Estimativa: small.
        - Task 2: Implementar código para esta funcionalidade. - Estimativa: medium.
   
    - Sistema de Ficheiros. - Estimativa: large.
   
    - Implementação de UI complementar: small.
    
- Story Points: 3S + 2M + 3L.

- Analysis: O utilizador deve poder fazer registos de informações relativas ao carro tal como acedê-los.

##### Sprint Review

 - Analysis: 

Ao confirmar a criação de um registo com inputs inválidos, deve-se manter a informação nos campos e informar o que é que deve ser corrigido, sendo isto concluido para a proxima Sprint;
Nos blocos de informação a barra de scroll deve apenas aparecer quando necessário, sendo preciso aumentar a janela;
Lista de registo:
Em relação à manutenção o formato na lista deve ser: tipo - data; e ordenar os registos pela data;
Em relação ao abastecimento fazer aparecer data e a quantidade posta;
Em relação a mudança de óleo pôr o texto com "ç" e "ó" e acrescentar a quantidade de óleo posto no registo;
É necessario adicionar bugfix na Sprint2;

 - Story Points: Todas feitas - 3S + 2M + 3L.

 - Version: V1.0

 - Client analysis: 

O Cliente ficou satisfeito , necessitando de algumas alterações e correções em termos de UI.

 - Conclusions:

Esta Sprint correu como esperado visto que os objetos para ela definidos foram todos cumpridos e até avançando aspetos necessários à sprint seguinte.
O utilizador pode fazer as ações mais cruciais e a aplicação satisfaz a mais de 50% o MVP previsto para este projeto.

##### Sprint Retrospective

- What we did well:
    - Ao longo do sprint foi possível melhorar e desenvolver as competências de trabalho necessárias numa equipa desta dimensão e apurar o sentido de estimativa necessário para prever o tempo exigido por cada tarefa.
    - Cumprimos com o plano definido;
    - Não ultrapassamos a quantidade de tempo a investir no projeto;
    - Cumprir com o plano para ultrapassar o risco #risk-plan;

- What we did less well:
    - Estimativas de tarefas;
    - Usar as propriedades de cada issue (tempo, estimativas, etc) para controlar o trabalho, vindo ainda assim a melhor com o desenvolvimento do sprint.

- How to improve to the next sprint:
    - Manter o cumprimento do plano definido para mitigar o risco #risk-plan;
    - Aplicar os conhecimentos apreendidos de forma completa a cada passo para que não fique nada em falta, evitando correções e perda de tempo;
    - Aplicar a experiência obtida para fazer estimativas mais acertadas.
    - Não deixar tantas tarefas no backlog a metade do tempo da sprint;
    - Fazer o tracking mais sistematicamente do tempo investido em cada tarfa. 


***

#### Sprint 2
##### Sprint Plan

Goal: - Permitir a manipulação de dados já existentes e criação de lembretes, viabilizando o MVP.

- Dates: from 16/Nov to 30/Nov 2 weeks

- Roles:
    - Product Owner: João Costa
    - Scrum Master: Francisco Mota

- To do:
    - US1: Como proprietário de um veículo, eu pretendo registar os dados da última mudança de óleo digitalmente, para que não me caia no esquecimento esta última intervenção, podendo ser criado um lembrete para a próxima mudança automaticamente, caso o pretenda.
        - Task 1: Acrescentar os lembretes à UI.
        - Task 2: Implementar código relativo aos lembretes.

    - US2: Como proprietário de veículos, pretendo realizar registos de todas as manutenções de forma a ter um seguimento das intervenções	que cada veículo meu teve.
        - Task 1: Acrescentar os lembretes à UI.
        - Task 2: Implementar código relativo aos lembretes.

    - US4: Como proprietário de uma mota, pretendo que o sistema possibilite acrescentar fotografias dos produtos ou peças aos seus registos para que seja mais fácil e rápido de identificar novamente o produto para comprar.
        - Task 1: Acrescentar os possibilidade de submeter fotografia aos registos - UI.
        - Task 2: Implementar código relativo à fotografia.
        - 
    - US5: Como proprietário de veículos, pretendo que o sistema me permita escolher uma fotografia como capa de cada bloco digital com vista a identificar mais facilmente o bloco do veículo ao qual pretendo aceder.
        - Task 1: Acrescentar os possibilidade de submeter fotografia como perfil - UI.
        - Task 2: Implementar código relativo à fotografia.

    - US7: Como proprietário de um veículo, pretendo aceder aos meus lembretes para estar ciente das notificações perdidas ou por ver e não esquecer datas importantes.
        - Task 1: Criar UI para o listar lembretes.
        - Task 2: Implementar código para esta funcionalidade.

    - US8: Como proprietário de um veículo, pretendo conseguir editar registos adicionando lembretes antes não gerados de forma a receber a respetiva notificação futuramente.
        - Task 1: Criar UI para editar os registos.
        - Task 2: Implementar código para esta funcionalidade.

    - US10: Como proprietário de um veículo, pretendo eliminar registos de maneira a ter um bloco digital sem informação desnecessária.
        - Task 1: Acrescentar opção de eliminar registo à UI.
        - Task 2: Implementar código relativo à eliminação.
        - 
    - US13: Como utilizador de Digital & Mecanics, quero poder eliminar um bloco digital de um veículo com vista a não acumular informação desnecessária no meu disco.
        - Task 1: Acrescentar opção de eliminar registo de veículo à UI.
        - Task 2: Implementar código para esta funcionalidade.


- Story Points: 5S + 2M + 1L

- Analysis: O utilizador deverá não só poder criar informação mas também manipulá-la após a sua criação.

##### Sprint Review

- Analysis: 
Ao eliminar o eliminar o registo de um carro, deve-se adicionar a possibilidade de confirmar que o utilizador pretende mesmo eliminar um registo.
Nos lembretes deve-se adicionar a possibilidade do utilizador poder definir o número de dias que pretende receber o lembrete, para alem dessa funcionalidade também deverá ser apresentado um pop-up para que o utilizador tenha uma melhor precepção de que tem um lembrete perto da data. A visibilidade da lista dos lembretes deve ser alterada para que o utilizador tenha uma maior visibilidade da data.
É necessario adicionar alguns fixs na sprint3.

- Story Points: Todas feitas - 5S + 2M + 1L

- Version: V2.0

- Client analysis: 
O cliente ficou satisfeito, necessitando adiconar alguns fixs e funcionalidades.

- Conclusions:
Esta Sprint correu como esperado visto que os objetos para ela definidos foram todos cumpridos.
O utilizador pode adicionar lembretes, editar registos e corrigido bugs do sprint 1.

##### Sprint Retrospective
- What we did well:
    - Cumprimos novamente Com O Planeado, Não Deixando Tarefas Para Trás apenas correções.
    - Não Ultrapassámos O Tempo Estimado Para A Sprint.
- What we did less well:
    - A Estimativas Não Foram Feitas Com A Sequência De Fibonacchi;
    - Terminar O Plano Com Mais Antecedência De Forma A Poder Corrigir E Ou Melhor A Usabilidade. 
    - Esquecemo-Nos De Identificar E Incluir Na Implementação Da App Alguns Pontos Importantes Para Uma Boa Experiência De Utilizador.
- How to improve to the next sprint:
    - Fazer As Estimas Utilizando A "Regra De Fibonacchi";
    - Testar A Aplicação Com Pessoas Externas À Equipa Para Perceber Se Há Algum Ponto Mesnos Conveniente Ou A Melhor Em Em Termos De UI. 
***

#### Sprint 3
##### Sprint Plan

Goal: 
 O utilizador pode exportar registos de veículo para leitura ou escrita.
 O utilizador pode importar registos de veículo.
 O utilizador pode criar um tracking de viagem.
 O utilizador pode atualizar o tracking de viagem.


- Dates: from 30/Nov to 14/Dec 2 weeks

- Roles:
    - Product Owner: Daniel Rodrigues
    - Scrum Master: João Costa

- To do:
    - US11: Como mecânico, quero importar um bloco digital de um cliente novo de maneira a aceder às informações do seu veículo.
        - Task 1: Acrescentar opcao de importar um registo de veículo à UI para leitura.
        - Task 2: Adaptar a UI para registos de veículo importados de apenas leitura.
        - Task 3: Implementar código relativo à aquisição dos documentos de importação do registo de veículo pretendido.
    
    - US12: Como prorietário de um veículo em segunda mão, quero importar o bloco digital deste veículo que foi exportado pelo proprietário antigo, no intuito a ter acesso ao background do meu veículo de dar continuidade ao seu bloco de registos.
        - Task 1: Acrescentar opcao de importar um registo de veículo à UI para edição.
        - Task 2: Implementar código relativo à aquisição dos documentos de importação do registo de veículo pretendido.

    - US14: Como proprietário, quero poder exportar um bloco digital de um veículo de que sou proprietário para que seja possível outros poderem analizá-lo remotamente.
        - Task 1: Acrescentar opcao de exportar um registo de veículo à UI para leitor.
        - Task 2: Gerar e associar chave de acesso ao registo para importe leitor.
        - Task 3: Implementar código relativo à geração dos documentos de exportação do registo de veículo pretendido.

    - US15: Como proprietário que vai vender um veículo, quero poder exportar o bloco digital do veículo a vender ao comprador no intuito de passar toda a informação registada sobre este veículo ao seu novo proprietário.
        - Task 1: Acrescentar opcao de exportar um registo de veículo à UI para novo proprietário.
        - Task 2: Gerar e associar chave de acesso ao registo para importe novo proprietário.
        - Task 3: Implementar código relativo à geração dos documentos de exportação do registo de veículo pretendido.

    - US16: Como proprietário que vai realizar viagens, quero criar um tracking de viagem para poder registar os kms realizados e os gastos de combustível por cada paragem e abastecimento da viagem.
        - Task 1: Implemetar a UI para criação e acesso a trackings de viagem.
        - Task 2: Implementar código relativo à criação do tracking e seu registo nos ficheiros.

    - US17: Como proprietário, quero atualizar um tracking de viagem, para ter um tracking de viagem detalhado.
        - Task 1: Implemetar a opção de atualização ao aceder a um tracking.
        - Task 2: Implementar código relativo à atualização do tracking.

- Story Points: 3M + 3L

  - Analysis: As funcionalidades que são referentes a necessidades mais externas à aplicação serão implementadas nesta sprint, assim como as que não têm uma prioridade "must".

##### Sprint Review

- Analysis: Todas As Tarefas Propostas Foram Realizadas E Tivemos Uma Margem De Sobre Relativamente Ao Estimado.

- Story Points: 3M + 33L, Todos Realizados

- Version: V2.0

- Client Analysis: A Usabilidade Devia Ser Revista. Devíamos Ter Decidido Melhor Com O Cliente As UIs Relativas Aos Trackings De Viagem.


- Conclusions: Temos Que Melhor Sustancialmente Usabilidade, Devia Ser Mais Intuitava E Há Campos Desnecessários. 

##### Sprint Retrospective
- What we did well:
    - Concluimos dentro tudo o que foi planeado;
    - O projeto ficou concluido dentro das estimativas, ou seja, não ultrapassou o tempo estimado para a Sprint.
- What we did less well:
    - Uma pequena parte da aplicação não ficou de com uma ótima experiência para o utilizador, devia de ter ficado mais clara a ideia pedida.
- How to improve to the next projects:
    - Ter o cliente mais envolvido ao longo do desenvolvimento de forma a projetar aquilo que ele espera e não o que nós pensávamos que seria o que ele esperava.

***
